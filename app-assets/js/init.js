$(window).on("load", function () {
    if (feather) {
        feather.replace({
            width: 14,
            height: 14,
        });
    }

    alertHide();
    alertCheckSiteHide();

    $("#alert-container").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        stopAlertSound();
    });

    $("#alert-checksite-container").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        stopAlertCheckSiteSound();

        if (window.location.pathname != '/orgApps/status') {
            window.location.href = base_url() + '/orgApps/status';
        }

    });

    $(".modal").on("shown.bs.modal", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $("input:visible:enabled:first", e.target).focus();
        console.log("init shown.bs.modal");
    });

    if (localStorage["dark-layout-current-skin"] == "light-layout") {
        $("html").removeClass("dark-layout").addClass("light-layout");
        $(".header-navbar").removeClass("navbar-dark").addClass("navbar-light");
        $(".main-menu").removeClass("menu-dark").addClass("menu-light");
        $("#sidebar-logo").attr(
            "src",
            "/app-assets/images/logo/logo-text-light.png"
        );
    } else {
        // $("html").removeClass("light-layout").addClass("dark-layout");
        // $(".header-navbar").removeClass("navbar-light").addClass("navbar-dark");
        // $(".main-menu").removeClass("menu-light").addClass("menu-dark");
        // $("#sidebar-logo").attr("src", "/app-assets/images/logo/logo-text.png");
    }
});

function alertCheckSiteShow() {
    $("#alert-checksite-container").removeClass("d-none");
}

function alertCheckSiteHide() {
    $("#alert-checksite-container").addClass("d-none");
}

function alertShow() {
    $("#alert-container").removeClass("d-none");
}

function alertHide() {
    $("#alert-container").addClass("d-none");
}

function notificationShow() {
    $("#notification-container").removeClass("d-none");
}

function notificationHide() {
    $("#notification-container").addClass("d-none");
}

var currentFile = "";
var oAudio = document.getElementById("myaudio");

var currentFileNotification = "";
var myNotificationAudio = document.getElementById("mynotificationaudio");

function playAlertSound() {
    // See if we already loaded this audio file.
    if ($("#audiofile").val() !== currentFile) {
        oAudio.src = $("#audiofile").val();
        currentFile = $("#audiofile").val();
    }
    var test = $("#myaudio");
    test.src = $("#audiofile").val();

    oAudio.loop = true;
    oAudio.play();
    alertShow();
}

function stopAlertSound() {
    alertHide();
    oAudio.loop = false;
    oAudio.pause();
    oAudio.currentTime = 0;
}


function playNotificationSound() {
    // See if we already loaded this audio file.
    if ($("#notificationfile").val() !== currentFileNotification) {
        myNotificationAudio.src = $("#notificationfile").val();
        currentFileNotification = $("#notificationfile").val();
    }
    var test = $("#mynotificationaudio");
    test.src = $("#notificationfile").val();

    myNotificationAudio.loop = false;
    myNotificationAudio.play();
    // alertShow();
}

function stopNotificationSound() {
    alertHide();
    myNotificationAudio.loop = false;
    myNotificationAudio.pause();
    myNotificationAudio.currentTime = 0;
}

function playAlertCheckSiteSound() {
    // See if we already loaded this audio file.
    if ($("#audiofile").val() !== currentFile) {
        oAudio.src = $("#audiofile").val();
        currentFile = $("#audiofile").val();
    }
    var test = $("#myaudio");
    test.src = $("#audiofile").val();

    oAudio.loop = true;
    oAudio.play();
    alertCheckSiteShow();
}

function stopAlertCheckSiteSound() {
    alertCheckSiteHide();
    oAudio.loop = false;
    oAudio.pause();
    oAudio.currentTime = 0;
}

/* For Export Buttons available inside jquery-datatable "server side processing" - Start
- due to "server side processing" jquery datatble doesn't support all data to be exported
- below function makes the datatable to export all records when "server side processing" is on */

function newexportaction(e, dt, button, config) {
    var self = this;
    var oldStart = dt.settings()[0]._iDisplayStart;
    dt.one("preXhr", function (e, s, data) {
        // Just this once, load all data from the server...
        data.start = 0;
        data.length = 2147483647;
        dt.one("preDraw", function (e, settings) {
            // Call the original action function
            if (button[0].className.indexOf("buttons-copy") >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(
                    self,
                    e,
                    dt,
                    button,
                    config
                );
            } else if (button[0].className.indexOf("buttons-excel") >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)
                    ? $.fn.dataTable.ext.buttons.excelHtml5.action.call(
                          self,
                          e,
                          dt,
                          button,
                          config
                      )
                    : $.fn.dataTable.ext.buttons.excelFlash.action.call(
                          self,
                          e,
                          dt,
                          button,
                          config
                      );
            } else if (button[0].className.indexOf("buttons-csv") >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config)
                    ? $.fn.dataTable.ext.buttons.csvHtml5.action.call(
                          self,
                          e,
                          dt,
                          button,
                          config
                      )
                    : $.fn.dataTable.ext.buttons.csvFlash.action.call(
                          self,
                          e,
                          dt,
                          button,
                          config
                      );
            } else if (button[0].className.indexOf("buttons-pdf") >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config)
                    ? $.fn.dataTable.ext.buttons.pdfHtml5.action.call(
                          self,
                          e,
                          dt,
                          button,
                          config
                      )
                    : $.fn.dataTable.ext.buttons.pdfFlash.action.call(
                          self,
                          e,
                          dt,
                          button,
                          config
                      );
            } else if (button[0].className.indexOf("buttons-print") >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
            dt.one("preXhr", function (e, s, data) {
                // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                // Set the property to what it was before exporting.
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
            });
            // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            setTimeout(dt.ajax.reload, 0);
            // Prevent rendering of the full data to the DOM
            return false;
        });
    });
    // Requery the server with the new one-time export settings
    dt.ajax.reload();
}
//For Export Buttons available inside jquery-datatable "server side processing" - End

// base url
function base_url() {
    var pathparts = location.pathname.split("/");
    if (location.host == "localhost") {
        var url = location.origin + "/" + pathparts[1].trim("/") + "/"; // http://localhost/myproject/
    } else {
        var url = location.origin; // http://stackoverflow.com
    }
    return url;
}

function checkClientSite() {
    console.log("checkClientSite() called at " + new Date().toISOString());

    $.ajax({
        url: base_url() + "/orgApps/checkClientSite"
      });

    // $.post(
    //     base_url() + "/orgApps/checkClientSite",
    //     {},
    //     function (r) {
    //         console.log('checkClientSite', r)
    //     },
    //     "json"
    // );
}

// var checkClientSiteMinutes = 5 * 60 * 1000;
// var checkClientSiteMinutes = 5 * 1000;
// setInterval(checkClientSite, checkClientSiteMinutes);
