'use-strict'

// var dateObj = new Date();
var dateObj = moment().toDate()
// var nextDay = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
var nextDay = moment(moment().toDate().getTime() + 24 * 60 * 60 * 1000).toDate()
// prettier-ignore
var nextMonth = dateObj.getMonth() === 11 ? new Date(dateObj.getFullYear() + 1, 0, 1) : new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 1)
// prettier-ignore
var prevMonth = dateObj.getMonth() === 11 ? new Date(dateObj.getFullYear() - 1, 0, 1) : new Date(dateObj.getFullYear(), dateObj.getMonth() - 1, 1)

console.log('dateObj', dateObj)
console.log('nextDay', nextDay)
console.log('nextMonth', nextMonth)
console.log('prevMonth', prevMonth)

var events = [
    {
        id: 1,
        url: '',
        title: 'Design Review',
        start: dateObj,
        end: nextDay,
        allDay: false,
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 2,
        url: '',
        title: 'Meeting With Client',
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -11),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -10),
        allDay: true,
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 3,
        url: '',
        title: 'Family Trip',
        allDay: true,
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -9),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -7),
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 4,
        url: '',
        title: 'Doctor\'s Appointment',
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -11),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -10),
        allDay: true,
        extendedProps: {
            calendar: 'Personal'
        }
    },
    {
        id: 5,
        url: '',
        title: 'Dart Game?',
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -13),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -12),
        allDay: true,
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 6,
        url: '',
        title: 'Meditation',
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -13),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -12),
        allDay: true,
        extendedProps: {
            calendar: 'Personal'
        }
    },
    {
        id: 7,
        url: '',
        title: 'Dinner',
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -13),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -12),
        allDay: true,
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 8,
        url: '',
        title: 'Product Review',
        start: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -13),
        end: new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, -12),
        allDay: true,
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 9,
        url: '',
        title: 'Monthly Meeting',
        start: nextMonth,
        end: nextMonth,
        allDay: true,
        extendedProps: {
            calendar: 'Public'
        }
    },
    {
        id: 10,
        url: '',
        title: 'Monthly Checkup',
        start: prevMonth,
        end: prevMonth,
        allDay: true,
        extendedProps: {
            calendar: 'Public'
        }
    }
]

// RTL Support
var direction = 'ltr',
    assetPath = '../../../app-assets/'
if ($('html').data('textdirection') == 'rtl') {
    direction = 'rtl'
}

if ($('body').attr('data-framework') === 'laravel') {
    assetPath = $('body').attr('data-asset-path')
}

$(document).on('click', '.fc-sidebarToggle-button', function(e) {
    $('.app-calendar-sidebar, .body-content-overlay').addClass('show')
})

$(document).on('click', '.body-content-overlay', function(e) {
    $('.app-calendar-sidebar, .body-content-overlay').removeClass('show')
})

// document.addEventListener('DOMContentLoaded', function () {
function renderCalendar() {
    var calendarEl = document.getElementById('calendar'),
        eventToUpdate,
        sidebar = $('.event-sidebar'),
        calendarsColor = calendarsColorList,
        eventForm = $('.event-form'),
        addEventBtn = $('.add-event-btn'),
        cancelBtn = $('.btn-cancel'),
        updateEventBtn = $('.update-event-btn'),
        toggleSidebarBtn = $('.btn-toggle-sidebar'),
        eventTitle = $('#title'),
        eventLabel = $('#select-label'),
        startDate = $('#start-date'),
        endDate = $('#end-date'),
        eventUrl = $('#event-url'),
        eventGuests = $('#event-guests'),
        eventLocation = $('#event-location'),
        allDaySwitch = $('.allDay-switch'),
        selectAll = $('.select-all'),
        calEventFilter = $('.calendar-events-filter'),
        filterInput = $('.input-filter'),
        btnDeleteEvent = $('.btn-delete-event'),
        calendarEditor = $('#event-description-editor')

    // --------------------------------------------
    // On add new item, clear sidebar-right field fields
    // --------------------------------------------
    $('.add-event button').on('click', function(e) {
        $('.event-sidebar').addClass('show')
        $('.sidebar-left').removeClass('show')
        $('.app-calendar .body-content-overlay').addClass('show')
    })

    // Label  select
    if (eventLabel.length) {
        function renderBullets(option) {
            if (!option.id) {
                return option.text
            }
            var $bullet =
                '<span class=\'bullet bullet-' +
                $(option.element).data('label') +
                ' bullet-sm me-50\'> ' +
                '</span>' +
                option.text

            return $bullet
        }

        eventLabel.wrap('<div class="position-relative"></div>').select2({
            placeholder: 'Select value',
            dropdownParent: eventLabel.parent(),
            templateResult: renderBullets,
            templateSelection: renderBullets,
            minimumResultsForSearch: -1,
            escapeMarkup: function(es) {
                return es
            }
        })
    }

    // Guests select
    if (eventGuests.length) {
        function renderGuestAvatar(option) {
            if (!option.id) {
                return option.text
            }

            var $avatar =
                '<div class=\'d-flex flex-wrap align-items-center\'>' +
                '<div class=\'avatar avatar-sm my-0 me-50\'>' +
                '<span class=\'avatar-content\'>' +
                '<img src=\'' +
                assetPath +
                'images/avatars/' +
                $(option.element).data('avatar') +
                '\' alt=\'avatar\' />' +
                '</span>' +
                '</div>' +
                option.text +
                '</div>'

            return $avatar
        }

        eventGuests.wrap('<div class="position-relative"></div>').select2({
            placeholder: 'Select value',
            dropdownParent: eventGuests.parent(),
            closeOnSelect: false,
            templateResult: renderGuestAvatar,
            templateSelection: renderGuestAvatar,
            escapeMarkup: function(es) {
                return es
            }
        })
    }

    // Start date picker
    if (startDate.length) {
        var start = startDate.flatpickr({
            enableTime: true,
            altFormat: 'Y-m-dTH:i:S',
            dateFormat: 'Y-m-d H:i:S',
            time_24hr: true,
            onReady: function(selectedDates, dateStr, instance) {
                if (instance.isMobile) {
                    $(instance.mobileInput).attr('step', null)
                }
            }
        })
    }

    // End date picker
    if (endDate.length) {
        var end = endDate.flatpickr({
            enableTime: true,
            altFormat: 'Y-m-dTH:i:S',
            dateFormat: 'Y-m-d H:i:S',
            time_24hr: true,
            onReady: function(selectedDates, dateStr, instance) {
                if (instance.isMobile) {
                    $(instance.mobileInput).attr('step', null)
                }
            }
        })
    }

    // Event click function
    function eventClick(info) {
        eventToUpdate = info.event

        console.log('eventToUpdate', eventToUpdate)
        console.log('eventToUpdate.url', eventToUpdate.url)

        if (eventToUpdate.url && eventToUpdate.url != undefined && eventToUpdate.url != 'undefined') {
            info.jsEvent.preventDefault()
            window.open(eventToUpdate.url, '_blank')
        }

        sidebar.modal('show')
        addEventBtn.addClass('d-none')
        cancelBtn.addClass('d-none')
        updateEventBtn.removeClass('d-none')
        btnDeleteEvent.removeClass('d-none')

        eventTitle.val(eventToUpdate.title)
        start.setDate(eventToUpdate.start, true, 'Y-m-d')
        eventToUpdate.allDay === true ? allDaySwitch.prop('checked', true) : allDaySwitch.prop('checked', false)
        eventToUpdate.end !== null
            ? end.setDate(eventToUpdate.end, true, 'Y-m-d')
            : end.setDate(eventToUpdate.start, true, 'Y-m-d')

        sidebar.find(eventLabel).val(eventToUpdate.extendedProps.calendar).trigger('change')

        eventToUpdate.extendedProps.location !== undefined ? eventLocation.val(eventToUpdate.extendedProps.location) : null

        eventToUpdate.extendedProps.guests !== undefined
            ? eventGuests.val(eventToUpdate.extendedProps.guests).trigger('change')
            : null

        eventToUpdate.extendedProps.guests !== undefined
            ? calendarEditor.val(eventToUpdate.extendedProps.description)
            : null

        //  Delete Event
        btnDeleteEvent.off('click');
        btnDeleteEvent.on('click', function(e) {
            e.preventDefault()
            e.stopPropagation()

            $.post(base_url() + '/dash/deleteEvent', 'id=' + eventToUpdate.id,
                function(resp) {
                    if (!resp.error) {

                        sidebar.modal('hide')
                        $('.event-sidebar').removeClass('show')
                        $('.app-calendar .body-content-overlay').removeClass('show')
                        calendar.refetchEvents()

                        toastr['success'](resp.message, 'SUKSES', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                    } else {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })
                    }
                }, 'json')

            return false

            // console.log('btnDeleteEvent eventToUpdate', eventToUpdate.id)
            // eventToUpdate.remove()

            // removeEvent(eventToUpdate.id);
            // sidebar.modal('hide')
            // $('.event-sidebar').removeClass('show')
            // $('.app-calendar .body-content-overlay').removeClass('show')
        })
    }

    // Modify sidebar toggler
    function modifyToggler() {
        $('.fc-sidebarToggle-button')
            .empty()
            .append(feather.icons['menu'].toSvg({ class: 'ficon' }))
    }

    // Selected Checkboxes
    function selectedCalendars() {
        var selected = []
        $('.calendar-events-filter input:checked').each(function() {
            selected.push($(this).attr('data-value'))
        })
        return selected
    }

    // --------------------------------------------------------------------------------------------------
    // AXIOS: fetchEvents
    // * This will be called by fullCalendar to fetch events. Also this can be used to refetch events.
    // --------------------------------------------------------------------------------------------------
    function fetchEvents(info, successCallback) {
        console.log('fetchEvents', info)
        // console.log('successCallback', successCallback)

        var category = $('.input-filter:checked').map(function() {
            return $(this).attr('data-value')
        }).get()
        console.log('checked', category)

        // Fetch Events from API endpoint reference
        $.post(base_url() + '/dash/getEvent', {
                sDate: moment(info.startStr).format('YYYY-MM-DD HH:mm:ss'),
                eDate: moment(info.endStr).format('YYYY-MM-DD HH:mm:ss'),
                category: category
            },
            function(result) {
                if (!result.error) {
                    var calendars = selectedCalendars()

                    selectedEvents = result.data.map(function(val) {
                        val.start = moment(val.start).toDate()
                        val.end = moment(val.end).toDate()
                        return val
                    })

                    successCallback(selectedEvents)

                    var tmp = selectedEvents.filter(event => calendars.includes(event.extendedProps.calendar))

                    return [tmp]
                } else {
                    console.log(result.error)
                }
            }, 'json')

        // $.ajax(
        //     {
        //         url: base_url() + '/dash/getEvent',
        //         data: {
        //             sDate: moment(info.startStr).format('YYYY-MM-DD HH:mm:ss'),
        //             eDate: moment(info.endStr).format('YYYY-MM-DD HH:mm:ss')
        //         },
        //         contentType: 'application/json',
        //         dataType: 'json',
        //         type: 'POST',
        //         success: function(result) {
        //             // Get requested calendars as Array
        //             var calendars = selectedCalendars()
        //
        //             selectedEvents = result.data.map(function(val) {
        //                 val.start = moment(val.start).toDate()
        //                 val.end = moment(val.end).toDate()
        //                 return val
        //             })
        //
        //             successCallback(selectedEvents)
        //
        //             var tmp = selectedEvents.filter(event => calendars.includes(event.extendedProps.calendar))
        //
        //             return [tmp]
        //         },
        //         error: function(error) {
        //             console.log(error)
        //         }
        //     }
        // )

        // var calendars = selectedCalendars();
        // // We are reading event object from app-calendar-events.js file directly by including that file above app-calendar file.
        // // You should make an API call, look into above commented API call for reference
        // selectedEvents = events.filter(function (event) {
        //     // console.log(event.extendedProps.calendar.toLowerCase());
        //     return calendars.includes(event.extendedProps.calendar.toLowerCase());
        // });
        // // if (selectedEvents.length > 0) {
        // successCallback(selectedEvents);
        // // // }
    }

    // Calendar plugins
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        events: fetchEvents,
        editable: true,
        dragScroll: true,
        dayMaxEvents: 2,
        eventResizableFromStart: true,
        customButtons: {
            sidebarToggle: {
                text: 'Sidebar'
            }
        },
        headerToolbar: {
            start: 'sidebarToggle, prev,next, title',
            end: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        },
        direction: direction,
        initialDate: new Date(),
        navLinks: true, // can click day/week names to navigate views
        eventClassNames: function({ event: calendarEvent }) {
            const colorName = calendarsColor[calendarEvent._def.extendedProps.calendar]

            return [
                // Background Color
                'bg-light-' + colorName
            ]
        },
        dateClick: function(info) {
            var date = moment(info.date).format('YYYY-MM-DD')
            resetValues()
            sidebar.modal('show')
            addEventBtn.removeClass('d-none')
            updateEventBtn.addClass('d-none')
            btnDeleteEvent.addClass('d-none')
            startDate.val(date)
            endDate.val(date)
        },
        eventClick: function(info) {
            eventClick(info)
        },
        datesSet: function() {
            modifyToggler()
        },
        viewDidMount: function() {
            modifyToggler()
        }
    })

    // Render calendar
    calendar.render()
    // Modify sidebar toggler
    modifyToggler()
    // updateEventClass();

    // Validate add new and update form
    if (eventForm.length) {
        eventForm.validate({
            submitHandler: function(form, event) {
                event.preventDefault()
                if (eventForm.valid()) {
                    sidebar.modal('hide')
                }
            },
            title: {
                required: true
            },
            rules: {
                'start-date': { required: true },
                'end-date': { required: true }
            },
            messages: {
                'start-date': { required: 'Start Date is required' },
                'end-date': { required: 'End Date is required' }
            }
        })
    }

    // Sidebar Toggle Btn
    if (toggleSidebarBtn.length) {
        toggleSidebarBtn.on('click', function() {
            cancelBtn.removeClass('d-none')
        })
    }

    // ------------------------------------------------
    // addEvent
    // ------------------------------------------------
    function addEvent(eventData) {
        calendar.addEvent(eventData)
        calendar.refetchEvents()
    }

    // ------------------------------------------------
    // updateEvent
    // ------------------------------------------------
    function updateEvent(eventData) {
        var propsToUpdate = ['id', 'title', 'url']
        var extendedPropsToUpdate = ['calendar', 'guests', 'location', 'description']

        updateEventInCalendar(eventData, propsToUpdate, extendedPropsToUpdate)
    }

    // ------------------------------------------------
    // removeEvent
    // ------------------------------------------------
    function removeEvent(eventId) {
        removeEventInCalendar(eventId)
    }

    // ------------------------------------------------
    // (UI) updateEventInCalendar
    // ------------------------------------------------
    const updateEventInCalendar = (updatedEventData, propsToUpdate, extendedPropsToUpdate) => {
        const existingEvent = calendar.getEventById(updatedEventData.id)

        // --- Set event properties except date related ----- //
        // ? Docs: https://fullcalendar.io/docs/Event-setProp
        // dateRelatedProps => ['start', 'end', 'allDay']
        // eslint-disable-next-line no-plusplus
        for (var index = 0; index < propsToUpdate.length; index++) {
            var propName = propsToUpdate[index]
            existingEvent.setProp(propName, updatedEventData[propName])
        }

        // --- Set date related props ----- //
        // ? Docs: https://fullcalendar.io/docs/Event-setDates
        existingEvent.setDates(updatedEventData.start, updatedEventData.end, { allDay: updatedEventData.allDay })

        // --- Set event's extendedProps ----- //
        // ? Docs: https://fullcalendar.io/docs/Event-setExtendedProp
        // eslint-disable-next-line no-plusplus
        for (var index = 0; index < extendedPropsToUpdate.length; index++) {
            var propName = extendedPropsToUpdate[index]
            existingEvent.setExtendedProp(propName, updatedEventData.extendedProps[propName])
        }
    }

    // ------------------------------------------------
    // (UI) removeEventInCalendar
    // ------------------------------------------------
    function removeEventInCalendar(eventId) {
        calendar.getEventById(eventId).remove()
    }

    // Add new event
    $(addEventBtn).on('click', function() {
        if (eventForm.valid()) {
            console.log(startDate.val())

            $.post(base_url() + '/dash/addEvent', {
                    id: null,
                    title: eventTitle.val(),
                    start: moment(startDate.val()).format('YYYY-MM-DD HH:mm:ss'),
                    end: moment(endDate.val()).format('YYYY-MM-DD HH:mm:ss'),
                    location: eventLocation.val(),
                    category: eventLabel.val(),
                    description: calendarEditor.val(),
                    allday: allDaySwitch.prop('checked') ? 1 : 0,
                },
                function(result) {
                    if (!result.error) {
                        calendar.refetchEvents()
                    } else {
                        console.log(result.error)
                    }
                }, 'json')



            // return false
            //
            // var newEvent = {
            //     id: calendar.getEvents().length + 1,
            //     title: eventTitle.val(),
            //     start: startDate.val(),
            //     end: endDate.val(),
            //     startStr: startDate.val(),
            //     endStr: endDate.val(),
            //     display: 'block',
            //     extendedProps: {
            //         location: eventLocation.val(),
            //         guests: eventGuests.val(),
            //         calendar: eventLabel.val(),
            //         description: calendarEditor.val()
            //     }
            // }
            //
            // if (eventUrl.val().length) {
            //     newEvent.url = eventUrl.val()
            // }
            //
            // if (allDaySwitch.prop('checked')) {
            //     newEvent.allDay = true
            // }
            // addEvent(newEvent)
        }
    })

    // Update new event
    updateEventBtn.on('click', function() {
        if (eventForm.valid()) {

            $.post(base_url() + '/dash/addEvent', {
                    id: eventToUpdate.id,
                    title: eventTitle.val(),
                    start: moment(startDate.val()).format('YYYY-MM-DD HH:mm:ss'),
                    end: moment(endDate.val()).format('YYYY-MM-DD HH:mm:ss'),
                    location: eventLocation.val(),
                    category: eventLabel.val(),
                    description: calendarEditor.val(),
                    allday: allDaySwitch.prop('checked') ? 1 : 0,
                },
                function(result) {
                    if (!result.error) {
                        calendar.refetchEvents()
                        sidebar.modal('hide')
                    } else {
                        toastr['error'](result.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })
                    }
                }, 'json')

            return false
            //
            // var eventData = {
            //     id: eventToUpdate.id,
            //     title: sidebar.find(eventTitle).val(),
            //     start: sidebar.find(startDate).val(),
            //     end: sidebar.find(endDate).val(),
            //     // url: eventUrl.val(),
            //     url: '',
            //     extendedProps: {
            //         location: eventLocation.val(),
            //         guests: eventGuests.val(),
            //         calendar: eventLabel.val(),
            //         description: calendarEditor.val()
            //     },
            //     display: 'block',
            //     allDay: allDaySwitch.prop('checked') ? true : false
            // }
            //
            // updateEvent(eventData)
            // sidebar.modal('hide')
        }
    })

    // Reset sidebar input values
    function resetValues() {
        endDate.val('')
        eventUrl.val('')
        startDate.val('')
        eventTitle.val('')
        eventLocation.val('')
        allDaySwitch.prop('checked', false)
        eventGuests.val('').trigger('change')
        calendarEditor.val('')
    }

    // When modal hides reset input values
    sidebar.on('hidden.bs.modal', function() {
        resetValues()
    })

    // Hide left sidebar if the right sidebar is open
    $('.btn-toggle-sidebar').on('click', function() {
        btnDeleteEvent.addClass('d-none')
        updateEventBtn.addClass('d-none')
        addEventBtn.removeClass('d-none')
        $('.app-calendar-sidebar, .body-content-overlay').removeClass('show')
    })

    // Select all & filter functionality
    if (selectAll.length) {
        selectAll.on('change', function() {
            var $this = $(this)

            if ($this.prop('checked')) {
                calEventFilter.find('input').prop('checked', true)
            } else {
                calEventFilter.find('input').prop('checked', false)
            }
            calendar.refetchEvents()
        })
    }

    if (filterInput.length) {
        filterInput.on('change', function() {
            $('.input-filter:checked').length < calEventFilter.find('input').length
                ? selectAll.prop('checked', false)
                : selectAll.prop('checked', true)
            calendar.refetchEvents()
        })
    }
}
