<?php

class Otp extends CI_Controller {

    public $data;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set(setting('timezone'));

        if (!empty($this->db->username) && !empty($this->db->hostname) && !empty($this->db->database)) {
        }
        else {
            die('Database is not configured');
        }

        if (is_logged()) {
            redirect('dash', 'refresh');
        }
    }

    public function index($otpId = null)
    {
        if (!$otpId) {
            redirect('osintauth', 'refresh');
        }

        $this->db->where("id", $otpId);
        $this->db->where("is_verified", 0);
        $this->db->order_by('otp_timestamp_from', 'desc');
        $q = $this->db->get("otp");
        $row = $q->row();

        if ($row) {
            $this->load->view('otp', [
                'otpId'            => $otpId,
                'email'            => hideEmailAddress($row->email_phone_no),
                'otp_timestamp_to' => $row->otp_timestamp_to,
            ]);
        } else {
            redirect('osintauth', 'refresh');
        }
    }

    public function checkOtp()
    {
        try {
            $otpCode = $this->input->post('otpCode', true);
            $otpId = $this->input->post('otpId', true);

            $otpData = $this->otp_model->checkOtp($otpId, $otpCode);

            if ($otpData->otp_category_code == 'E') {
                $datalogin = $this->crud_model->checkloginByEmail($otpData->email_phone_no);
            }
            else {
                $datalogin = $this->crud_model->checkloginByPhone($otpData->email_phone_no);
            }

            if (isset($datalogin->user_id) && $datalogin->user_id != "") {
                $time = time();

                // encypting userid and password with current time $time
                $login_token = sha1($datalogin->user_id . $datalogin->user_password . $time);

                $array = [
                    'login'       => true,
                    // saving encrypted userid and password as token in session
                    'login_token' => $login_token,
                    'logged'      => [
                        'id'                => $datalogin->user_id,
                        'user_type'         => $datalogin->user_type,
                        'user_type_name'    => $datalogin->user_type_name,
                        'user_name'         => $datalogin->user_name,
                        'verifikator_level' => $datalogin->verifikator_level,
                        'jabatan_id'        => $datalogin->jabatan_id,
                        'time'              => $time,
                    ]
                ];

                $this->session->set_userdata($array);

                $this->db->where('id', $otpData->id);
                $this->db->update('otp', [
                    'is_verified' => 1
                ]);

                $insertlog = $this->log_model->insertlog($datalogin->user_name, "LOGIN", "LOGIN", $datalogin->user_id);

                echo json_encode([
                    "error"    => false,
                    'message'  => 'Kode OTP Sesuai',
                    'redirect' => base_url() . "dash",
                ]);
            }
            else {
                throw new Exception('User tidak ditemukan');
            }
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    public function resendOtp()
    {
        $otpId = $this->input->post('otpId', true);

        $this->db->where("id", $otpId);
        $q = $this->db->get("otp");
        $row = $q->row();

        $otpId = $this->otp_model->makeOtp($row->email_phone_no);

        echo json_encode([
            "error"    => false,
            'redirect' => base_url() . 'otp/' . $otpId,
        ]);
    }
}
