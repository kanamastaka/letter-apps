<?php

class Jabatan extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Jabatan';
        $this->page_data['page']->menu = 'jabatan';

        $this->load->model('Jabatan_model');
    }

    function index()
    {
        $this->load->view('jabatan/list', $this->page_data);
    }

    function getById()
    {
        $id = $this->input->post('id', true);

        $data = Jabatan_model::select([
            'id',
            'nama',
        ])
            ->whereId($id)
            ->first();

        echo json_encode([
            "error" => false,
            'data'  => $data
        ]);
    }

    function delete()
    {
        try {
            $id = $this->input->post('id', true);

            $Jabatan = Jabatan_model::whereId($id)->first();

            if (!$Jabatan) {
                throw new Exception('Data tidak ditemukan');
            }

            $Jabatan->delete();

            $action = 'Hapus Data Jabatan';

            $this->log_model->logInsert([
                'type'    => 'JABATAN',
                'action'  => $action,
                'id'      => $Jabatan->id,
                'logData' => $Jabatan,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $Jabatan,
                'message' => 'Data Jabatan Sudah Dihapus'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_list()
    {
        $sdate = $this->input->post("sdate");
        $edate = $this->input->post("edate");

        $select = "SELECT
                jabatan.id AS DT_RowId,
                jabatan.id,
                jabatan.nama
            FROM
                jabatan
                WHERE deleted_at IS NULL";

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['DT_RowId'];
        });

        echo $dt->generate();
    }

    function process_save()
    {
        try {
            $id = $this->input->post('id', true);
            $nama = $this->input->post('nama', true);

            unset($trx);

            $trx = [
                'id'   => $id,
                'nama' => $nama,
            ];

            if (!$id) {
                $action = 'Tambah data jabatan';
                $msg = 'Tambah data sukses';
            }
            else {
                $action = 'Edit data jabatan';
                $msg = 'Edit data sukses';
            }

            $jabatan = Jabatan_model::updateOrCreate([
                'id' => $id
            ], $trx);

            unset($trx['id']);

            $this->log_model->logInsert([
                'type'    => 'JABATAN',
                'action'  => $action,
                'id'      => $jabatan->id,
                'logData' => $jabatan,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $jabatan,
                'message' => $msg,
            ]);
        } catch (\Exception $e) {

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_select2_list()
    {
        $json = [];

        $limit = 10;
        $page = $this->input->get("page");

        if (!$page) {
            $page = 0;
        }
        else {
            $page = ($page - 1) * $limit;
        }

        $query = $this->db
            ->where('deleted_at IS NULL')
            ->like('nama', $this->input->get("q"));

        $query = $this->db
            ->select([
                'id',
                'nama as text'
            ])
            ->order_by('nama', 'asc');

        $queryLimit = $query
            ->limit($limit, $page)
            ->get("jabatan");

        $json['items'] = $queryLimit->result();
        $json['total'] = $query->get("jabatan")->num_rows();

        echo json_encode($json);
    }
}
