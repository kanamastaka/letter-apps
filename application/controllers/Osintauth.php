<?php
class Osintauth extends CI_Controller
{

    public $data;

	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set(setting('timezone'));

		if (!empty($this->db->username) && !empty($this->db->hostname) && !empty($this->db->database)) {
		} else {
			die('Database is not configured');
		}

		if (is_logged()) {
			redirect('dash', 'refresh');
		}
	}

	public function index()
	{
		$this->load->view('login');
	}
}
