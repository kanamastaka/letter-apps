<?php

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class SuratMasuk extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Surat Masuk';
        $this->page_data['page']->menu = 'surat-masuk';

        $this->load->model('SuratMasuk_model');
        $this->load->model('Notification_model');
        $this->load->model('Aksi_model');
        $this->load->model('SuratMasukAksi_model');
        $this->load->model('SuratMasukAlamatAksi_model');
        $this->load->model('FileManagement_model');

        $this->load->library('Base64fileUploads');
    }

    function index()
    {
        $aksiList = Aksi_model::select(['id', 'nama'])->get()->toArray();
        $this->page_data['aksiList'] = $aksiList;

        $this->load->view('suratMasuk/list', $this->page_data);
    }

    function generate_qrcode($data, $filename)
    {
        /* Load QR Code Library */
        $this->load->library('ciqrcode');

        /* Data */
        $hex_data = bin2hex($data);
        // $save_name = $hex_data . '.png';
        $save_name = $filename;

        /* QR Code File Directory Initialize */
        $dir = 'app-assets/upload/suratMasukParafQRCode/';
        if (!file_exists($dir)) {
            mkdir($dir, 0775, true);
        }

        /* QR Configuration  */
        $config['cacheable'] = true;
        $config['imagedir'] = $dir;
        $config['quality'] = true;
        $config['size'] = '1024';
        $config['black'] = [255, 255, 255];
        $config['white'] = [255, 255, 255];
        $this->ciqrcode->initialize($config);

        /* QR Data  */
        $params['data'] = $data;
        $params['level'] = 'L';
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $save_name;

        $this->ciqrcode->generate($params);

        /* Return Data */
        $return = [
            'content' => $data,
            'file'    => $dir . $save_name
        ];

        return $return;
    }

    function getById()
    {
        try {
            $id = $this->input->post('id', true);
            $modeView = filter_var($this->input->post('modeView', true), FILTER_VALIDATE_BOOLEAN);

            $query = $this
                ->db
                ->select([
                    'surat_masuk.*',
                    'surat_masuk.tujuan AS tujuan_list',
                    'GROUP_CONCAT(DISTINCT jabatan.nama ORDER BY jabatan.id) AS tujuan',
                    'GROUP_CONCAT(DISTINCT alamatAksi.nama SEPARATOR ", ") AS alamat_aksi',
                    'GROUP_CONCAT(DISTINCT aksi.nama ORDER BY aksi.id SEPARATOR ", ") AS aksi',
                    // 'User.user_name AS disposisi_user_name',
                ], false)
                ->from('surat_masuk')
                ->join('jabatan', 'FIND_IN_SET(jabatan.id, surat_masuk.tujuan) > 0')
                // ->join('User', 'User.user_id = surat_masuk.disposisi_user_id', 'left')
                ->join('surat_masuk_alamat_aksi', 'surat_masuk_alamat_aksi.surat_masuk_id = surat_masuk.id', 'left')
                ->join('jabatan alamatAksi', 'alamatAksi.id = surat_masuk_alamat_aksi.jabatan_id', 'left')
                ->join('surat_masuk_aksi', 'surat_masuk_aksi.surat_masuk_id = surat_masuk.id', 'left')
                ->join('aksi', 'aksi.id = surat_masuk_aksi.aksi_id', 'left')
                ->where("surat_masuk.deleted_at IS NULL")
                ->where("surat_masuk.id", $id)
                ->group_by('surat_masuk.id')
                ->get();

            $data = $query->row();

            if (!$data) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($modeView == false && $data->level_verifikasi == $this->config->item("level_verifikasi_max")) {
                throw new Exception('Surat Masuk Sudah Terverifikasi');
            }

            if ($modeView == false && $data->status_verifikasi == 'SEDANG DIVERIFIKASI') {
                throw new Exception('Surat Masuk Sedang Diverifikasi');
            }

            $file = FCPATH . 'app-assets/upload/suratMasuk/' . $data->filename;
            $fileInfo = pathinfo($file);

            $data->file_extension = $fileInfo['extension'];
            $data->file_mime = mime_content_type($file);

            if ($modeView) {

                $level_verifikasi = $data->level_verifikasi;
                $level_verifikasi_max = $this->config->item("level_verifikasi_max");

                if ($data->status_verifikasi == 'SEDANG DIVERIFIKASI' || ($data->status_verifikasi == 'DITOLAK' && $level_verifikasi > 0)) {
                    $level_verifikasi = $data->level_verifikasi - 1;
                }
                elseif ($data->status_verifikasi == 'TERVERIFIKASI') {
                    $level_verifikasi = $level_verifikasi_max;
                }

                $data->verif_progress = $level_verifikasi / $level_verifikasi_max * 100;

                $query = $this->db
                    ->select([
                        'Log.log_id',
                        'Log.log_action',
                        'Log.log_name',
                        'Log.log_user',
                        'Log.log_created',
                        'Log.log_data',
                        'Log.subject_id',
                    ])
                    ->where('Log.subject_id', $id)
                    ->where('Log.log_name', 'SURAT MASUK')
                    ->order_by('Log.log_created', 'desc')
                    // ->group_by('org.id')
                    ->get("Log");

                $data->log = $query->result();

                $userId = logged('user_id');
                $modulId = $data->id;
                $sql = "SELECT id FROM notification WHERE user_id = $userId AND modul_type = 'Surat Masuk' AND modul_id = $modulId AND read_at IS NULL";
                $query = $this->db->query($sql);

                $readAt = Carbon::now()->format('Y-m-d H:i:s');
                if ($query->num_rows() > 0) {
                    if ($data->status_verifikasi != 'TERVERIFIKASI' && logged('user_type') != '1') {
                        $this->log_model->logInsert([
                            'type'    => 'SURAT MASUK',
                            'action'  => 'Lihat Detail Surat Masuk',
                            'id'      => $id,
                            'logData' => null,
                        ]);
                    }

                    $sql = "UPDATE notification SET read_at = '$readAt' WHERE user_id = $userId AND modul_type = 'Surat Masuk' AND modul_id = $modulId AND read_at IS NULL";
                    $query = $this->db->query($sql);

                    $this->pushNotif('suratMasukViewBroadcast', [
                        'user_id' => logged('user_id'),
                        'data'    => $data,
                    ]);
                }
            }
            else {
                $data->tujuan = explode(',', $data->tujuan_list);
            }

            $data->file = $data->filename;

            echo json_encode([
                "error" => false,
                'data'  => $data
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function verifikasi()
    {
        try {
            $id = $this->input->post('id', true);
            $catatan = $this->input->post('catatan', true);
            $paraf = $this->input->post('paraf', false);
            $fieldParaf = null;
            $saveNotif = false;

            $this->db->trans_begin();

            $suratMasuk = SuratMasuk_model::whereId($id)->first();

            if (!$suratMasuk) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($suratMasuk->status_verifikasi == 'TERVERIFIKASI') {
                throw new Exception('Surat Masuk Sudah Terverifikasi');
            }

            if ($suratMasuk->status_verifikasi == 'BARU' || ($suratMasuk->status_verifikasi == 'DITOLAK' && $suratMasuk->level_verifikasi == 0)) {
                $action = 'Pengajuan Verifikasi Surat Masuk';
            }
            else {
                $action = 'Verifikasi Surat Masuk Ke-' . $suratMasuk->level_verifikasi;
                $fieldParaf = 'paraf' . $suratMasuk->level_verifikasi;
            }

            $suratMasuk->level_verifikasi += 1;

            if ($suratMasuk->level_verifikasi > $this->config->item("level_verifikasi_max")) {
                $suratMasuk->level_verifikasi = $this->config->item("level_verifikasi_max");
                $suratMasuk->status_verifikasi = 'TERVERIFIKASI';
                $saveNotif = true;
            }
            else {
                $suratMasuk->status_verifikasi = 'SEDANG DIVERIFIKASI';
                $saveNotif = true;
            }

            $suratMasuk->catatan = $catatan ?? null;

            if ($paraf && $fieldParaf) {
                $base64file = new Base64fileUploads();
                $parafFile = $base64file->du_uploads('app-assets/upload/suratMasukParaf/', explode(',', $paraf)[1]);

                $path = base_url() . 'app-assets/upload/suratMasukParaf/' . $parafFile['file_name'];
                $qr = $this->generate_qrcode($path, $parafFile['file_name']);

                $suratMasuk->{$fieldParaf} = $parafFile['file_name'];
            }

            $suratMasuk->save();

            $this->log_model->logInsert([
                'type'    => 'SURAT MASUK',
                'action'  => $action,
                'id'      => $suratMasuk->id,
                'logData' => $suratMasuk,
            ]);

            if ($saveNotif) {
                if ($suratMasuk->status_verifikasi == 'TERVERIFIKASI') {
                    $tujuan = $suratMasuk->tujuan;
                    $query = $this->db
                        ->select([
                            'User.user_id'
                        ])
                        ->where("FIND_IN_SET(jabatan_id,'$tujuan') > 0")
                        ->get("User");

                    foreach ($query->result() as $user) {
                        $this->db->insert("notification", [
                            'user_id'    => $user->user_id,
                            'modul_type' => 'Surat Masuk',
                            'modul_id'   => $suratMasuk->id,
                            'message'    => 'Surat masuk sudah terverifikasi',
                            'read_at'    => null,
                            'created_at' => $suratMasuk->updated_at,
                        ]);
                    }
                }
                else {
                    $query = $this->db
                        ->select([
                            'User.user_id'
                        ])
                        ->where([
                            "User.verifikator_level =" => $suratMasuk->level_verifikasi
                        ])
                        ->get("User");

                    foreach ($query->result() as $user) {
                        $this->db->insert("notification", [
                            'user_id'    => $user->user_id,
                            'modul_type' => 'Surat Masuk',
                            'modul_id'   => $suratMasuk->id,
                            'message'    => 'Surat masuk memerlukan verifikasi Anda',
                            'read_at'    => null,
                            'created_at' => $suratMasuk->updated_at,
                        ]);
                    }
                }
            }

            $this->db->trans_commit();

            if ($suratMasuk->status_verifikasi == 'TERVERIFIKASI') {
                $this->pushNotif('suratMasukVerifCompletedBroadcast', [
                    'user_id'   => logged('user_id'),
                    'modeVerif' => 'verif',
                    'data'      => $suratMasuk,
                ]);
            }
            else {
                $this->pushNotif('suratMasukVerifBroadcast', [
                    'user_id'   => logged('user_id'),
                    'modeVerif' => 'verif',
                    'data'      => $suratMasuk,
                ]);
            }

            echo json_encode([
                "error"   => false,
                'data'    => $suratMasuk,
                'message' => 'Verifikasi Data Sukses'
            ]);
        } catch (\Exception $e) {
            $this->db->trans_rollback();
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function pushNotif($eventName, $data)
    {
        $client = new \ElephantIO\Client(new \ElephantIO\Engine\SocketIO\Version2X($this->config->item("socket_url_local"), [
            'headers' => [
                'X-My-Header: websocket rocks',
                'Authorization: Bearer 12b3c4d5e6f7g8h9i'
            ],
            'context' => ['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]
        ]));
        
        $client->initialize();
        $client->emit($eventName, $data);

        $client->close();
    }

    function reject()
    {
        try {
            $id = $this->input->post('id', true);
            $catatan = $this->input->post('catatan', true);
            $saveNotif = false;

            $suratMasuk = SuratMasuk_model::whereId($id)->first();

            if (!$suratMasuk) {
                throw new Exception('Data tidak ditemukan');
            }

            $action = 'Penolakan Verifikasi Surat Masuk';

            if ($suratMasuk->level_verifikasi > 0) {
                $suratMasuk->level_verifikasi -= 1;
            }

            if ($suratMasuk->level_verifikasi > 0) {
                $saveNotif = true;
            }

            $suratMasuk->status_verifikasi = 'DITOLAK';
            $suratMasuk->catatan = $catatan;
            $suratMasuk->save();

            $this->log_model->logInsert([
                'type'    => 'SURAT MASUK',
                'action'  => $action,
                'id'      => $suratMasuk->id,
                'logData' => $suratMasuk,
            ]);

            if ($saveNotif) {
                $query = $this->db
                    ->select([
                        'User.user_id'
                    ])
                    ->where([
                        "User.verifikator_level =" => $suratMasuk->level_verifikasi
                    ])
                    ->get("User");

                foreach ($query->result() as $user) {
                    $this->db->insert("notification", [
                        'user_id'    => $user->user_id,
                        'modul_type' => 'Surat Masuk',
                        'modul_id'   => $suratMasuk->id,
                        'message'    => 'Surat masuk memerlukan verifikasi Anda',
                        'read_at'    => null,
                        'created_at' => $suratMasuk->updated_at,
                    ]);
                }
            }

            $this->pushNotif('suratMasukVerifBroadcast', [
                'user_id'   => logged('user_id'),
                'modeVerif' => 'reject',
                'data'      => $suratMasuk,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratMasuk,
                'message' => 'Penolakan Surat Masuk Sudah Diproses'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function delete()
    {
        try {
            $id = $this->input->post('id', true);

            $suratMasuk = SuratMasuk_model::whereId($id)->first();

            if (!$suratMasuk) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($suratMasuk->level_verifikasi == $this->config->item("level_verifikasi_max")) {
                throw new Exception('Surat Masuk Sudah Terverifikasi');
            }

            if ($suratMasuk->status_verifikasi != 'BARU') {
                throw new Exception('Data sedang dalam proses verifikasi');
            }

            $suratMasuk->delete();

            $action = 'Hapus Data Surat Masuk';

            $this->log_model->logInsert([
                'type'    => 'SURAT MASUK',
                'action'  => $action,
                'id'      => $suratMasuk->id,
                'logData' => $suratMasuk,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratMasuk,
                'message' => 'Data Surat Masuk Sudah Dihapus'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_list()
    {
        $period = $this->input->post("period");
        $month = $this->input->post("month");
        $year = $this->input->post("year");
        $sDate = $this->input->post("sDate");
        $eDate = $this->input->post("eDate");

        $w = [];

        if ($period == 'harian') {
            array_push($w, "DATE(surat_masuk.tgl_surat) = '$sDate'");
        }
        else if ($period == 'bulanan') {
            array_push($w, "MONTH(surat_masuk.tgl_surat) = $month");
            array_push($w, "YEAR(surat_masuk.tgl_surat) = $year");
        }
        else if ($period == 'tahunan') {
            array_push($w, "YEAR(surat_masuk.tgl_surat) = $year");
        }
        else if ($period == 'manual') {
            array_push($w, "DATE(surat_masuk.tgl_surat) >= '$sDate'");
            array_push($w, "DATE(surat_masuk.tgl_surat) <= '$eDate'");
        }

        array_push($w, "surat_masuk.deleted_at IS NULL");

        $verifikator_level = logged('verifikator_level') ?? 99;
        $jabatan_id = logged('jabatan_id') ?? 999999;

        if (logged('user_type') != '1') {
            // array_push($w, "(surat_masuk.level_verifikasi = '".logged('verifikator_level')."' OR (surat_masuk.status_verifikasi = 'SEDANG DIVERIFIKASI' AND surat_masuk.level_verifikasi = '".logged('verifikator_level')."' - 1))");

            // array_push($w, "((surat_masuk.status_verifikasi = 'SEDANG DIVERIFIKASI' AND surat_masuk.level_verifikasi = $verifikator_level - 1) OR (surat_masuk.level_verifikasi = $verifikator_level))");

            array_push($w, "surat_masuk.level_verifikasi = $verifikator_level");
        }

        $where = '(' . implode(' AND ', $w) . ')';

        if (logged('user_type') != '1') {
            $userId = logged('user_id');
            $where .= " OR (surat_masuk.disposisi_user_id = $userId) ";
            $where .= " OR (FIND_IN_SET($jabatan_id, surat_masuk.tujuan) AND (surat_masuk.status_verifikasi = 'TERVERIFIKASI' OR surat_masuk.status_verifikasi = 'DISPOSISI')) ";
        }

        $select = "SELECT
                surat_masuk.id AS DT_RowId,
                surat_masuk.id,
                surat_masuk.no_agenda,
                surat_masuk.no_surat,
                surat_masuk.tgl_surat,
                surat_masuk.tgl_terima,
                surat_masuk.pengirim,
                surat_masuk.perihal,
                GROUP_CONCAT(jabatan.nama ORDER BY jabatan.id) tujuan,
                surat_masuk.kerahasiaan,
                surat_masuk.level_verifikasi,
                surat_masuk.status_verifikasi,
                surat_masuk.catatan,
                surat_masuk.created_at,
                surat_masuk.updated_at,
                surat_masuk.deleted_at
            FROM
                surat_masuk
            JOIN jabatan ON FIND_IN_SET(jabatan.id, surat_masuk.tujuan) > 0
            WHERE $where GROUP BY surat_masuk.id";

        // dd($select);

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['id'];
        });

        $dt->edit('tgl_surat', function ($data) {
            return Carbon::parse($data['tgl_surat'])->format('d M Y');
        });

        echo $dt->generate();
    }

    function process_save2()
    {
        try {
            $filesCount = count($_FILES['files']['name']);

            for ($i = 0; $i < $filesCount; $i ++) {
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                $now = DateTime::createFromFormat('U.u', microtime(true));
                $now = $now->format("m-d-Y H:i:s.u");

                $config['upload_path'] = FCPATH . 'app-assets/upload/suratMasuk/';
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['file_name'] = md5($now);
                $config['max_size'] = 10485760; // 10MB

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $fileData = $this->upload->data();
                    // $uploadData[$i]['file_name'] = $fileData['file_name'];
                    // $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
                else {
                    $error = $this->upload->display_errors();
                    throw new Exception($error);
                    die;
                }
            }

            echo json_encode([
                "error"   => false,
                'message' => 'adad',
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function process_save()
    {
        try {
            $id = $this->input->post('id', true);
            $tgl_surat = $this->input->post('tgl_surat', true);
            $tgl_terima = $this->input->post('tgl_terima', true);
            $no_surat = $this->input->post('no_surat', true);
            $pengirim = $this->input->post('pengirim', true);
            $perihal = $this->input->post('perihal', true);
            $tujuan = $this->input->post('tujuan', true);
            $kerahasiaan = $this->input->post('kerahasiaan', true);
            $catatan = $this->input->post('catatan', true);

            $now = DateTime::createFromFormat('U.u', microtime(true));
            $now = $now->format("m-d-Y H:i:s.u");

            $config['upload_path'] = FCPATH . 'app-assets/upload/suratMasuk/';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['file_name'] = md5($now);
            $config['max_size'] = 10485760; // 10MB
            // $config['max_width']            = 1080;
            // $config['max_height']           = 1080;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                throw new Exception($error);
            }
            else {
                $uploaded_data = $this->upload->data();

                unset($trx);

                $trx = [
                    'id'          => $id,
                    'no_agenda'   => $this->getNoAgenda(),
                    'tgl_terima'  => $tgl_terima,
                    'tgl_surat'   => $tgl_surat,
                    'no_surat'    => $no_surat,
                    'pengirim'    => $pengirim,
                    'perihal'     => $perihal,
                    'tujuan'      => $tujuan,
                    'kerahasiaan' => $kerahasiaan,
                    'catatan'     => $catatan,
                    'filename'    => $uploaded_data['file_name'],
                ];

                if (!$id) {
                    $action = 'Tambah data surat masuk';
                    $msg = 'Tambah data sukses';
                }
                else {
                    $action = 'Edit data surat masuk';
                    $msg = 'Edit data sukses';
                }

                // dd($trx);

                $this->db->trans_begin();

                $suratMasuk = SuratMasuk_model::updateOrCreate([
                    'id' => $id
                ], $trx);

                unset($trx['id']);

                $this->log_model->logInsert([
                    'type'    => 'SURAT MASUK',
                    'action'  => $action,
                    'id'      => $suratMasuk->id,
                    'logData' => $suratMasuk,
                ]);

                $this->db->trans_commit();

                echo json_encode([
                    "error"   => false,
                    'data'    => $suratMasuk,
                    'message' => $msg,
                ]);
            }
        } catch (\Exception $e) {
            $this->db->trans_rollback();

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function getNoAgenda()
    {
        $sql = "SELECT no_agenda FROM surat_masuk ORDER BY no_agenda DESC FOR UPDATE";
        $query = $this->db->query($sql);
        $record = $query->first_row();

        $yearMonthNow = date('ym');

        if (!is_null($record)) {
            $yearMonth = substr($record->no_agenda, 0, 4);
            if ($yearMonth !== $yearMonthNow) {
                $yearMonth = $yearMonthNow;
                $lastCounter = 0;
            }
            else {
                $lastCounter = intval(substr($record->no_agenda, - 4));
            }
        }
        else {
            $yearMonth = $yearMonthNow;
            $lastCounter = 0;
        }

        $newCounter = ($yearMonth == $yearMonthNow) ? ($lastCounter + 1) : 1;
        $nextNumber = sprintf('%1$s%2$04d', $yearMonth, $newCounter);

        return $nextNumber;
    }

    function disposisi()
    {
        try {
            $id = $this->input->post('id', true);
            $alamat_aksi = $this->input->post('alamat_aksi', true);
            $tindakan_segera = $this->input->post('tindakan_segera', true);
            $catatan_disposisi = $this->input->post('catatan_disposisi', true);
            $redisposisi = $this->input->post('redisposisi', true) == 'true' ? true : false;
            $paraf = $this->input->post('paraf', false);
            // $disposisi_user_id = $this->input->post('disposisi_user_id', true);
            // $tindakan_biasa = $this->input->post('tindakan_biasa', true);

            $this->db->trans_begin();

            $suratMasuk = SuratMasuk_model::whereId($id)->first();

            if (!$suratMasuk) {
                throw new Exception('Data tidak ditemukan');
            }

            $suratMasuk->tgl_disposisi = Carbon::now()->format('Y-m-d');
            $suratMasuk->catatan_disposisi = $catatan_disposisi;
            $suratMasuk->status_verifikasi = 'DISPOSISI';

            if ($paraf) {
                $base64file = new Base64fileUploads();
                $parafFile = $base64file->du_uploads('app-assets/upload/suratMasukParaf/', explode(',', $paraf)[1]);

                $path = base_url() . 'app-assets/upload/suratMasukParaf/' . $parafFile['file_name'];
                $qr = $this->generate_qrcode($path, $parafFile['file_name']);

                $suratMasuk->paraf5 = $parafFile['file_name'];
            }
            // $suratMasuk->tindakan_segera = $tindakan_segera;
            // $suratMasuk->disposisi_user_id = $disposisi_user_id;
            // $suratMasuk->tindakan_biasa = $tindakan_biasa;
            $suratMasuk->save();

            $suratMasuk->alamatAksi()->delete();
            foreach ($alamat_aksi as $row) {
                SuratMasukAlamatAksi_model::create([
                    'surat_masuk_id' => $suratMasuk->id,
                    'jabatan_id'     => $row
                ]);
            }

            $suratMasuk->aksi()->delete();
            foreach ($tindakan_segera as $row) {
                SuratMasukAksi_model::create([
                    'surat_masuk_id' => $suratMasuk->id,
                    'aksi_id'        => $row
                ]);
            }

            if (!$redisposisi) {
                $filename = $suratMasuk->filename;
                $sourcePath = FCPATH . 'app-assets/upload/suratMasuk/'.$filename;
                $destinationPath = FCPATH . 'app-assets/upload/fileManagement/'.$filename;


                $pathinfo = pathinfo($sourcePath);
                $fileext = '.' . $pathinfo['extension'];
                $filetype = mime_content_type($sourcePath);

                $filesize = filesize($sourcePath);
                $client_filename = $filename;

                $trx = [
                    'category_id'     => 6,
                    'deskripsi'       => $suratMasuk->perihal,
                    'is_public'       => 1,
                    'client_filename' => $client_filename,
                    'filename'        => $filename,
                    'filesize'        => $filesize,
                    'filetype'        => $filetype,
                    'fileext'         => $fileext,
                ];

                $fileManagement = FileManagement_model::updateOrCreate([
                    'id' => null
                ], $trx);

                copy($sourcePath, $destinationPath);
            }

            $this->log_model->logInsert([
                'type'    => 'SURAT MASUK',
                'action'  => 'Disposisi Surat Masuk',
                'id'      => $suratMasuk->id,
                'logData' => $suratMasuk,
            ]);

            // $saveNotif = true;
            // if ($saveNotif) {
            //     $query = $this->db
            //         ->select([
            //             'User.user_id'
            //         ])
            //         ->where([
            //             "User.user_id =" => $disposisi_user_id
            //         ])
            //         ->get("User");
            //
            //     foreach ($query->result() as $user) {
            //         $this->db->insert("notification", [
            //             'user_id'    => $user->user_id,
            //             'modul_type' => 'Surat Masuk',
            //             'modul_id'   => $suratMasuk->id,
            //             'message'    => 'Disposisi surat masuk',
            //             'read_at'    => null,
            //             'created_at' => $suratMasuk->updated_at,
            //         ]);
            //     }
            // }

            $this->db->trans_commit();

            // $this->pushNotif('suratMasukDisposisiBroadcast', [
            //     'user_id'   => logged('user_id'),
            //     'modeVerif' => 'disposisi',
            //     'data'      => $suratMasuk,
            // ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratMasuk,
                'message' => 'Disposisi surat masuk sudah diproses',
            ]);
        } catch (\Exception $e) {
            $this->db->trans_rollback();

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }
}
