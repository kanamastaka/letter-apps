<?php

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class SuratKeluar extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Surat Keluar';
        $this->page_data['page']->menu = 'surat-keluar';

        $this->load->model('SuratKeluar_model');
        $this->load->model('Notification_model');
    }

    function index()
    {
        $this->load->view('suratKeluar/list', $this->page_data);
    }

    function getById()
    {
        try {
            $id = $this->input->post('id', true);
            $modeView = filter_var($this->input->post('modeView', true), FILTER_VALIDATE_BOOLEAN);

            $data = SuratKeluar_model::whereId($id)->first();

            if (!$data) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($modeView == false && $data->level_verifikasi == $this->config->item("level_verifikasi_max")) {
                throw new Exception('Surat Keluar Sudah Terverifikasi');
            }

            if ($modeView == false && $data->status_verifikasi == 'SEDANG DIVERIFIKASI') {
                throw new Exception('Surat Keluar Sedang Diverifikasi');
            }

            $data->isi = str_replace('<p><br></p>', '', $data->isi);

            if ($modeView) {

                $level_verifikasi = $data->level_verifikasi;
                $level_verifikasi_max = $this->config->item("level_verifikasi_max");

                if ($data->status_verifikasi == 'SEDANG DIVERIFIKASI' || ($data->status_verifikasi == 'DITOLAK' && $level_verifikasi > 0)) {
                    $level_verifikasi = $data->level_verifikasi - 1;
                }
                elseif ($data->status_verifikasi == 'TERVERIFIKASI') {
                    $level_verifikasi = $level_verifikasi_max;
                }

                $data->verif_progress = $level_verifikasi / $level_verifikasi_max * 100;

                $query = $this->db
                    ->select([
                        'Log.log_id',
                        'Log.log_action',
                        'Log.log_name',
                        'Log.log_user',
                        'Log.log_created',
                        'Log.log_data',
                        'Log.subject_id',
                    ])
                    ->where('Log.subject_id', $id)
                    ->where('Log.log_name', 'SURAT KELUAR')
                    ->order_by('Log.log_created', 'desc')
                    // ->group_by('org.id')
                    ->get("Log");

                $data->log = $query->result();

                $userId = logged('user_id');
                $modulId = $data->id;
                $sql = "SELECT id FROM notification WHERE user_id = $userId AND modul_type = 'Surat Keluar' AND modul_id = $modulId AND read_at IS NULL";
                $query = $this->db->query($sql);

                $readAt = Carbon::now()->format('Y-m-d H:i:s');
                if ($query->num_rows() > 0) {
                    if ($data->status_verifikasi != 'TERVERIFIKASI' && logged('user_type') != '1') {
                        $this->log_model->logInsert([
                            'type'    => 'SURAT KELUAR',
                            'action'  => 'Lihat Detail Surat Keluar',
                            'id'      => $id,
                            'logData' => null,
                        ]);
                    }

                    $sql = "UPDATE notification SET read_at = '$readAt' WHERE user_id = $userId AND modul_type = 'Surat Keluar' AND modul_id = $modulId AND read_at IS NULL";
                    $query = $this->db->query($sql);

                    $this->pushNotif('suratKeluarViewBroadcast', [
                        'user_id' => logged('user_id'),
                        'data'    => $data,
                    ]);
                }
            }

            echo json_encode([
                "error" => false,
                'data'  => $data
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function verifikasi()
    {
        try {
            $id = $this->input->post('id', true);
            $catatan = $this->input->post('catatan', true);
            $saveNotif = false;

            $this->db->trans_begin();

            $suratKeluar = SuratKeluar_model::whereId($id)->first();

            if (!$suratKeluar) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($suratKeluar->status_verifikasi == 'TERVERIFIKASI') {
                throw new Exception('Surat Keluar Sudah Terverifikasi');
            }

            if ($suratKeluar->status_verifikasi == 'BARU' || ($suratKeluar->status_verifikasi == 'DITOLAK' && $suratKeluar->level_verifikasi == 0)) {
                $action = 'Pengajuan Verifikasi Surat Keluar';
            }
            else {
                $action = 'Verifikasi Surat Keluar Ke-' . $suratKeluar->level_verifikasi;
            }
            $suratKeluar->level_verifikasi += 1;

            if ($suratKeluar->level_verifikasi > $this->config->item("level_verifikasi_max")) {
                $suratKeluar->level_verifikasi = $this->config->item("level_verifikasi_max");
                $suratKeluar->status_verifikasi = 'TERVERIFIKASI';
            }
            else {
                $suratKeluar->status_verifikasi = 'SEDANG DIVERIFIKASI';
                $saveNotif = true;
            }

            $suratKeluar->catatan = $catatan ?? null;

            $suratKeluar->save();

            $this->log_model->logInsert([
                'type'    => 'SURAT KELUAR',
                'action'  => $action,
                'id'      => $suratKeluar->id,
                'logData' => $suratKeluar,
            ]);

            if ($saveNotif) {
                $query = $this->db
                    ->select([
                        'User.user_id'
                    ])
                    ->where([
                        "User.verifikator_level =" => $suratKeluar->level_verifikasi
                    ])
                    ->get("User");

                foreach ($query->result() as $user) {
                    $this->db->insert("notification", [
                        'user_id'    => $user->user_id,
                        'modul_type' => 'Surat Keluar',
                        'modul_id'   => $suratKeluar->id,
                        'message'    => 'Surat keluar memerlukan verifikasi Anda',
                        'read_at'    => null,
                        'created_at' => $suratKeluar->updated_at,
                    ]);
                }
            }

            $this->db->trans_commit();

            $this->pushNotif('suratKeluarVerifBroadcast', [
                'user_id'   => logged('user_id'),
                'modeVerif' => 'verif',
                'data'      => $suratKeluar,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratKeluar,
                'message' => 'Verifikasi Data Sukses'
            ]);
        } catch (\Exception $e) {
            $this->db->trans_rollback();
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function pushNotif($eventName, $data)
    {
        $client = new \ElephantIO\Client(new \ElephantIO\Engine\SocketIO\Version2X($this->config->item("socket_url_local"), [
            'headers' => [
                'X-My-Header: websocket rocks',
                'Authorization: Bearer 12b3c4d5e6f7g8h9i'
            ],
            'context' => ['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]
        ]));

        $client->initialize();
        $client->emit($eventName, $data);

        $client->close();
    }

    function reject()
    {
        try {
            $id = $this->input->post('id', true);
            $catatan = $this->input->post('catatan', true);
            $saveNotif = false;

            $suratKeluar = SuratKeluar_model::whereId($id)->first();

            if (!$suratKeluar) {
                throw new Exception('Data tidak ditemukan');
            }

            $action = 'Penolakan Verifikasi Surat Keluar';

            if ($suratKeluar->level_verifikasi > 0) {
                $suratKeluar->level_verifikasi -= 1;
            }

            if ($suratKeluar->level_verifikasi > 0) {
                $saveNotif = true;
            }

            $suratKeluar->status_verifikasi = 'DITOLAK';
            $suratKeluar->catatan = $catatan;
            $suratKeluar->save();

            $this->log_model->logInsert([
                'type'    => 'SURAT KELUAR',
                'action'  => $action,
                'id'      => $suratKeluar->id,
                'logData' => $suratKeluar,
            ]);

            if ($saveNotif) {
                $query = $this->db
                    ->select([
                        'User.user_id'
                    ])
                    ->where([
                        "User.verifikator_level =" => $suratKeluar->level_verifikasi
                    ])
                    ->get("User");

                foreach ($query->result() as $user) {
                    $this->db->insert("notification", [
                        'user_id'    => $user->user_id,
                        'modul_type' => 'Surat Keluar',
                        'modul_id'   => $suratKeluar->id,
                        'message'    => 'Surat keluar memerlukan verifikasi Anda',
                        'read_at'    => null,
                        'created_at' => $suratKeluar->updated_at,
                    ]);
                }
            }

            $this->pushNotif('suratKeluarVerifBroadcast', [
                'user_id'   => logged('user_id'),
                'modeVerif' => 'reject',
                'data'      => $suratKeluar,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratKeluar,
                'message' => 'Penolakan Surat Keluar Sudah Diproses'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function delete()
    {
        try {
            $id = $this->input->post('id', true);

            $suratKeluar = SuratKeluar_model::whereId($id)->first();

            if (!$suratKeluar) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($suratKeluar->level_verifikasi == $this->config->item("level_verifikasi_max")) {
                throw new Exception('Surat Keluar Sudah Terverifikasi');
            }

            if ($suratKeluar->status_verifikasi != 'BARU') {
                throw new Exception('Data sedang dalam proses verifikasi');
            }

            $suratKeluar->delete();

            $action = 'Hapus Data Surat Keluar';

            $this->log_model->logInsert([
                'type'    => 'SURAT KELUAR',
                'action'  => $action,
                'id'      => $suratKeluar->id,
                'logData' => $suratKeluar,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratKeluar,
                'message' => 'Data Surat Keluar Sudah Dihapus'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_list()
    {
        // $sdate = $this->input->post("sdate");
        // $edate = $this->input->post("edate");

        $w = [];

        array_push($w, "surat_keluar.deleted_at IS NULL");

        $verifikator_level = logged('verifikator_level') ?? 99;
        if (logged('user_type') != '1') {
            array_push($w, "surat_keluar.level_verifikasi = $verifikator_level");
        }

        $where = implode(' AND ', $w);

        $select = "SELECT
                surat_keluar.id AS DT_RowId,
                surat_keluar.id,
                surat_keluar.tgl_surat,
                surat_keluar.nama,
                surat_keluar.tujuan,
                surat_keluar.kerahasiaan,
                surat_keluar.level_verifikasi,
                surat_keluar.status_verifikasi,
                surat_keluar.catatan,
                surat_keluar.isi,
                surat_keluar.created_at,
                surat_keluar.updated_at,
                surat_keluar.deleted_at
            FROM
                surat_keluar
            WHERE $where";

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['id'];
        });

        $dt->edit('tgl_surat', function ($data) {
            return Carbon::parse($data['tgl_surat'])->format('d M Y');
        });

        echo $dt->generate();
    }

    function process_save()
    {
        try {
            $id = $this->input->post('id', true);
            $tgl_surat = $this->input->post('tgl_surat', true);
            $nama = $this->input->post('nama', true);
            $tujuan = $this->input->post('tujuan', true);
            $kerahasiaan = $this->input->post('kerahasiaan', true);
            $isi = $this->input->post('isi', false);
            // $isi = htmlentities($isi);

            // dd(htmlentities($isi), html_entity_decode(htmlentities($isi)));

            // $now = DateTime::createFromFormat('U.u', microtime(true));
            // $now = $now->format("m-d-Y H:i:s.u");
            //
            // $config['upload_path'] = FCPATH . 'app-assets/upload/suratKeluar/';
            // $config['allowed_types'] = 'pdf|doc|docx';
            // $config['file_name'] = md5($now);
            // $config['max_size'] = 10485760; // 10MB
            // $config['max_width']            = 1080;
            // $config['max_height']           = 1080;

            // $this->load->library('upload', $config);

            // if (!$this->upload->do_upload('file')) {
            //     $error = $this->upload->display_errors();
            //     throw new Exception($error);
            // }
            // else {
            //     $uploaded_data = $this->upload->data();

            unset($trx);

            $trx = [
                'id'          => $id,
                'tgl_surat'   => $tgl_surat,
                'nama'        => $nama,
                'tujuan'      => $tujuan,
                'kerahasiaan' => $kerahasiaan,
                'isi'         => $isi,
                // 'filename'    => $uploaded_data['file_name'],
            ];

            if (!$id) {
                $action = 'Tambah data surat keluar';
                $msg = 'Tambah data sukses';
            }
            else {
                $action = 'Edit data surat keluar';
                $msg = 'Edit data sukses';
            }

            $suratKeluar = SuratKeluar_model::updateOrCreate([
                'id' => $id
            ], $trx);

            unset($trx['id']);

            $this->log_model->logInsert([
                'type'    => 'SURAT KELUAR',
                'action'  => $action,
                'id'      => $suratKeluar->id,
                'logData' => $suratKeluar,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $suratKeluar,
                'message' => $msg,
            ]);
            // }
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }
}
