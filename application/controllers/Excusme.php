<?php

class Excusme extends CI_Controller {

    var $sess;
    var $params;

    public function __construct()
    {
        parent::__construct();
    }

    function dologin()
    {
        try {
            $username = $this->input->post('username', true);
            $userpass = $this->input->post('password', true);

            if (!$username) {
                echo json_encode(["error" => true, "message" => "Masukkan Username Anda !"]);

                return;
            }

            if (!$userpass) {
                echo json_encode(["error" => true, "message" => "Masukkan Password Anda !"]);

                return;
            }

            $datalogin = $this->crud_model->checklogin($username, $userpass);

            if (isset($datalogin->user_id) && $datalogin->user_id != "") {

                if (!$datalogin->email) {
                    echo json_encode(["error" => true, "message" => "Email pengguna tidak ditemukan!"]);
                    exit;
                }

                $otpId = $this->otp_model->makeOtp($datalogin->email);
                echo json_encode([
                    "error"    => false,
                    'redirect' => 'otp/' . $otpId,
                ]);
            }
            else {
                echo json_encode(["error" => true, "message" => "User tidak ditemukan!"]);
                return;
            }
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function dologinBak()
    {
        $username = $this->input->post('username', true);
        $userpass = $this->input->post('password', true);

        if (!$username) {
            echo json_encode(["error" => true, "message" => "Masukkan Username Anda !"]);

            return;
        }

        if (!$userpass) {
            echo json_encode(["error" => true, "message" => "Masukkan Password Anda !"]);

            return;
        }

        $datalogin = $this->crud_model->checklogin($username, $userpass);

        if (isset($datalogin->user_id) && $datalogin->user_id != "") {
            $time = time();

            // encypting userid and password with current time $time
            $login_token = sha1($datalogin->user_id . $datalogin->user_password . $time);

            $array = [
                'login'       => true,
                // saving encrypted userid and password as token in session
                'login_token' => $login_token,
                'logged'      => [
                    'id'                => $datalogin->user_id,
                    'user_type'         => $datalogin->user_type,
                    'user_type_name'    => $datalogin->user_type_name,
                    'user_name'         => $datalogin->user_name,
                    'verifikator_level' => $datalogin->verifikator_level,
                    'jabatan_id'        => $datalogin->jabatan_id,
                    'time'              => $time,
                ]
            ];

            $this->session->set_userdata($array);
            // $this->session->set_userdata($this->config->item('session_name'), serialize($datalogin));
        }
        else {
            echo json_encode(["error" => true, "message" => "User not found!"]);

            return;
        }

        $insertlog = $this->log_model->insertlog($datalogin->user_name, "LOGIN", "LOGIN", $datalogin->user_id);
        echo json_encode(["error" => false, "redirect" => base_url() . "dash"]);
    }
}
