<?php

use Carbon\Carbon;

class FileManagement extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Manajemen File';
        $this->page_data['page']->menu = 'fileManagement';

        $this->load->model('FileManagement_model');
        $this->load->model('FileManagementCategory_model');
        $this->load->model('FileManagementShare_model');
    }

    function index()
    {
        $this->load->view('fileManagement/list', $this->page_data);
    }

    function get_list()
    {
        $sdate = $this->input->post("sdate");
        $edate = $this->input->post("edate");
        $userId = logged('user_id');

        $condition[] = "file_management.deleted_at IS NULL";

        if (logged('user_type') != 1) {
            $condition[] = "(file_management.created_by = $userId OR file_management.is_public = 1 OR (file_management.is_public = 0 AND file_management_share.id IS NOT NULL))";
        }

        $w = implode(' AND ', $condition);

        if (logged('user_type') == 1) {
            $select = "SELECT
                file_management.id AS DT_RowId,
                file_management.id,
                file_management.category_id,
                file_management.deskripsi AS deskripsi_text,
                file_management.deskripsi,
                file_management.filename,
                file_management.filesize,
                file_management.filetype,
                file_management.is_public,
                file_management.created_by,
                IF(file_management.is_public=1,'YA','TIDAK') AS is_public_text,
                file_management_category.nama AS category_name,
                User.user_name AS owner
            FROM
                file_management
                JOIN file_management_category ON file_management_category.id = file_management.category_id
                JOIN User ON User.user_id = file_management.created_by
            WHERE $w GROUP BY file_management.id";
        }
        else {
            $select = "SELECT
                file_management.id AS DT_RowId,
                file_management.id,
                file_management.category_id,
                file_management.deskripsi AS deskripsi_text,
                file_management.deskripsi,
                file_management.filename,
                file_management.filesize,
                file_management.filetype,
                file_management.is_public,
                file_management.created_by,
                IF(file_management.is_public=1,'YA','TIDAK') AS is_public_text,
                file_management_category.nama AS category_name,
                User.user_name AS owner
            FROM
                file_management
                JOIN file_management_category ON file_management_category.id = file_management.category_id
                JOIN User ON User.user_id = file_management.created_by
                LEFT JOIN file_management_share ON file_management_share.file_management_id = file_management.id
			        AND file_management_share.user_id = $userId
            WHERE $w GROUP BY file_management.id";
        }

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['DT_RowId'];
        });

        $dt->edit('deskripsi', function ($data) {
            $desc = $data['deskripsi'];
            $href = base_url() . 'app-assets/upload/fileManagement/' . $data['filename'];

            return "<a href='$href' target='_blank'>$desc</a>";
        });

        $dt->edit('filesize', function ($data) {
            return humanFilesize($data['filesize']);
        });

        echo $dt->generate();
    }

    function getById()
    {
        try {
            $id = $this->input->post('id', true);

            $data = FileManagement_model::select([
                'id',
                'category_id',
                'deskripsi',
                'filename',
                'filesize',
                'filetype',
                'is_public',
                'created_by',
            ])
                ->whereId($id)
                ->first();

            if ($data->created_by != logged('user_id') && logged('user_type') != '1') {
                throw new Exception('Tidak diperkenankan mengedit file milik pengguna lain');
            }

            $dataCategory = FileManagementCategory_model::select([
                'id',
                'nama',
            ])
                ->whereId($data->category_id)
                ->first();

            $data = $data->toArray();

            $data['category'] = [
                'id'   => $dataCategory->id,
                'text' => $dataCategory->nama,
            ];

            $data['is_public'] = [
                (string) $data['is_public']
            ];

            echo json_encode([
                "error" => false,
                'data'  => $data
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function getByIdForShare()
    {
        $id = $this->input->post('id', true);

        $data = FileManagement_model::select([
            'id',
        ])
            ->whereId($id)
            ->first();

        $dataCategory = FileManagementShare_model::select([
            'file_management_share.file_management_id',
            'file_management_share.user_id',
        ])->addSelect([
            'User.user_name'
        ])->join('User', 'User.user_id', '=', 'file_management_share.user_id')
            ->where('file_management_id', $data->id)
            ->get();

        $data = $data->toArray();

        foreach ($dataCategory->toArray() as $row) {
            $data['user_list'][] = (string) $row['user_id'];

            $data['user_list_option'][] = [
                'id'   => (string) $row['user_id'],
                'text' => $row['user_name'],
            ];
        }

        echo json_encode([
            "error" => false,
            'data'  => $data
        ]);
    }

    function delete()
    {
        try {
            $id = $this->input->post('id', true);

            $fileManagement = FileManagement_model::whereId($id)->first();

            if (!$fileManagement) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($fileManagement->created_by != logged('user_id') && logged('user_type') != '1') {
                throw new Exception('Tidak diperkenankan menghapus file milik pengguna lain');
            }

            $fileManagement->deleted_by = logged('user_id');
            $fileManagement->save();

            $fileManagement->delete();

            $action = 'Hapus Data File';

            $this->log_model->logInsert([
                'type'    => 'MANAJEMEN FILE',
                'action'  => $action,
                'id'      => $fileManagement->id,
                'logData' => $fileManagement,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $fileManagement,
                'message' => 'Data File Sudah Dihapus'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function share()
    {
        try {
            $id = $this->input->post('id', true);
            $user_list_id = $this->input->post('user_list_id', true);

            $fileManagement = FileManagement_model::whereId($id)->first();

            if (!$fileManagement) {
                throw new Exception('Data tidak ditemukan');
            }

            $fileManagement->fileManagementShare()->delete();

            foreach (explode(',', $user_list_id) as $row) {
                FileManagementShare_model::create([
                    'file_management_id' => $fileManagement->id,
                    'user_id'            => $row
                ]);
            }

            $action = 'Share Data File';

            $this->log_model->logInsert([
                'type'    => 'MANAJEMEN FILE',
                'action'  => $action,
                'id'      => $fileManagement->id,
                'logData' => $fileManagement,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $fileManagement,
                'message' => 'Share Data File Sudah Diproses'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function process_save()
    {
        try {
            $id = $this->input->post('id', true);
            $deskripsi = strtoupper($this->input->post('deskripsi', true));
            $category_id = $this->input->post('category_id', true);
            $is_public = $this->input->post('is_public', true) == '1' ? 1 : 0;

            $now = DateTime::createFromFormat('U.u', microtime(true));
            $now = $now->format("m-d-Y H:i:s.u");

            $config['upload_path'] = FCPATH . 'app-assets/upload/fileManagement/';
            $config['allowed_types'] = 'pdf|doc|docx|jpg|jpeg|png|xls|xlsx';
            $config['file_name'] = md5($now);
            // $config['max_size'] = 104857600; // 100MB
            // $config['max_width']            = 1080;
            // $config['max_height']           = 1080;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                throw new Exception($error);
            }
            else {
                $uploaded_data = $this->upload->data();

                unset($trx);

                $full_path = $uploaded_data['full_path'];
                $client_filename = $uploaded_data['client_name'];
                $filetype = $uploaded_data['file_type'];
                $fileext = $uploaded_data['file_ext'];
                $filename = $uploaded_data['file_name'];
                $filesize = filesize($full_path);

                $trx = [
                    'id'              => $id,
                    'category_id'     => $category_id,
                    'deskripsi'       => $deskripsi,
                    'is_public'       => $is_public,
                    'client_filename' => $client_filename,
                    'filename'        => $filename,
                    'filesize'        => $filesize,
                    'filetype'        => $filetype,
                    'fileext'         => $fileext,
                ];

                if (!$id) {
                    $action = 'Tambah data file';
                    $msg = 'Tambah data sukses';
                }
                else {
                    $action = 'Edit data file';
                    $msg = 'Edit data sukses';
                }

                $this->db->trans_begin();

                $fileManagement = FileManagement_model::updateOrCreate([
                    'id' => $id
                ], $trx);

                unset($trx['id']);

                $this->log_model->logInsert([
                    'type'    => 'MANAJEMEN FILE',
                    'action'  => $action,
                    'id'      => $fileManagement->id,
                    'logData' => $fileManagement,
                ]);

                $this->db->trans_commit();

                echo json_encode([
                    "error"   => false,
                    'data'    => $fileManagement,
                    'message' => $msg,
                ]);
            }
        } catch (\Exception $e) {
            $this->db->trans_rollback();

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_select2_list()
    {
        $json = [];

        $limit = 10;
        $page = $this->input->get("page");

        if (!$page) {
            $page = 0;
        }
        else {
            $page = ($page - 1) * $limit;
        }

        $query = $this->db
            ->like('deskripsi', $this->input->get("q"));
        // ->where('org_id', $this->input->get("orgId"));

        $query = $this->db
            ->select([
                'id AS id',
                'deskripsi as text'
            ])
            ->order_by('deskripsi', 'asc');

        $queryLimit = $query
            ->limit($limit, $page)
            ->get("FileManagement");

        $json['items'] = $queryLimit->result();
        $json['total'] = $query->get("org_apps")->num_rows();

        // dd($json);

        echo json_encode($json);
    }
}
