<?php

class Test extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'CSS';
        $this->page_data['page']->menu = 'test';

        // $this->load->model('Org_model');
    }

    function index()
    {
        $sess = $this->session->userdata($this->config->item("session_name"));

        // for ($i = 1; $i <= 10; $i++) {
        //     // echo $i - 1 . ' # ';
        //     for ($j = 1; $j <= $i; $j++) {

        //         if ($i > 5 && $j == 5) {
        //             echo '2';
        //         } else {
        //             if ($i % 5 == 0 && $j % 5 == 0) {
        //                 echo '2';
        //             } else {
        //                 echo '1';
        //             }
        //         }
        //     }
        //     echo '<br/>';
        // }

        // dd('');

        $this->page_data['page']->tab = 'account';
        $this->load->view('test', $this->page_data);
        // redirect('/test');
    }

    function testcss()
    {
        $this->page_data['page']->title = 'CSS';
        $this->load->view('testcss', $this->page_data);
    }

    function testphp()
    {
        $this->page_data['page']->title = 'PHP';
        $this->load->view('testphp', $this->page_data);
    }

    function testjs()
    {
        $this->page_data['page']->title = 'Javascript';
        $this->load->view('testjs', $this->page_data);
    }

    function security()
    {
        $this->page_data['page']->tab = 'security';
        $this->load->view('profile', $this->page_data);
    }
}
