<?php

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class User extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'User';
        $this->page_data['page']->menu = 'user';

        $this->load->model('MyUser_model');
        $this->load->model('Jabatan_model');
    }

    function index()
    {
        $dataLevelVerifikasi = [];
        $verificator_name = $this->config->item('verificator_name');
        for ($i = 1; $i <= $this->config->item("level_verifikasi_max"); $i ++) {
            $text = "";
            foreach($verificator_name as $key=>$val) {
                if($key == $i) {
                    $text = $val;
                }
            }
            $dataLevelVerifikasi[] = [
                'id'   => $i,
                'text' => $text,
            ];
        }
        $this->page_data['org'] = $this->orgList();
        $this->page_data['dataLevelVerifikasi'] = $dataLevelVerifikasi;
        $this->load->view('user/list', $this->page_data);
    }

    function orgList()
    {
        $query = $this->db
            ->select('id,name as text')
            ->order_by('name', 'asc')
            ->get("org");

        return $query->result();
    }

    function getById()
    {
        $id = $this->input->post('id', true);

        $data = MyUser_model::select([
            'user_id',
            'user_login',
            'user_name',
            'email',
            'user_nrp',
            'verifikator_level',
            'jabatan_id',
            'user_mobile'
        ])
            ->whereUserId($id)
            ->first();

        $dataJabatan = Jabatan_model::select([
            'id',
            'nama',
        ])
            ->whereId($data->jabatan_id)
            ->first();

        $data = $data->toArray();

        $data['jabatan'] = [
            'id' => $dataJabatan->id,
            'text' => $dataJabatan->nama,
        ];

        echo json_encode([
            "error" => false,
            'data'  => $data
        ]);
    }

    function delete()
    {
        $id = $this->input->post('id', true);

        $data = $this->db
            ->where('id', $id)
            ->get('Project')
            ->first_row();

        $this->db->where('id', $id);
        $this->db->delete('Project');

        echo json_encode([
            "error" => false,
            'data'  => $data
        ]);
    }

    function get_list()
    {
        $sdate = $this->input->post("sdate");
        $edate = $this->input->post("edate");

        $select = "SELECT
                User.user_id AS DT_RowId,
                User.user_login,
                User.user_name,
                User.user_id,
                User.email,
                User.user_type_name,
                User.user_nrp,
                User.verifikator_level,
                User.user_mobile
            FROM
                User
            WHERE
                User.user_type != 1
            ";

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['DT_RowId'];
        });

        $dt->add('verifikator_name', function ($data) {
            $verificator_name = $this->config->item('verificator_name');
            $a = json_decode($data["verifikator_level"],true);
            $text = "";
            foreach($verificator_name as $key=>$val) {
                if($key == $a) {
                    $text = $val;
                }
            }
            return $text;
        });

        // $dt->edit('date', function ($data) {
        //     return Carbon::parse($data['date'])->translatedFormat('d M Y, H:i:s');
        // });

        echo $dt->generate();
    }

    function get_list_status()
    {
        $sdate = $this->input->post("sdate");
        $edate = $this->input->post("edate");

        $select = "SELECT
                org_apps.id AS DT_RowId,
                org_apps.org_id,
                org.name AS org_name,
                org_apps.id,
                org_apps.name,
                org_apps.user_login,
                IF(user_login_status=1,'UP','DOWN') AS user_login_status
            FROM
                org_apps
            JOIN
                org ON org.id = org_apps.org_id
            WHERE
                org_apps.user_login IS NOT NULL
                AND org_apps.user_login != ''
            ";

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['id'];
        });

        // $dt->edit('date', function ($data) {
        //     return Carbon::parse($data['date'])->translatedFormat('d M Y, H:i:s');
        // });

        echo $dt->generate();
    }

    function process_save()
    {
        $user_id = $this->input->post('user_id', true);
        $user_name = strtoupper($this->input->post('user_name', true));
        $user_login = strtolower($this->input->post('user_login', true));
        $email = strtolower($this->input->post('email', true));
        $user_nrp = strtolower($this->input->post('user_nrp', true));
        $user_password = $this->input->post('user_password', true);
        $verifikator_level = $this->input->post('verifikator_level', true);
        $jabatan_id = $this->input->post('jabatan_id', true);
        $user_mobile = $this->input->post('user_mobile', true);

        unset($trx);

        if (empty($user_id)) {
            $action = 'ADD USER';
            $trx['user_type'] = 2;
            $trx['user_type_name'] = 'Operator';
            $trx['user_status'] = 1;
            $trx['user_nrp'] = $user_nrp;
            $trx['email'] = $email;
            $trx['user_mobile'] = $user_mobile;
            $trx["user_password"] = sha1($user_password);

            $check = MyUser_model::where('user_login', $user_login)
                ->first();
        }
        else {
            $action = 'EDIT USER';

            if ($user_password) {
                $trx["user_password"] = sha1($user_password);
            }

            $check = MyUser_model::where('user_login', $user_login)
                ->where('user_id', '!=', $user_id)
                ->first();
        }

        if ($check) {
            echo json_encode([
                "error"   => true,
                'message' => 'Username already exists'
            ]);
            exit;
        }

        // $trx["id"] = $id;
        $trx["user_name"] = $user_name;
        $trx["user_login"] = $user_login;
        $trx['user_modified'] = Carbon::now();
        $trx['verifikator_level'] = $verifikator_level == '' ? null : $verifikator_level;
        $trx['jabatan_id'] = $jabatan_id;
        $trx['user_nrp'] = $user_nrp;
        $trx['email'] = $email;
        $trx['user_mobile'] = $user_mobile;

        // dd($trx);

        $User = MyUser_model::updateOrCreate([
            'user_id' => $user_id
        ], $trx);

        $this->log_model->logInsert([
            'type'    => 'USER',
            'action'  => $action,
            'logData' => $trx,
        ]);

        echo json_encode([
            "error" => false,
            'data'  => $User
        ]);
    }

    function get_select2_list()
    {
        $json = [];

        $limit = 10;
        $page = $this->input->get("page");

        if (!$page) {
            $page = 0;
        }
        else {
            $page = ($page - 1) * $limit;
        }

        $query = $this->db
            ->like('user_name', $this->input->get("q"));
            // ->where('org_id', $this->input->get("orgId"));

        $query = $this->db
            ->select([
                'user_id AS id',
                'user_name as text'
            ])
            ->order_by('user_name', 'asc');

        $queryLimit = $query
            ->limit($limit, $page)
            ->get("User");

        $json['items'] = $queryLimit->result();
        $json['total'] = $query->get("User")->num_rows();

        // dd($json);

        echo json_encode($json);
    }
}
