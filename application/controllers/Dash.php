<?php

class Dash extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Dashboard';
        $this->page_data['page']->menu = 'dash';

        $this->load->model('Event_model');
    }

    function index()
    {
        $this->page_data['calendarsColorList'] = [
            'Public'   => 'success',
            // 'Holiday' => 'success',
            'Personal' => 'primary',
            // 'Family' => 'warning',
            // 'Lainnya' => 'info'
        ];

        $this->load->view('dashboard', $this->page_data);
    }

    function addEvent()
    {
        try {
            $id = $this->input->post('id', true);
            $title = strtoupper($this->input->post('title', true));
            $start = strtoupper($this->input->post('start', true));
            $end = strtoupper($this->input->post('end', true));
            $location = $this->input->post('location', true);
            $category = $this->input->post('category', true);
            $description = $this->input->post('description', true);
            $allday = $this->input->post('allday', true);

            unset($trx);

            $trx = [
                'id'          => $id,
                'title'       => $title,
                'start_date'  => $start,
                'end_date'    => $end,
                'location'    => $location,
                'category'    => $category,
                'description' => $description,
                'allday'      => $allday,
            ];

            if (!$id) {
                $action = 'Tambah data event';
                $msg = 'Tambah data sukses';
            }
            else {
                $action = 'Edit data event';
                $msg = 'Edit data sukses';

                $check = Event_model::whereId($id)->first();
                if ($check->created_by != logged('user_id') && logged('user_type') != '1') {
                    throw new Exception('Tidak diperkenankan mengedit data milik pengguna lain');
                }
            }

            $this->db->trans_begin();

            $event = Event_model::updateOrCreate([
                'id' => $id
            ], $trx);

            unset($trx['id']);

            $this->log_model->logInsert([
                'type'    => 'EVENT',
                'action'  => $action,
                'id'      => $event->id,
                'logData' => $event,
            ]);

            $this->db->trans_commit();

            echo json_encode([
                "error"   => false,
                'data'    => $event,
                'message' => $msg,
            ]);
        } catch (\Exception $e) {
            $this->db->trans_rollback();

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function getEvent()
    {
        $userId = logged('user_id');
        $sDate = $this->input->post("sDate");
        $eDate = $this->input->post("eDate");
        $category = $this->input->post("category") ?? [];

        $conditionCateg = [];
        foreach ($category as $row) {
            $conditionCateg[] = "LOWER(event.category) = '$row'";
        }

        $condition[] = "event.deleted_at IS NULL";
        $condition[] = "event.start_date >= '$sDate'";
        $condition[] = "event.start_date <= '$eDate'";

        if (count($conditionCateg) > 0) {
            $wCateg = implode(' OR ', $conditionCateg);
            $condition[] = "($wCateg)";
        }

        if (logged('user_type') != 1) {
            $condition[] = "(event.created_by = $userId OR event.category = 'Public')";
        }

        $w = implode(' AND ', $condition);

        $select = "SELECT
                event.id,
                event.title,
                event.start_date,
                event.end_date,
                event.allday,
                event.category,
                event.location,
                event.url,
                event.description
            FROM
                event
            WHERE $w GROUP BY event.id ORDER BY event.start_date, event.end_date";

        $query = $this->db->query($select);

        $data = [];
        foreach ($query->result() as $row) {
            $data[] = [
                'id'            => intval($row->id),
                'url'           => $row->url ?? '',
                'title'         => $row->title,
                'start'         => $row->start_date,
                'end'           => $row->end_date,
                'allDay'        => $row->allday == '1' ? true : false,
                'display'       => 'block',
                'extendedProps' => [
                    'location'    => $row->location ?? '',
                    'calendar'    => $row->category,
                    'description' => $row->description,
                ],
            ];
        }

        echo json_encode([
            'error' => false,
            'data'  => $data
        ]);

        // echo json_encode([
        //     'error' => false,
        //     'data'  => [
        //         [
        //             'id'            => 1,
        //             'url'           => '',
        //             'title'         => 'Design Review',
        //             'start'         => '2024-01-05',
        //             'end'           => '2024-01-07',
        //             'allDay'        => true,
        //             // 'display'       => 'block',
        //             'extendedProps' => [
        //                 'calendar' => 'Public'
        //             ],
        //         ],
        //         [
        //             'id'            => 2,
        //             'url'           => '',
        //             'title'         => 'asdj akjshd k',
        //             'start'         => '2024-01-10 08:10:00',
        //             'end'           => '2024-01-10 13:10:00',
        //             'allDay'        => false,
        //             // 'display'       => 'block',
        //             'extendedProps' => [
        //                 'calendar' => 'Personal'
        //             ],
        //         ],
        //         [
        //             'id'            => 3,
        //             'url'           => '',
        //             'title'         => 'asdads',
        //             'start'         => '2024-01-11',
        //             'end'           => '2024-01-11',
        //             'allDay'        => true,
        //             // 'display'       => 'block',
        //             'extendedProps' => [
        //                 'calendar' => 'Personal'
        //             ],
        //         ]
        //
        //     ]
        // ]);
    }

    function deleteEvent()
    {
        try {
            $id = $this->input->post('id', true);

            $event = Event_model::whereId($id)->first();

            if (!$event) {
                throw new Exception('Data tidak ditemukan');
            }

            if ($event->created_by != logged('user_id') && logged('user_type') != '1') {
                throw new Exception('Tidak diperkenankan menghapus data milik pengguna lain');
            }

            $event->deleted_by = logged('user_id');
            $event->save();

            $event->delete();

            $action = 'Hapus data event';

            $this->log_model->logInsert([
                'type'    => 'EVENT',
                'action'  => $action,
                'id'      => $event->id,
                'logData' => $event,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $event,
                'message' => 'Data Event Sudah Dihapus'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_dashboard_data()
    {
        try {
            $period = $this->input->post("period");
            $month = $this->input->post("month");
            $year = $this->input->post("year");
            $sDate = $this->input->post("sDate");
            $eDate = $this->input->post("eDate");

            $condition[] = "surat_masuk.deleted_at IS NULL";

            if ($period == 'harian') {
                $condition[] = "DATE(surat_masuk.tgl_surat) = '$sDate'";
            }
            else if ($period == 'bulanan') {
                $condition[] = "MONTH(surat_masuk.tgl_surat) = $month";
                $condition[] = "YEAR(surat_masuk.tgl_surat) = $year";
            }
            else if ($period == 'tahunan') {
                $condition[] = "YEAR(surat_masuk.tgl_surat) = $year";
            }
            else if ($period == 'manual') {
                $condition[] = "DATE(surat_masuk.tgl_surat) >= '$sDate'";
                $condition[] = "DATE(surat_masuk.tgl_surat) <= '$eDate'";
            }

            $w = implode(' AND ', $condition);

            $sql = "
        SELECT
            SUM(IF(surat_masuk.status_verifikasi='BARU',1,0)) AS suratMasukBaruCount,
            SUM(IF(surat_masuk.status_verifikasi='SEDANG DIVERIFIKASI',1,0)) AS suratMasukSedangVerifikasiCount,
            SUM(IF(surat_masuk.status_verifikasi='TERVERIFIKASI',1,0)) AS suratMasukTerverifikasiCount,
            SUM(IF(surat_masuk.status_verifikasi='DITOLAK',1,0)) AS suratMasukDitolakCount,
            SUM(IF(surat_masuk.status_verifikasi='DISPOSISI',1,0)) AS suratMasukDisposisiCount
        FROM
            surat_masuk
        WHERE $w";

            $query = $this->db->query($sql);
            $dataSuratMasuk = $query->result()[0];

            $sql = str_replace('surat_masuk', 'surat_keluar', $sql);
            $sql = str_replace('suratMasuk', 'suratKeluar', $sql);

            $query = $this->db->query($sql);
            $dataSuratKeluar = $query->result()[0];

            echo json_encode([
                'error' => false,
                'data'  => [
                    'suratMasukBaruCount'              => $dataSuratMasuk->suratMasukBaruCount,
                    'suratMasukSedangVerifikasiCount'  => $dataSuratMasuk->suratMasukSedangVerifikasiCount,
                    'suratMasukTerverifikasiCount'     => $dataSuratMasuk->suratMasukTerverifikasiCount,
                    'suratMasukDitolakCount'           => $dataSuratMasuk->suratMasukDitolakCount,
                    'suratMasukDisposisiCount'         => $dataSuratMasuk->suratMasukDisposisiCount,
                    'suratKeluarBaruCount'             => $dataSuratKeluar->suratKeluarBaruCount,
                    'suratKeluarSedangVerifikasiCount' => $dataSuratKeluar->suratKeluarSedangVerifikasiCount,
                    'suratKeluarTerverifikasiCount'    => $dataSuratKeluar->suratKeluarTerverifikasiCount,
                    'suratKeluarDitolakCount'          => $dataSuratKeluar->suratKeluarDitolakCount,
                ]
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }
}
