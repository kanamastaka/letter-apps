<?php

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Profile';
        $this->page_data['page']->menu = 'profile';

        // $this->load->model('Org_model');

        $this->load->model('MyUser_model');
    }

    function index()
    {
        redirect('/profile/changePassword', 'refresh');

        $this->page_data['page']->tab = 'account';
        $this->load->view('profile', $this->page_data);
    }

    function security()
    {
        $this->page_data['page']->tab = 'security';
        $this->load->view('profile', $this->page_data);
    }

    function changePassword()
    {
        $this->page_data['page']->title = 'Change Password';
        $this->load->view('changePassword', $this->page_data);
    }

    function changePasswordSave()
    {
        $user_id = $this->input->post('user_id', TRUE);
        $user_password_old = $this->input->post('passwordOld', TRUE);
        $user_password = $this->input->post('password', TRUE);

        $user = MyUser_model::where('user_id', $user_id)
            ->where('user_password', sha1($user_password_old))
            ->first();

        if (!$user) {
            echo json_encode([
                "error" => true,
                'message' => 'Old Password Incorrect'
            ]);
            exit;
        } else if (sha1($user_password) == sha1($user_password_old)) {
            echo json_encode([
                "error" => true,
                'message' => 'Old Password and New Password not change'
            ]);
            exit;
        }

        $user->update([
            'user_password' => sha1($user_password)
        ]);

        $time = time();
        $login_token = sha1($user->user_id . $user->user_password . $time);

        $array = [
            'login' => true,
            // saving encrypted userid and password as token in session
            'login_token' => $login_token,
            'logged' => [
                'id' => $user->user_id,
                'user_type' => $user->user_type,
                'user_type_name' => $user->user_type_name,
                'user_name' => $user->user_name,
                'verifikator_level' => $user->verifikator_level,
                'jabatan_id' => $user->jabatan_id,
                'time' => $time,
            ]
        ];

        $this->session->set_userdata($array);

        $this->log_model->logInsert([
            'type' => 'UPDATE PASSWORD',
            'action' => 'UPDATE PASSWORD',
            'logData' => [
                'user_password_old' => sha1($user_password_old),
                'user_password' => sha1($user_password),
            ],
        ]);

        echo json_encode([
            'message' => 'Change Password Success',
            "error" => false,
            '$check' => $user,
        ]);
    }
}
