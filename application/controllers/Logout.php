<?php

class Logout extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!is_logged()) {
            redirect(base_url(), 'refresh');
        }
    }

    public function index()
    {
        $insertlog = $this->log_model->insertlog(logged('user_name'), "LOGOUT", "LOGOUT", logged('user_id'));
        $this->users_model->logout();
        redirect(base_url(), 'refresh');
    }
}
