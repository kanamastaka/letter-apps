<?php

class FileManagementCategory extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'Kategori File';
        $this->page_data['page']->menu = 'fileManagementCategory';

        $this->load->model('FileManagementCategory_model');
    }

    function index()
    {
        $this->load->view('fileManagementCategory/list', $this->page_data);
    }

    function getById()
    {
        $id = $this->input->post('id', true);

        $data = FileManagementCategory_model::select([
            'id',
            'nama',
        ])
            ->whereId($id)
            ->first();

        echo json_encode([
            "error" => false,
            'data'  => $data
        ]);
    }

    function delete()
    {
        try {
            $id = $this->input->post('id', true);

            $FileManagementCategory = FileManagementCategory_model::whereId($id)->first();

            if (!$FileManagementCategory) {
                throw new Exception('Data tidak ditemukan');
            }

            $FileManagementCategory->deleted_by = logged('user_id');
            $FileManagementCategory->save();

            $FileManagementCategory->delete();

            $action = 'Hapus Data Kategori File';

            $this->log_model->logInsert([
                'type'    => 'KATEGORI FILE',
                'action'  => $action,
                'id'      => $FileManagementCategory->id,
                'logData' => $FileManagementCategory,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $FileManagementCategory,
                'message' => 'Data Kategori File Sudah Dihapus'
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_list()
    {
        $sdate = $this->input->post("sdate");
        $edate = $this->input->post("edate");

        $select = "SELECT
                file_management_category.id AS DT_RowId,
                file_management_category.id,
                file_management_category.nama
            FROM
                file_management_category
                WHERE deleted_at IS NULL";

        $dt = $this->getDatatable($select);

        $dt->add('DT_RowId', function ($data) {
            return 'row_' . $data['DT_RowId'];
        });

        echo $dt->generate();
    }

    function process_save()
    {
        try {
            $id = $this->input->post('id', true);
            $nama = $this->input->post('nama', true);

            unset($trx);

            $trx = [
                'id'   => $id,
                'nama' => $nama,
            ];

            if (!$id) {
                $action = 'Tambah data kategori file';
                $msg = 'Tambah data sukses';
            }
            else {
                $action = 'Edit data kategori file';
                $msg = 'Edit data sukses';
            }

            $FileManagementCategory = FileManagementCategory_model::updateOrCreate([
                'id' => $id
            ], $trx);

            unset($trx['id']);

            $this->log_model->logInsert([
                'type'    => 'KATEGORI FILE',
                'action'  => $action,
                'id'      => $FileManagementCategory->id,
                'logData' => $FileManagementCategory,
            ]);

            echo json_encode([
                "error"   => false,
                'data'    => $FileManagementCategory,
                'message' => $msg,
            ]);
        } catch (\Exception $e) {

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    function get_select2_list()
    {
        $json = [];

        $limit = 10;
        $page = $this->input->get("page");

        if (!$page) {
            $page = 0;
        }
        else {
            $page = ($page - 1) * $limit;
        }

        $query = $this->db
            ->where('deleted_at IS NULL')
            ->like('nama', $this->input->get("q"));

        $query = $this->db
            ->select([
                'id',
                'nama as text'
            ])
            ->order_by('nama', 'asc');

        $queryLimit = $query
            ->limit($limit, $page)
            ->get("file_management_category");

        $json['items'] = $queryLimit->result();
        $json['total'] = $query->get("file_management_category")->num_rows();

        echo json_encode($json);
    }
}
