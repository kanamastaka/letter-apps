<?php
class Tes extends CI_Controller
{
    var $sess;
    var $params;

    public function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $now = \Carbon\Carbon::now();
        $last30Days = \Carbon\Carbon::now()->subDays(30)->format('Y-m-d H:i:s');

        $this->db->trans_begin();

        $sql = "INSERT INTO psec_logs_history SELECT * FROM psec_logs WHERE date <= ?";
        $query = $this->db->query($sql, array($last30Days));

        $sqlDelete = "DELETE FROM psec_logs WHERE date <= ?";
        $queryDelete = $this->db->query($sqlDelete, array($last30Days));

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

    }
}
