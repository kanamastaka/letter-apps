<form id="formSuratKeluar" class="row gy-1 gx-2 mt-75" onsubmit="return formSuratKeluarSubmit()">
</form>

<template id="formSuratKeluarTpl">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>"/>
    <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>"/>
    <input type="hidden" name="id" id="id"/>

    <div class="row gy-0 gx-0 mt-75">

        <div class="col-sm-12 col-lg-12">
            <div class="row gy-1 gx-2">

                <div class="col-sm-3 col-12 position-relative">
                    <label class="form-label" for="tgl_surat">Tanggal Surat</label>
                    <input type="text" id="tgl_surat" name="tgl_surat" class="form-control"/>
                </div>

                <div class="col-sm-3 col-12">
                    <label class="form-label" for="nama">Nama Surat</label>
                    <input type="text" id="nama" name="nama" class="form-control"/>
                </div>

                <div class="col-sm-3 col-12">
                    <label class="form-label" for="tujuan">Tujuan</label>
                    <input type="text" id="tujuan" name="tujuan" class="form-control"/>
                </div>

                <div class="col-sm-3 col-12">
                    <label class="form-label" for="kerahasiaan">Kerahasiaan Surat</label>
                    <select class="select2 select2-data-ajax form-select" id="kerahasiaan" name="kerahasiaan"></select>
                </div>

                <!--                <div class="col-sm-3 col-12">-->
                <!--                    <label for="file" class="form-label">File Surat</label>-->
                <!--                    <input class="form-control" type="file" id="file" name="file" accept="application/pdf,application/msword,-->
                <!--  application/vnd.openxmlformats-officedocument.wordprocessingml.document" />-->
                <!--                </div>-->

                <!--                <div class="col-12">-->
                <!--                    <label class="form-label" for="isi">Isi Surat</label>-->
                <!--                    <textarea id="isi" name="isi" class="form-control"></textarea>-->
                <!---->
                <!--                </div>-->

                <div class="col-12">
                    <label class="form-label" for="isi">Isi Surat</label>
                    <div id="full-wrapper" class="form-control">
                        <div class="editor">
                        </div>
                    </div>

                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-50">
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        Simpan
                    </button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

</template>

<script>
    var formSuratKeluar = $('#formSuratKeluar'),
        tglSuratEl = null,
        kerahasiaanEl = null,
        formSuratKeluarLoading = false,
        formSuratKeluarMode = 'ADD',
        tgl_surat = moment(),
        base64data = null,
        isiEditor = null

    jQuery(document).ready(function() {

    })

    $(window).on('load', function() {

        $('#formSuratKeluarModal').on('shown.bs.modal', function(e) {
            window.toastr.clear()

            // var buttons = ['html', 'formatting', '|', 'bold', 'italic', 'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'table', 'fontcolor', 'backcolor', 'alignment', 'horizontalrule']
            // isiEditor = $('#isi').redactor({
            //     minHeight: 250,
            //     // buttons: buttons
            // })

            isiEditor = $(".editor", formSuratKeluar).dxHtmlEditor({
                height: 450,
                value: null,
                imageUpload: {
                    // tabs: ['file', 'url'],
                    tabs: ['file'],
                    fileUploadMode: 'base64',
                },
                tableContextMenu: { enabled: true },
                tableResizing: { enabled: true },
                toolbar: {
                    items: [
                        'undo', 'redo', 'separator',
                        {
                            name: 'size',
                            acceptedValues: ['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt'],
                            options: { inputAttr: { 'aria-label': 'Font size' } },
                        },
                        {
                            name: 'font',
                            acceptedValues: ['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana'],
                            options: { inputAttr: { 'aria-label': 'Font family' } },
                        },
                        'separator', 'bold', 'italic', 'strike', 'underline', 'separator',
                        'alignLeft', 'alignCenter', 'alignRight', 'alignJustify', 'separator',
                        'orderedList', 'bulletList', 'increaseIndent', 'decreaseIndent', 'separator',
                        {
                            name: 'header',
                            acceptedValues: [false, 1, 2, 3, 4, 5],
                            options: { inputAttr: { 'aria-label': 'Header' } },
                        }, 'separator',
                        'color', 'background', 'separator',
                        'link', 'image', 'separator',
                        'clear', 'codeBlock', 'blockquote', 'separator',
                        'insertTable', 'deleteTable',
                        'insertRowAbove', 'insertRowBelow', 'deleteRow',
                        'insertColumnLeft', 'insertColumnRight', 'deleteColumn',
                    ],
                },
                mediaResizing: {
                    enabled: true,
                },// Configuration goes here
            }).dxHtmlEditor('instance')

            tglSuratEl = $('#tgl_surat', formSuratKeluar).flatpickr({
                altInput: true,
                altFormat: 'j F Y',
                dateFormat: 'Y-m-d',
                // locale: 'id',
                defaultDate: tgl_surat.format('YYYY-MM-DD'),
                onChange(selectedDates, dateStr) {
                    // sDate = moment(selectedDates[0])
                    // loadData()
                    // loadDataChart()
                }
            })

            $('button[type=submit], input[type=submit]', formSuratKeluar).find('.spinner').hide()

            kerahasiaanEl = $('#kerahasiaan', formSuratKeluar)
            kerahasiaanEl
                .wrap('<div class="position-relative"></div>')
                .prepend('<option selected=""></option>')
                .select2({
                    placeholder: 'Pilih Kerahasiaan',
                    data: [
                        { id: 'BIASA', text: 'BIASA' },
                        { id: 'RAHASIA', text: 'RAHASIA' }
                    ],
                    // minimumResultsForSearch: -1,
                    allowClear: true,
                    dropdownAutoWidth: true,
                    dropdownParent: kerahasiaanEl.parent(),
                    width: '100%'
                }).on('change.select2', function(e) {
                var val = kerahasiaanEl.select2('val')
                if (val) {
                    formSuratKeluar.validate().resetForm()
                }
            })

            // kerahasiaanEl.select2('focus')

            if (formSuratKeluar.my('data').kerahasiaan) {
                kerahasiaanEl.val(formSuratKeluar.my('data').kerahasiaan).trigger('change')
            }

            if (formSuratKeluar.my('data').isi) {
                isiEditor.option("value", formSuratKeluar.my('data').isi)
            }
        })

        $('#formSuratKeluarModal').on('hidden.bs.modal', function(e) {
            e.preventDefault()
            e.stopPropagation()

            base64data = null
            $('#org-logo').attr('src', 'app-assets/images/suratKeluar/default.jpg')
            $('button[type=submit], input[type=submit]', formSuratKeluar).find('.spinner').hide()

            formSuratKeluar.validate().resetForm()
            formSuratKeluar.my('remove')
        })

        formSuratKeluar.submit(function() {
            if (formSuratKeluar.valid()) {
                $('button[type=submit], input[type=submit]', formSuratKeluar).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formSuratKeluar).find('.spinner').show()
            }
        })
    })

    function formSuratKeluarInit(idSuratKeluar = null) {
        if (idSuratKeluar) {
            $.post("<?php echo base_url() ?>suratKeluar/getById", {
                    id: idSuratKeluar,
                    modeView: false
                },
                function(resp) {
                    if (resp.error) {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    }
                    formSuratKeluarRender(resp.data.id, resp.data)
                }, 'json')
        } else {
            formSuratKeluarRender()
        }
    }

    function formSuratKeluarRender(idSuratKeluar = null, formData = null) {

        formSuratKeluarMode = idSuratKeluar ? 'EDIT' : 'ADD'
        var title = idSuratKeluar ? 'Edit Data' : 'Tambah Data'

        $('#formSuratKeluarModalTitle').html(title + ' <?php echo $page->title ?>')

        // Manifest
            var manifest = {

            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id: <?php echo logged('user_id'); ?>,
                id: null,
                nama: null,
                kerahasiaan: null,
                tujuan: null,
                // file: null,
                isi: null
            },

            // Init function
            init: function($form, runtime) {
                $form.empty().html($('#formSuratKeluarTpl').html())

                $form.then(function() { // Fade when start succeds
                    // $('input:visible:enabled:first', formSuratKeluar).focus()
                    formSuratKeluar.validate({
                        rules: {
                            tgl_surat: {
                                required: true
                            },
                            nama: {
                                required: true
                            },
                            kerahasiaan: {
                                required: true
                            },
                            tujuan: {
                                required: true
                            },
                            // file: {
                            //     required: true
                            // },
                            isi: {
                                required: true
                            }
                        },
                        messages: {
                            tgl_surat: 'Tanggal surat harus diisi',
                            nama: 'Nama surat harus diisi',
                            kerahasiaan: 'Kerahasiaan surat harus diisi',
                            tujuan: 'Tujuan surat harus diisi',
                            // file: 'File surat harus diisi',
                            isi: 'Isi surat harus diisi'
                        }
                    })
                })

                if (idSuratKeluar != null) {
                    $('#formSuratKeluarModalTitle').html('Edit <?php echo $page->title ?> Data')
                }

                $('#formSuratKeluarModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show')
            },

            // Bindings
            ui: {
                '#id': {
                    bind: 'id'
                },
                '#tgl_surat': {
                    bind: 'tgl_surat'
                },
                '#nama': {
                    bind: 'nama'
                },
                '#kerahasiaan': {
                    bind: 'kerahasiaan'
                },
                '#tujuan': {
                    bind: 'tujuan'
                },
                // '#file': {
                //     bind: 'file'
                // },
                // '.editor': {
                //     bind: function (data, value, $control) {
                //         console.log('data', data)
                //         console.log('value', value)
                //         console.log('$control', $control)
                //
                //         var dm = data.isi;
                //         if (value != null) dm.isi = value;
                //
                //         console.log('dm', dm)
                //
                //         return data.isi = (isiEditor !== null ? isiEditor.option("value") : '')
                //         // var dm = data.metrics;
                //         // if (value != null) dm.age = value;
                //         // return dm.age =  (dm.age + "").replace(/\D/g,"");
                //     }
                //     // bind: 'isi',
                //     // init: function($ctrl) {
                //     //     console.log('$ctrl', $ctrl)
                //     //     isiEditor !== null ? isiEditor.option("value") : null
                //     //
                //     //
                //     // }
                // }
            }
        }

        // Init $.my over DOM node
        formSuratKeluar.my(manifest, formData)
    }

    function formSuratKeluarSubmit() {
        if (formSuratKeluar.valid()) {

            var data = formSuratKeluar.my('data')

            var formData = new FormData()
            // fileInput = document.querySelector('#file')

            // Append the text fields
            formData.append('id', data.id)
            formData.append('tgl_surat', data.tgl_surat)
            formData.append('nama', data.nama)
            formData.append('tujuan', data.tujuan)
            formData.append('kerahasiaan', data.kerahasiaan)
            formData.append('isi', isiEditor.option("value"))

            // let docFile = fileInput.files
            // if (docFile.length > 0) {
            //     formData.append('file', docFile[0])
            // }

            fetch("<?php echo base_url(); ?>suratKeluar/process_save", {
                method: 'POST',
                // headers: {
                //     'X-CSRF-TOKEN': formProfile.querySelector('[name="_token"]').value
                // },
                body: formData
            }).then((resp) => resp.json())
              .then(resp => {

                  if (resp.error) {
                      toastr['error'](resp.message, 'ERROR', {
                          closeButton: true,
                          tapToDismiss: false
                      })
                  } else {
                      // base64data = null
                      // $('#org-logo').attr('src', 'app-assets/images/suratKeluar/default.jpg')
                      gridSuratKeluar.ajax.reload()

                      toastr['success'](' ', resp.message, {
                          closeButton: false,
                          tapToDismiss: true
                      })

                      if (formSuratKeluarMode == 'ADD') {

                          formSuratKeluar.trigger('reset')
                          formSuratKeluar.validate().resetForm()

                          // $('input:visible:enabled:first', formSuratKeluar).focus()
                          kerahasiaanEl.val(null).trigger('change')
                          tglSuratEl.setDate(tgl_surat.format('YYYY-MM-DD'))
                      } else {
                          $('#formSuratKeluarModal').modal('hide')
                      }
                  }

                  $('button[type=submit], input[type=submit]', formSuratKeluar).prop('disabled', false)
                  $('button[type=submit], input[type=submit]', formSuratKeluar).find('.spinner').hide()

                  return false
              })

            return false
        }

        return false
    }
</script>
