<section class="invoice-preview-wrapper">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-50 text-primary">Detail Surat Keluar</h4>
                    <div>
                        <button type="button" class="btn btn-primary waves-effect waves-float waves-light verif-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                            <span class="visually-hidden">Loading...</span>
                            <span id="verif-btn-txt">VERIFIKASI</span>
                        </button>
                        <button type="button" class="btn btn-danger waves-effect waves-float waves-light reject-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                            <span class="visually-hidden">Loading...</span>
                            <span id="verif-btn-txt">TOLAK</span>
                        </button>
                        <button type="button"
                                class="btn btn-icon btn-secondary waves-effect waves-float waves-light close-btn">
                            <i data-feather="x-circle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row invoice-preview">
        <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
            <div class="card">
                <div class="card-body">

                    <div class="form form-vertical d-none">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="first-name-vertical">First Name</label>
                                    <input type="text" id="first-name-vertical" class="form-control" name="fname" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="email-id-vertical">Email</label>
                                    <input type="email" id="email-id-vertical" class="form-control" name="email-id" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="contact-info-vertical">Mobile</label>
                                    <input type="number" id="contact-info-vertical" class="form-control" name="contact" placeholder="Mobile">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="password-vertical">Password</label>
                                    <input type="password" id="password-vertical" class="form-control" name="contact" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="customCheck3">
                                        <label class="form-check-label" for="customCheck3">Remember me</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="reset" class="btn btn-primary me-1 waves-effect waves-float waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                            </div>
                        </div>
                    </div>

                    <dl class="row mb-0">
                        <dt class="col-sm-5 fw-bolder mb-1">Tanggal Surat</dt>
                        <dd class="col-sm-7 mb-1" id="tglSuratEl"></dd>

                        <dt class="col-sm-5 fw-bolder mb-1">Nama Surat</dt>
                        <dd class="col-sm-7 mb-1" id="namaEl"></dd>

                        <dt class="col-sm-5 fw-bolder mb-1">Tujuan</dt>
                        <dd class="col-sm-7 mb-1" id="tujuanEl"></dd>

                        <dt class="col-sm-5 fw-bolder mb-1">Kerahasiaan</dt>
                        <dd class="col-sm-7 mb-1" id="kerahasiaanEl"></dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-md-8 col-12">
            <div class="card invoice-preview-card">
                <div class="card-body invoice-padding">
                    <div id="isiEl" class="wysiwyg-container">
                    </div>
                </div>
            </div>
        </div>
</section>

<!--<div class="card">-->
<!--    <div class="card-body">-->
<!--        <div class="row">-->
<!--            <div class="col-12">-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!--<page size="A4" class="card">-->
<!--    <div class="card-body p-0">-->
<!---->
<!--    </div>-->
<!--    </div>-->
<!--</page>-->

<script>
    var viewSuratKeluarModalEl = $('#viewSuratKeluarModal'),
        // fileInfoElContainer = $('#fileInfoElContainer'),
        // fileElContainer = $('#fileElContainer'),
        verifBtn = $('.verif-btn', viewSuratKeluarModalEl),
        rejectBtn = $('.reject-btn', viewSuratKeluarModalEl),
        viewData = null

    $(window).on('load', function() {
        // fileInfoElContainer.hide()
        // fileElContainer.hide()

        viewSuratKeluarModalEl.on('shown.bs.modal', function(e) {
            window.toastr.clear()

            verifBtn.prop('disabled', false)
            verifBtn.find('.spinner').hide()

            rejectBtn.prop('disabled', false)
            rejectBtn.find('.spinner').hide()
            rejectBtn.hide()

            $('#tglSuratEl', viewSuratKeluarModalEl).html(moment(viewData.tgl_surat).format('DD MMM YYYY'))
            $('#namaEl', viewSuratKeluarModalEl).html(viewData.nama)
            $('#tujuanEl', viewSuratKeluarModalEl).html(viewData.tujuan)
            $('#kerahasiaanEl', viewSuratKeluarModalEl).html(viewData.kerahasiaan)
            $('#isiEl', viewSuratKeluarModalEl).html(viewData.isi)

            if (viewData.status_verifikasi == 'TERVERIFIKASI') {
                verifBtn.hide()
            } else {
                verifBtn.show()

                if (viewData.level_verifikasi == 0 && viewData.status_verifikasi == 'BARU') {
                    $('#verif-btn-txt', viewSuratKeluarModalEl).html('AJUKAN VERIFIKASI')
                } else {
                    rejectBtn.show()
                    $('#verif-btn-txt', viewSuratKeluarModalEl).html('VERIFIKASI')
                }

                if (viewData.status_verifikasi == 'DITOLAK') {
                    rejectBtn.hide()
                    $('#verif-btn-txt', viewSuratKeluarModalEl).html('AJUKAN VERIFIKASI')
                }
            }

            //if (viewData.file_extension == 'pdf') {
            //    fileElContainer.show()
            //    $('#fileEl', viewSuratKeluarModalEl).attr('src', `<?php //echo base_url(); ?>//app-assets/upload/suratKeluar/${viewData.filename}`)
            //} else {
            //    fileInfoElContainer.show()
            //    $('#fileDownloadEl', viewSuratKeluarModalEl).attr('href', `<?php //echo base_url(); ?>//app-assets/upload/suratKeluar/${viewData.filename}`)
            //}
        })

        viewSuratKeluarModalEl.on('hidden.bs.modal', function(e) {
            // $('#fileEl', viewSuratKeluarModalEl).attr('src', '')
            // fileInfoElContainer.hide()
            // fileElContainer.hide()
        })

        $('.close-btn', viewSuratKeluarModalEl).on('click', function() {
            viewSuratKeluarModalEl.modal('hide')
        })

        $('.verif-btn', viewSuratKeluarModalEl).on('click', function(e) {

            var title = viewData.level_verifikasi == 0 ? 'Proses Pengajuan Verifikasi' : 'Verifikasi'

            Swal.fire({
                text: `${title} Data Surat Keluar`,
                icon: 'question',
                showCancelButton: true,
                customClass: {
                    confirmButton: 'btn btn-lg btn-outline-success me-1',
                    cancelButton: 'btn btn-lg btn-outline-secondary'
                },
                confirmButtonText: 'YA',
                cancelButtonText: 'BATAL',
                buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {

                    verifBtn.prop('disabled', true)
                    verifBtn.find('.spinner').show()

                    $.post(`<?php echo base_url(); ?>suratKeluar/verifikasi`, {
                            id: viewData.id
                        },
                        function(resp) {
                            if (resp.error) {
                                toastr['error'](resp.message, 'ERROR', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            } else {
                                gridSuratKeluar.ajax.reload()

                                viewSuratKeluarModalEl.modal('hide')

                                toastr['success'](resp.message, 'SUKSES', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            }

                            verifBtn.prop('disabled', false)
                            verifBtn.find('.spinner').hide()
                        }, 'json')

                    return false
                }
            })
        })

        $('.reject-btn', viewSuratKeluarModalEl).on('click', function(e) {
            viewSuratKeluarModalEl.modal('hide')
            Swal.fire({
                title: 'Alasan Penolakan',
                input: 'textarea',
                customClass: {
                    confirmButton: 'btn btn-lg btn-outline-success me-1',
                    cancelButton: 'btn btn-lg btn-outline-secondary'
                },
                confirmButtonText: 'PROSES',
                cancelButtonText: 'BATAL',
                buttonsStyling: false,
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var id = document.getElementsByClassName('swal2-textarea')
                    var value = id[0].value
                    if (value.length != 0) {
                    } else {
                        Swal.showValidationMessage('Alasan penolakan belum diisi!')
                    }
                }
            }).then(result => {
                if (result.isConfirmed) {
                    $.post(`<?php echo base_url(); ?>suratKeluar/reject`, {
                            id: viewData.id,
                            catatan: result.value
                        },
                        function(resp) {
                            if (resp.error) {
                                toastr['error'](resp.message, 'ERROR', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            } else {
                                gridSuratKeluar.ajax.reload()

                                toastr['success'](resp.message, 'SUKSES', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            }
                        }, 'json')
                }
            })
        })
    })

    function viewSuratKeluarInit(idSuratKeluar = null) {
        if (idSuratKeluar) {
            $.post("<?php echo base_url() ?>suratKeluar/getById", {
                    id: idSuratKeluar,
                    modeView: true
                },
                function(resp) {
                    if (resp.error) {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    }
                    viewSuratKeluarRender(resp.data.id, resp.data)
                }, 'json')
        } else {
            viewSuratKeluarRender()
        }
    }

    function viewSuratKeluarRender(idSuratKeluar = null, formData = null) {

        viewData = formData

        $('#viewSuratKeluarModal').modal().modal('show')
    }
</script>
