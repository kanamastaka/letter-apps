<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-50 text-primary">Detail Surat Keluar</h4>
                <div>
                    <button type="button" class="btn btn-primary waves-effect waves-float waves-light verif-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        <span id="verif-btn-txt">VERIFIKASI</span>
                    </button>
                    <button type="button" class="btn btn-danger waves-effect waves-float waves-light reject-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        <span id="verif-btn-txt">TOLAK</span>
                    </button>
                    <button type="button"
                            class="btn btn-icon btn-secondary waves-effect waves-float waves-light close-btn">
                        <i data-feather="x-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-12 mb-2">
                        <dl class="row mb-0">
                            <dt class="col-sm-5 fw-bolder mb-1">Tanggal Surat</dt>
                            <dd class="col-sm-7 mb-1" id="tglSuratEl"></dd>

                            <dt class="col-sm-5 fw-bolder mb-1">Nama Surat</dt>
                            <dd class="col-sm-7 mb-1" id="namaEl"></dd>

                            <dt class="col-sm-5 fw-bolder mb-1">Tujuan</dt>
                            <dd class="col-sm-7 mb-1" id="tujuanEl"></dd>

                            <dt class="col-sm-5 fw-bolder mb-1">Kerahasiaan</dt>
                            <dd class="col-sm-7 mb-1" id="kerahasiaanEl"></dd>

                            <dd id="catatan-container" class="col-12 mb-1">
                                <label class="form-label" for="catatan">Catatan</label>
                                <textarea id="catatan" name="catatan" rows="3" class="form-control"></textarea>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div id="isiEl" class="wysiwyg-container">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="mb-2">
            <div id="verif-progress-chart"></div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Riwayat</h4>
            </div>
            <div id="riwayat-container" class="card-body" style="position: relative;height: 400px;overflow: hidden;">
                <ul id="riwayatEl" class="timeline">
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    var viewSuratKeluarModalEl = $('#viewSuratKeluarModal'),
        // fileInfoElContainer = $('#fileInfoElContainer'),
        // fileElContainer = $('#fileElContainer'),
        catatanContainer = $('#catatan-container'),
        riwayatContainer = $('#riwayat-container'),
        verifBtn = $('.verif-btn', viewSuratKeluarModalEl),
        rejectBtn = $('.reject-btn', viewSuratKeluarModalEl),
        viewData = null,
        verifProgressChart = null

    var $textHeadingColor = '#5e5873'
    var $white = '#fff'

    $(window).on('load', function() {
        // fileInfoElContainer.hide()
        // fileElContainer.hide()

        viewSuratKeluarModalEl.on('shown.bs.modal', function(e) {
            $('#catatan', viewSuratKeluarModalEl).val('')

            window.toastr.clear()

            verifBtn.prop('disabled', false)
            verifBtn.find('.spinner').hide()

            rejectBtn.prop('disabled', false)
            rejectBtn.find('.spinner').hide()
            rejectBtn.hide()

            $('#tglSuratEl', viewSuratKeluarModalEl).html(moment(viewData.tgl_surat).format('DD MMM YYYY'))
            $('#namaEl', viewSuratKeluarModalEl).html(viewData.nama)
            $('#tujuanEl', viewSuratKeluarModalEl).html(viewData.tujuan)
            $('#kerahasiaanEl', viewSuratKeluarModalEl).html(viewData.kerahasiaan)
            $('#isiEl', viewSuratKeluarModalEl).html(viewData.isi)

            if (viewData.status_verifikasi == 'TERVERIFIKASI') {
                verifBtn.hide()
                catatanContainer.hide()
            } else {
                verifBtn.show()
                catatanContainer.show()

                if (viewData.level_verifikasi == 0 && viewData.status_verifikasi == 'BARU') {
                    $('#verif-btn-txt', viewSuratKeluarModalEl).html('AJUKAN VERIFIKASI')
                } else {
                    rejectBtn.show()
                    $('#verif-btn-txt', viewSuratKeluarModalEl).html('VERIFIKASI')
                }

                if (viewData.status_verifikasi == 'DITOLAK') {
                    rejectBtn.hide()
                    $('#verif-btn-txt', viewSuratKeluarModalEl).html('AJUKAN VERIFIKASI')
                }
            }

            $.each(viewData.log, function(i, val) {
                var catatan = ''
                if (val.log_data !== null) {
                    var logData = JSON.parse(val.log_data)
                    if (logData.status_verifikasi != 'Disposisi' && logData.catatan !== undefined && logData.catatan !== null && logData.catatan != '') {
                        catatan = `<div>Catatan<pre class="p-1 mt-50 fs-6">${logData.catatan}</pre></div>`
                    } else if (logData.status_verifikasi = 'Disposisi' && logData.catatan_disposisi !== undefined && logData.catatan_disposisi !== null && logData.catatan_disposisi != '') {
                        catatan = `<div>Catatan<pre class="p-1 mt-50 fs-6">${logData.catatan_disposisi}</pre></div>`
                    }
                }
                $('#riwayatEl', viewSuratKeluarModalEl).append(`
                    <li class="timeline-item">
                        <span class="timeline-point timeline-point-indicator"></span>
                        <div class="timeline-event">
                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                <h6>${val.log_action}</h6>
                                <span class="timeline-event-time">${moment(val.log_created).format('DD MMM YYYY, HH:mm:ss')}</span>
                            </div>
                            <div class="mb-50">User : ${val.log_user}</div>
                            ${catatan}
                        </div>
                    </li>
                `)
            })

            renderVerifProgressChart()
            new PerfectScrollbar(riwayatContainer[0])
        })

        viewSuratKeluarModalEl.on('hidden.bs.modal', function(e) {
            $('#catatan', viewSuratKeluarModalEl).val('')
            $('#riwayatEl', viewSuratKeluarModalEl).empty()

            if (verifProgressChart !== null) {
                verifProgressChart.destroy()
            }
        })

        $('.close-btn', viewSuratKeluarModalEl).on('click', function() {
            viewSuratKeluarModalEl.modal('hide')
        })

        $('.verif-btn', viewSuratKeluarModalEl).on('click', function(e) {

            var title = viewData.level_verifikasi == 0 ? 'Proses Pengajuan Verifikasi' : 'Verifikasi'

            Swal.fire({
                title: `${title} Data Surat Keluar`,
                icon: 'question',
                showCancelButton: true,
                customClass: {
                    confirmButton: 'btn btn-lg btn-outline-success me-1',
                    cancelButton: 'btn btn-lg btn-outline-secondary'
                },
                confirmButtonText: 'YA',
                cancelButtonText: 'BATAL',
                buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {

                    verifBtn.prop('disabled', true)
                    verifBtn.find('.spinner').show()

                    $.post(`<?php echo base_url(); ?>suratKeluar/verifikasi`, {
                            id: viewData.id,
                            catatan: $('#catatan', viewSuratKeluarModalEl).val()
                        },
                        function(resp) {
                            if (resp.error) {
                                toastr['error'](resp.message, 'ERROR', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            } else {
                                gridSuratKeluar.ajax.reload()

                                viewSuratKeluarModalEl.modal('hide')

                                toastr['success'](resp.message, 'SUKSES', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            }

                            verifBtn.prop('disabled', false)
                            verifBtn.find('.spinner').hide()
                        }, 'json')

                    return false
                }
            })
        })

        $('.reject-btn', viewSuratKeluarModalEl).on('click', function(e) {
            viewSuratKeluarModalEl.modal('hide')
            Swal.fire({
                title: 'Alasan Penolakan',
                input: 'textarea',
                customClass: {
                    confirmButton: 'btn btn-lg btn-outline-success me-1',
                    cancelButton: 'btn btn-lg btn-outline-secondary'
                },
                confirmButtonText: 'PROSES',
                cancelButtonText: 'BATAL',
                buttonsStyling: false,
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var id = document.getElementsByClassName('swal2-textarea')
                    var value = id[0].value
                    if (value.length != 0) {
                    } else {
                        Swal.showValidationMessage('Alasan penolakan belum diisi!')
                    }
                }
            }).then(result => {
                if (result.isConfirmed) {
                    $.post(`<?php echo base_url(); ?>suratKeluar/reject`, {
                            id: viewData.id,
                            catatan: result.value
                        },
                        function(resp) {
                            if (resp.error) {
                                toastr['error'](resp.message, 'ERROR', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            } else {
                                gridSuratKeluar.ajax.reload()

                                toastr['success'](resp.message, 'SUKSES', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            }
                        }, 'json')
                }
            })
        })
    })

    function viewSuratKeluarInit(idSuratKeluar = null) {
        if (idSuratKeluar) {
            $.post("<?php echo base_url() ?>suratKeluar/getById", {
                    id: idSuratKeluar,
                    modeView: true
                },
                function(resp) {
                    if (resp.error) {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    }
                    viewSuratKeluarRender(resp.data.id, resp.data)
                }, 'json')
        } else {
            viewSuratKeluarRender()
        }
    }

    function viewSuratKeluarRender(idSuratKeluar = null, formData = null) {

        viewData = formData

        $('#viewSuratKeluarModal').modal().modal('show')
    }

    function renderVerifProgressChart() {
        verifProgressChart = new ApexCharts(document.querySelector('#verif-progress-chart'), {
            chart: {
                height: 300,
                type: 'radialBar'
            },
            plotOptions: {
                radialBar: {
                    size: 150,
                    offsetY: -30,
                    startAngle: -150,
                    endAngle: 150,
                    hollow: {
                        size: '65%'
                    },
                    track: {
                        background: $white,
                        strokeWidth: '100%'
                    },
                    dataLabels: {
                        name: {
                            offsetY: -5,
                            color: $textHeadingColor,
                            fontSize: '1rem'
                        },
                        value: {
                            offsetY: 15,
                            color: $textHeadingColor,
                            fontSize: '1.714rem'
                        }
                    }
                }
            },
            colors: [window.colors.solid.danger],
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: [window.colors.solid.primary],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            stroke: {
                dashArray: 8
            },
            series: [viewData.verif_progress],
            labels: ['Status Verifikasi']
        })
        verifProgressChart.render()
    }
</script>
