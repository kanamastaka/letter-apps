<!DOCTYPE html>
<html class="loading light-layout" lang="en" data-layout="light-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="<?php echo $this->config->item("appdesc"); ?>">
    <meta name="keywords" content="surat, arsip">

    <meta name="author" content="<?php echo $this->config->item("appowner"); ?>">
    <title><?php echo $this->config->item("apptitle"); ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="app-assets/images/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="app-assets/images/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="app-assets/images/ico/favicon-16x16.png">
    <link rel="manifest" href="app-assets/images/ico/site.webmanifest">
    <link rel="mask-icon" href="app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/js/bootstrap-daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="//cdn3.devexpress.com/jslib/23.1.6/css/dx.light.css"/>
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-toastr.css">
    <!--    <link rel="stylesheet" type="text/css"-->
    <!--          href="--><?php //echo base_url(); ?><!--app-assets/css/plugins/forms/form-quill-editor.css">-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/forms/pickers/form-pickadate.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/datatable-custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/daterangepicker-custom.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<?php $this->load->view("partial/header"); ?>

<?php $this->load->view("partial/sidebar"); ?>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">

        <div class="content-body">

            <div class="card">
                <!-- <div class="card-header">
                    <h4 class="card-title">card-title</h4>
                </div> -->
                <div class="card-body p-0">
                    <table id="gridSuratKeluar" class="table text-nowrap w-100">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Tgl. Surat</th>
                            <th>Nama Surat</th>
                            <th>Tujuan</th>
                            <th>Kerahasiaan</th>
                            <th>Status</th>
                            <th>Level Verifikasi</th>
<!--                            <th>Catatan</th>-->
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Vendor JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sugar/1.4.1/sugar.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/jQuery.my/jquerymy.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/polyfill.min.js"></script>

<!-- <script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script> -->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>

<script
    src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.print.min.js"></script>
<script
    src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js"></script>

<script src="<?php echo base_url(); ?>app-assets/vendors/js/moment/moment-with-locales.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="https://cdn3.devexpress.com/jslib/23.1.6/js/dx-quill.min.js"></script>
<script src="https://cdn3.devexpress.com/jslib/23.1.6/js/dx.all.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/core/app.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/init.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?php echo base_url(); ?>app-assets/js/scripts/extensions/ext-component-toastr.min.js"></script>

<!-- END: Page JS-->

<div class="modal fade" id="formSuratKeluarModal" style="display: none" aria-labelledby="formSuratKeluarModalTitle"
     aria-hidden="true" data-bs-focus="false">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
            <!-- <div class="modal-header bg-transparent">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div> -->
            <div class="modal-body px-sm-5 mx-50 pb-5">
                <h1 class="text-center mb-1" id="formSuratKeluarModalTitle"></h1>

                <?php $this->load->view("suratKeluar/form"); ?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewSuratKeluarModal" aria-labelledby="formSuratKeluarModalTitle"
     aria-hidden="true" data-bs-focus="false">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
            <!-- <div class="modal-header bg-transparent">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div> -->
            <div class="modal-body px-sm-5 mx-50 pb-5">
                <h1 class="text-center mb-1" id="viewSuratKeluarModalTitle"></h1>

                <?php $this->load->view("suratKeluar/view"); ?>

            </div>
        </div>
    </div>
</div>

<script>

    moment.locale('id')
    var formDataView = null
    var interval = null
    var gridSuratKeluar = null
    var sdate = moment().startOf('month')
    var edate = moment().endOf('month')

    jQuery(document).ready(function() {
        gridSuratKeluar = $('#gridSuratKeluar').DataTable({
            dom: '<"d-flex flex-row align-items-center mx-0 px-1"<"mt-50"B>f>rt<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"<"d-inline-flex flex-row align-items-baseline"li>><"col-sm-12 col-md-6"p>>',
            lengthMenu: [10, 20, 30, 40, 50],
            processing: true,
            serverSide: true,
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: 'cari',
                processing: '<span>Loading...</span>',
                lengthMenu: '_MENU_',
                paginate: {
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },
            order: [
                [1, 'desc']
            ],
            ajax: {
                'url': "<?php echo base_url(); ?>" + 'suratKeluar/get_list',
                'data': function(data) {
                    data.sdate = sdate.format('YYYY-MM-DD'),
                        data.edate = edate.format('YYYY-MM-DD')
                },
                'type': 'POST',
                'dataType': 'json'
            },
            buttons: {
                dom: {
                    button: {
                        tag: 'button',
                        className: ''
                    }
                },
                buttons: [{
                    titleAttr: 'add new',
                    text: '<i class="fas fa-sm fa-fw fa-plus"></i>',
                    className: 'btn btn-icon btn-outline-secondary waves-effect',
                    action: function(e, dt, node, config) {
                        formSuratKeluarInit()
                        // $('#formSuratKeluarModalTitle').html('Add Data <?php echo $page->title ?>');
                        // $('#formSuratKeluarModal').modal('show');
                    }
                },
                    {
                        titleAttr: 'refresh',
                        text: '<i class="fas fa-sm fa-fw fa-sync"></i>',
                        className: 'btn btn-icon btn-outline-secondary waves-effect',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload()
                        }
                    }
                    // {
                    //     extend: 'print',
                    //     className: 'btn btn-outline-default btn-sm ms-2'
                    // },
                    // {
                    //     extend: 'csv',
                    //     className: 'btn btn-outline-default btn-sm'
                    // }
                ]
            },
            columns: [
                {
                    data: 'id'
                }, {
                    data: 'id'
                },
                {
                    data: 'tgl_surat'
                },
                {
                    data: 'nama'
                },
                {
                    data: 'tujuan'
                },
                {
                    data: 'kerahasiaan'
                },
                {
                    data: 'status_verifikasi'
                },
                {
                    data: 'level_verifikasi'
                },
                // {
                //     data: 'catatan'
                // }
            ],
            columnDefs: [{
                targets: 0,
                width: '10px',
                searchable: false,
                orderable: false,
                className: 'pt-0 pb-0',
                render: function(data, type, row, meta) {
                    var el = `<button title="Edit" onclick="btnEditSuratKeluarClicked('${row.id}')" class="btn btn-icon btn-outline-primary btn-sm waves-effect me-50" id=n-"' + meta.row + '"><i class="fas fa-sm fa-fw fa-edit"></i></button><button title="Lihat Detail" onclick="btnViewSuratKeluarClicked('${row.id}')" class="btn btn-icon btn-outline-primary btn-sm waves-effect me-50" id=n-"' + meta.row + '"><i class="fas fa-sm fa-fw fa-eye"></i></button>`

                    if (row.deleted_at !== null) {
                        el += `</button><button title="Hapus" onclick="btnDeleteSuratKeluarClicked('${row.id}')" class="btn btn-icon btn-outline-primary btn-sm waves-effect me-50" id=n-"' + meta.row + '"><i class="fas fa-sm fa-fw fa-trash"></i></button>`
                    }

                    return el
                }
            },
                {
                    targets: 1,
                    visible: false,
                    width: '30px'
                },
                {
                    targets: 5,
                    width: '80px',
                    render: function(data, type, row, meta) {
                        if (row.kerahasiaan == 'BIASA') {
                            return `<span class="badge badge-light-success">${row.kerahasiaan}</span>`
                        } else {
                            return `<span class="badge badge-light-warning">${row.kerahasiaan}</span>`
                        }
                    }
                },
                {
                    targets: 6,
                    width: '80px',
                    render: function(data, type, row, meta) {
                        if (row.status_verifikasi == 'BARU') {
                            return `<span class="badge badge-light-secondary">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'SEDANG DIVERIFIKASI') {
                            return `<span class="badge badge-light-warning">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'DITOLAK') {
                            return `<span class="badge badge-light-danger">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'TERVERIFIKASI') {
                            return `<span class="badge badge-light-success">${row.status_verifikasi}</span>`
                        } else {
                            return `<span class="badge">${row.status_verifikasi}</span>`
                        }
                    }
                },
                {
                    targets: 7,
                    width: '80px',
                    className: 'text-center',
                    render: function(data, type, row, meta) {

                        var level_verifikasi = row.level_verifikasi,
                            level_verifikasi_max = <?php echo $this->config->item("level_verifikasi_max"); ?>

                        if (row.status_verifikasi == 'SEDANG DIVERIFIKASI' || (row.status_verifikasi == 'DITOLAK' && level_verifikasi > 0)) {
                            level_verifikasi = row.level_verifikasi - 1
                        } else if (row.status_verifikasi == 'TERVERIFIKASI') {
                            level_verifikasi = level_verifikasi_max
                        }

                        return `${level_verifikasi} / ${level_verifikasi_max}`
                    }
                },
                // {
                //     targets: 8,
                //     className: 'none',
                //     render: function(data, type, row, meta) {
                //         return `<pre>${row.catatan}</pre>`
                //     }
                // }
            ]
            // createdRow: function(row, data, dataIndex) {
            //     $(row).addClass('row-clicked');
            // }
        })

        $('#gridSuratKeluar tbody').on('click', '.dtr-control button', function(e) {
            e.preventDefault()
            e.stopPropagation()
        })


    })

    function btnEditSuratKeluarClicked(data) {
        formSuratKeluarInit(data)
    }

    function btnViewSuratKeluarClicked(data) {
        viewSuratKeluarInit(data)
    }

    function btnDeleteSuratKeluarClicked(row) {
        // var data = gridSuratKeluar.rows(row).data()[0]
        Swal.fire({
            text: `Hapus Data Surat Keluar`,
            icon: 'warning',
            showCancelButton: true,
            customClass: {
                confirmButton: 'btn btn-lg btn-outline-success me-1',
                cancelButton: 'btn btn-lg btn-outline-secondary'
            },
            confirmButtonText: 'YA',
            cancelButtonText: 'TIDAK',
            buttonsStyling: false
        }).then((result) => {
            if (result.isConfirmed) {
                // clearInterval(interval)
                $('#resrow').hide()

                $.post("<?php echo base_url() ?>suratKeluar/delete", 'id=' + row,
                    function(resp) {
                        if (!resp.error) {
                            gridSuratKeluar.ajax.reload()

                            toastr['success'](resp.message, 'SUKSES', {
                                closeButton: true,
                                tapToDismiss: false
                            })

                        } else {
                            toastr['error'](resp.message, 'ERROR', {
                                closeButton: true,
                                tapToDismiss: false
                            })
                        }
                    }, 'json')
                return false
            }
        })
    }
</script>

<?php $this->load->view("partial/socket-js"); ?>

</body>
<!-- END: Body-->

</html>
