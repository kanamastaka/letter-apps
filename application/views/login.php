<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="<?php echo $this->config->item("appdesc"); ?>">
    <meta name="keywords" content="surat, arsip">
    <meta name="author" content="<?php echo $this->config->item("appowner"); ?>">
    <title><?php echo $this->config->item("apptitle"); ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="app-assets/images/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="app-assets/images/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="app-assets/images/ico/favicon-16x16.png">
    <link rel="manifest" href="app-assets/images/ico/site.webmanifest">
    <link rel="mask-icon" href="app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/authentication.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click"
      data-menu="vertical-menu-modern" data-col="blank-page">
<!-- BEGIN: Content-->
<div class="app-content content login-bg">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div id="auth-particles" class="auth-wrapper auth-basic px-2">
                <div class="auth-inner my-2" style="position:absolute;">
                    <div class="my-2 text-center">
                        <a href="<?php echo base_url(); ?>" class="brand-logo m-0">
                            <img src="<?php echo base_url(); ?>/app-assets/images/logo/logo-light.png" alt=""
                                 height="150">
                        </a>

                        <h2 class="brand-text text-white text-uppercase mt-2 fs-4"><?php echo $this->config->item("apptitle"); ?></h2>
                    </div>

                    <!-- Login basic -->
                    <div class="card mb-0">
                        <div class="card-body">

                            <!-- <a href="<?php echo base_url(); ?>" class="brand-logo m-0">
                  <img src="/app-assets/images/logo/logo-light.png" alt="" height="125"> -->
                            <!-- <h2 class="brand-text text-primary ms-1">Vuexy</h2> -->
                            <!-- </a> -->

                            <!-- <h4 class="card-title mb-2 text-center fs-1" style="letter-spacing: 4px;">&nbsp;</h4> -->
                            <!-- <p class="card-text mb-2 text-center">Silahkan Verifikasi Identitas Anda</p> -->

                            <form name="auth-login-form" id="auth-login-form"
                                  onSubmit="javascript: return dologin(this)">

                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                       value="<?php echo $this->security->get_csrf_hash(); ?>"/>

                                <div class="mb-1">
                                    <label for="username" class="form-label mb-1">Username <span
                                            class="text-danger">*</span></label>
                                    <input type="text" id="username" name="username"
                                           class="form-control form-control-lg" value="" placeholder="" autofocus/>
                                </div>

                                <div class="mb-1">
                                    <div class="d-flex justify-content-between">
                                        <label class="form-label mb-1" for="password">Password <span
                                                class="text-danger">*</span></label>
                                        <!-- <a href="auth-forgot-password-basic.html">
                                                                  <small>Forgot Password?</small>
                                                              </a> -->
                                    </div>
                                    <div class="input-group input-group-merge form-password-toggle">
                                        <input type="password" class="form-control form-control-lg form-control-merge"
                                               id="password" name="password"
                                               placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                               aria-describedby="password"/>
                                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                    </div>
                                </div>
                                <!-- <div class="mb-1">
                                                      <div class="form-check">
                                                          <input class="form-check-input" type="checkbox" id="remember-me"
                                                              tabindex="3" />
                                                          <label class="form-check-label" for="remember-me"> Remember Me </label>
                                                      </div>
                                                  </div> -->
                                <button class="btn btn-primary btn-lg w-100" tabindex="4">LOGIN</button>
                            </form>

                            <!-- <p class="text-center mt-2">
                                                <span>New on our platform?</span>
                                                <a href="auth-register-basic.html">
                                                    <span>Create an account</span>
                                                </a>
                                            </p> -->

                            <!-- <div class="divider my-2">
                                                <div class="divider-text">or</div>
                                            </div>

                                            <div class="auth-footer-btn d-flex justify-content-center">
                                                <a href="#" class="btn btn-facebook">
                                                    <i data-feather="facebook"></i>
                                                </a>
                                                <a href="#" class="btn btn-twitter white">
                                                    <i data-feather="twitter"></i>
                                                </a>
                                                <a href="#" class="btn btn-google">
                                                    <i data-feather="mail"></i>
                                                </a>
                                                <a href="#" class="btn btn-github">
                                                    <i data-feather="github"></i>
                                                </a>
                                            </div> -->

                        </div>
                    </div>
                    <!-- /Login basic -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="app-assets/vendors/js/extensions/polyfill.min.js"></script>
<script src="app-assets/vendors/js/particles.js/particles.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<script src="app-assets/vendors/js/particles.js/particles.app.js"></script>
<!-- END: Theme JS-->

<script>
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            })
        }
    })

    var pageLoginForm = $('#auth-login-form')

    $(function() {
        'use strict'

        if (pageLoginForm.length) {
            pageLoginForm.validate({
                rules: {
                    username: {
                        required: true
                        // email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    username: 'Enter your Username!',
                    password: 'Enter your Password!'
                }
            })
        }
    })

    function dologin() {
        if (pageLoginForm.valid()) {
            $('.pageloader').fadeIn('slow')

            $.post("<?php echo base_url(); ?>excusme/dologin", $('#auth-login-form').serialize(),
                function(r) {
                    $('.pageloader').fadeOut('slow')
                    if (r.error) {
                        Swal.fire({
                            title: 'Login Failed',
                            text: r.message,
                            icon: 'error',
                            customClass: {
                                confirmButton: 'btn btn-primary'
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $('#username').val('')
                                $('#password').val('')
                                setTimeout(() => {
                                    $('#username').focus()
                                }, 500)
                            }
                        })

                        return false
                    }

                    location = r.redirect
                }, 'json')
            return false
        }
    }
</script>
</body>
<!-- END: Body-->

</html>
