<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="card">
    <h4 class="card-header">Change Password</h4>
    <div class="card-body">
        <form id="formChangePassword" method="POST" onsubmit="return false" novalidate="novalidate">
            <div class="alert alert-warning mb-2 d-none" role="alert">
                <h6 class="alert-heading">Ensure that these requirements are met</h6>
                <div class="alert-body fw-normal">Minimum 8 characters long, uppercase &amp; symbol</div>
            </div>

            <div class="row">
                <div class="mb-2 col-md-6 form-password-toggle">
                    <label class="form-label" for="newPassword">New Password</label>
                    <div class="input-group input-group-merge form-password-toggle">
                        <input class="form-control validate-equalTo-blur" type="text" id="newPassword" name="newPassword" placeholder="············" aria-describedby="newPassword-error" aria-invalid="false">
                        <span class="input-group-text cursor-pointer">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off font-small-4">
                                <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                <line x1="1" y1="1" x2="23" y2="23"></line>
                            </svg>
                        </span>
                    </div><span id="newPassword-error" class="error" style="display: none;"></span>
                </div>

                <div class="mb-2 col-md-6 form-password-toggle">
                    <label class="form-label" for="confirmPassword">Confirm New Password</label>
                    <div class="input-group input-group-merge">
                        <input class="form-control" type="password" name="confirmPassword" id="confirmPassword" placeholder="············" aria-describedby="confirmPassword-error" aria-invalid="false">
                        <span class="input-group-text cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye">
                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                <circle cx="12" cy="12" r="3"></circle>
                            </svg></span>
                    </div><span id="confirmPassword-error" class="error" style="display: none;"></span>
                </div>
                <div>
                    <button type="submit" class="btn btn-primary me-2 waves-effect waves-float waves-light">Change Password</button>
                </div>
            </div>
        </form>
    </div>
</div>