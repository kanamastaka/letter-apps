<?php
defined('BASEPATH') or exit('No direct script access allowed');
$logged = $this->session->userdata('logged');
?>

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header" style="padding-left: 15px">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto">
                <a class="navbar-brand" style="margin: 4px 0px 0px 0px;" href="<?php echo base_url() . 'suratMasuk' ?>">
                    <span class="">
                        <img id="sidebar-logo" src="<?php echo base_url(); ?>/app-assets/images/logo/logo-text.png"
                             alt="" height="44">
                    </span>
                    <!-- <h2 class="brand-text">Vuexy</h2> -->
                </a>
            </li>

            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                        class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                        class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                        data-ticon="disc"></i></a></li>
        </ul>
    </div>

    <div class="shadow-bottom"></div>

    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item <?php echo ($page->menu == 'dash') ? 'active' : '' ?>">
                <a class="d-flex align-items-center" href="<?php echo base_url() . 'dash' ?>">
                    <i data-feather="home"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span>
                </a>
            </li>

            <!--            <li class="nav-item -->
            <?php //echo ($page->menu == 'profile' || $page->menu == 'map') ? 'active' : '' ?><!--">-->
            <!--                <a class="d-flex align-items-center" href="-->
            <?php //echo base_url() . 'map' ?><!--">-->
            <!--                    <i data-feather="home"></i>-->
            <!--                    <span class="menu-title text-truncate" data-i18n="Map">Dashboard</span>-->
            <!--                </a>-->
            <!--            </li>-->

            <li class=" navigation-header"><span data-i18n="User">Manajemen Surat</span><i
                    data-feather="more-horizontal"></i>
            </li>

            <li class="nav-item <?php echo ($page->menu == 'surat-masuk') ? 'active' : '' ?>">
                <a class="d-flex align-items-center" href="<?php echo base_url() . 'suratMasuk' ?>">
                    <i data-feather='log-in'></i>
                    <span class="menu-title text-truncate" data-i18n="surat-masuk">Surat Masuk</span>

                    <span id="surat-masuk-count-el" class="badge badge-light-warning rounded-pill ms-auto me-1"></span>
                </a>
            </li>

            <li class="nav-item <?php echo ($page->menu == 'surat-keluar') ? 'active' : '' ?>">
                <a class="d-flex align-items-center" href="<?php echo base_url() . 'suratKeluar' ?>">
                    <i data-feather='log-out'></i>
                    <span class="menu-title text-truncate" data-i18n="surat-keluar">Surat Keluar</span>

                    <span id="surat-keluar-count-el" class="badge badge-light-warning rounded-pill ms-auto me-1"></span>
                </a>
            </li>

            <li class=" navigation-header"><span data-i18n="User">Manajemen File</span><i
                    data-feather="more-horizontal"></i>
            </li>

            <li class="nav-item <?php echo ($page->menu == 'fileManagementCategory') ? 'active' : '' ?>">
                <a class="d-flex align-items-center" href="<?php echo base_url() . 'fileManagementCategory' ?>">
                    <i data-feather='list'></i>
                    <span class="menu-title text-truncate" data-i18n="fileManagementCategory">Kategori File</span>
                </a>
            </li>

            <li class="nav-item <?php echo ($page->menu == 'fileManagement') ? 'active' : '' ?>">
                <a class="d-flex align-items-center" href="<?php echo base_url() . 'fileManagement' ?>">
                    <i data-feather='file'></i>
                    <span class="menu-title text-truncate" data-i18n="fileManagementCategory">Manajemen File</span>
                </a>
            </li>

            <?php if ($logged['user_type'] == 1) { ?>

                <li class=" navigation-header"><span data-i18n="User">Admin</span><i data-feather="more-horizontal"></i>
                </li>

                <li class="nav-item <?php echo ($page->menu == 'user') ? 'active' : '' ?>">
                    <a class="d-flex align-items-center" href="<?php echo base_url() . 'user' ?>">
                        <i data-feather='user'></i>
                        <span class="menu-title text-truncate" data-i18n="User">User</span>
                    </a>
                </li>

            <?php } ?>

        </ul>
    </div>
</div>

<script>
    suratMasukNotifCount = <?php echo $suratMasukNotifCount?>;
    suratKeluarNotifCount = <?php echo $suratKeluarNotifCount?>;



</script>
