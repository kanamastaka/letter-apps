<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="card">
    <div class="card-body">
        <div class="user-avatar-section">
            <div class="d-flex align-items-center flex-column">
                <img class="img-fluid rounded mt-3 mb-2" src="<?php echo base_url(); ?>app-assets/images/user/no-photo-male.jpeg" height="110" width="110" alt="User avatar" />
                <div class="user-info text-center">
                    <h4><?php echo logged('user_name') ?></h4>
                    <!-- <span class="badge bg-light-secondary">
                                            <?php echo logged('user_type_name') ?>
                                            </span> -->
                </div>
            </div>
        </div>
        <!-- <div class="d-flex justify-content-around my-2 pt-75">
                                    <div class="d-flex align-items-start me-2">
                                        <span class="badge bg-light-primary p-75 rounded">
                                            <i data-feather="check" class="font-medium-2"></i>
                                        </span>
                                        <div class="ms-75">
                                            <h4 class="mb-0">1.23k</h4>
                                            <small>Tasks Done</small>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start">
                                        <span class="badge bg-light-primary p-75 rounded">
                                            <i data-feather="briefcase" class="font-medium-2"></i>
                                        </span>
                                        <div class="ms-75">
                                            <h4 class="mb-0">568</h4>
                                            <small>Projects Done</small>
                                        </div>
                                    </div>
                                </div> -->
        <h4 class="fw-bolder border-bottom pb-50 mb-1 mt-1">General Information</h4>
        <div class="info-container">
            <ul class="list-unstyled">
                <li class="mb-75">
                    <span class="fw-bolder me-25">Username:</span>
                    <span><?php echo logged('user_login') ?></span>
                </li>
                <li class="mb-75 d-none">
                    <span class="fw-bolder me-25">Email:</span>
                    <span><?php echo logged('email') ?></span>
                </li>
                <li class="mb-75 d-none">
                    <span class="fw-bolder me-25">Status:</span>
                    <span class="badge bg-light-success">Active</span>
                </li>
                <li class="mb-75">
                    <span class="fw-bolder me-25">Role:</span>
                    <span><?php echo logged('user_type_name') ?></span>
                </li>
            </ul>
            <div class="d-flex justify-content-center pt-2">
                <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#editUser" data-bs-toggle="modal">
                    Edit
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editUser" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
        <div class="modal-content">
            <div class="modal-header bg-transparent">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-5 px-sm-5 pt-50">
                <div class="text-center mb-2">
                    <h1 class="mb-1">General Information</h1>
                </div>
                <form id="editUserForm" class="row gy-1 pt-75" onsubmit="return editUserFormSubmit()">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>" />
                    <div class="col-12">
                        <label class="form-label" for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control" value="<?php echo logged('user_login') ?>" placeholder="" />
                    </div>
                    <div class="col-12">
                        <label class="form-label" for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control" value="<?php echo logged('email') ?>" placeholder="" />
                    </div>
                    <div class="col-12 text-center mt-2 pt-50">
                        <button type="submit" class="btn btn-primary me-1">Submit</button>
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                            Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>