<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern navbar-sticky footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="">


    <!-- <nav class="header-navbar navbar navbar-expand-lg align-items-center fixed-top navbar-dark navbar-shadow container-xxl"> -->
    <nav class="header-navbar navbar navbar-shadow align-items-center navbar-light navbar-expand fixed-top">
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav d-xl-none">
                    <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a></li>
                </ul>

                <!-- <ul class="nav navbar-nav">
                    <li class="nav-item d-none d-lg-block">
                        <a class="nav-link nav-link-style"><i class="ficon" data-feather="sun"></i></a>
                    </li>
                </ul> -->

                <h2 class="content-header-title float-start mb-0 ms-1">
                    <?php echo $page->title ?>
                </h2>
            </div>
            <ul class="nav navbar-nav align-items-center ms-auto">
                <li id="alert-checksite-container" class="nav-item d-none">
                    <div class="me-1 position-relative">
                        <div class="pulse-alert-warning bg-warning rounded-circle justify-content-center align-items-center d-flex">
                            <i class="blink ficon text-white" data-feather="alert-triangle"></i>
                        </div>
                    </div>
                </li>
                <li id="alert-container" class="nav-item d-none">
                    <div class="me-1 position-relative">
                        <div class="pulse-alert bg-danger rounded-circle justify-content-center align-items-center d-flex">
                            <i class="blink ficon text-white" data-feather="alert-circle"></i>
                        </div>
                    </div>
                </li>
                <li id="notification-container" class="nav-item d-none">
                    <div class="me-1 position-relative">
                        <div class="pulse-alert bg-danger rounded-circle justify-content-center align-items-center d-flex">
                            <i class="blink ficon text-white" data-feather="alert-circle"></i>
                        </div>
                    </div>
                </li>
                <li class="nav-item d-none">
                    <div class="me-1 position-relative">
                        <button id="alert-btn" onclick="playAlertSound()">asd</button>
                    </div>
                </li>
                <li class="nav-item d-none">
                    <div class="me-1 position-relative">
                        <a class="nav-link nav-link-style"><i class="ficon" data-feather="sun"></i></a>
                    </div>
                </li>
                <li class="nav-item dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none"><span class="user-name fw-bolder">
                                <?php echo logged('user_name') ?>
                            </span>
<!--                            <span class="user-status">-->
<!--                                --><?php //echo logged('user_type_name') ?>
<!--                            </span>-->
                        </div>
                        <span class="avatar">
                            <img class="round" src="<?php echo base_url(); ?>app-assets/images/user/no-photo-male.jpeg" alt="avatar" height="40" width="40">
                            <span class="avatar-status-online"></span>
                        </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                        <a class="dropdown-item" href="<?php echo base_url() . 'profile/changePassword' ?>"><i class="me-50" data-feather="key"></i> Change Password</a>
                        <!-- <a class="dropdown-item" href="#"><i class="me-50" data-feather="mail"></i> Inbox</a>
                        <a class="dropdown-item" href="#"><i class="me-50" data-feather="check-square"></i> Task</a>
                        <a class="dropdown-item" href="#"><i class="me-50" data-feather="message-square"></i> Chats</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="#"><i class="me-50" data-feather="settings"></i> Settings</a>
                        <a class="dropdown-item" href="#"><i class="me-50" data-feather="credit-card"></i> Pricing</a>
                        <a class="dropdown-item" href="#"><i class="me-50" data-feather="help-circle"></i> FAQ</a> -->
                        <a class="dropdown-item" href="javascript:dologout()"><i class="me-50" data-feather="power"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div id="HTML5Audio">
        <input id="audiofile" type="text" value="" style="display: none;" />
        <input id="notificationfile" type="text" value="" style="display: none;" />
    </div>
    <audio id="myaudio"></audio>
    <audio id="mynotificationaudio"></audio>

    <script>



        function dologout() {
            Swal.fire({
                title: 'Logout sekarang ?',
                icon: 'warning',
                showCancelButton: true,
                customClass: {
                    confirmButton: 'btn btn-lg btn-outline-success me-1',
                    cancelButton: 'btn btn-lg btn-outline-secondary'
                },
                confirmButtonText: 'YA',
                cancelButtonText: 'TIDAK',
                buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {

                    window.location = '<?php echo base_url(); ?>logout';

                }
            })
        }
    </script>
