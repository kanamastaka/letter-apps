<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="card">
    <h4 class="card-header">User Activity</h4>
    <div class="card-body pt-1">
        <ul class="timeline ms-50">
            <li class="timeline-item">
                <span class="timeline-point timeline-point-indicator"></span>
                <div class="timeline-event">
                    <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                        <h6>User login</h6>
                        <span class="timeline-event-time me-1">12 min ago</span>
                    </div>
                    <p>User login at 2:12pm</p>
                </div>
            </li>
            <li class="timeline-item">
                <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                <div class="timeline-event">
                    <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                        <h6>Create a new react project for client</h6>
                        <span class="timeline-event-time me-1">2 day ago</span>
                    </div>
                    <p>Add files to new design folder</p>
                </div>
            </li>
            <li class="timeline-item">
                <span class="timeline-point timeline-point-danger timeline-point-indicator"></span>
                <div class="timeline-event">
                    <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                        <h6>Create Invoices for client</h6>
                        <span class="timeline-event-time me-1">12 min ago</span>
                    </div>
                    <p class="mb-0">Create new Invoices and send to Leona Watkins</p>
                </div>
            </li>
        </ul>
    </div>
</div>