<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.4.0/socket.io.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/modernizr-custom.js"></script>

<script>
    const socket = io("<?php echo $this->config->item("socket_url") . '?username=' . logged('user_login'); ?>")
    jQuery(document).ready(function() {
        if (Modernizr.audio) {
            if (Modernizr.audio.wav) {
                $('#notificationfile').val(
                    "<?php echo base_url(); ?>app-assets/sound/notification.wav"
                )
            }
            if (Modernizr.audio.mp3) {
                $('#notificationfile').val(
                    "<?php echo base_url(); ?>app-assets/sound/notification.mp3"
                )
            }
        } else {
            $('#HTML5Audio').hide()
            $('#notificationfile').html(
                '<embed src="<?php echo base_url(); ?>app-assets/sound/notification.wav" autostart=false width=1 height=1 id="LegacySound" enablejavascript="true" >'
            )
        }

        socket.on('suratMasukDisposisiEmit', function(data) {
            console.log('suratMasukDisposisiEmit', data)
            if (typeof gridSuratMasuk !== "undefined") {
                gridSuratMasuk.ajax.reload()
            }

            var user_id = "<?php echo logged('user_id'); ?>"
            var verifikator_level = "<?php echo logged('verifikator_level'); ?>"

            if (data.user_id != user_id && data.data.disposisi_user_id == user_id && data.data.status_verifikasi == 'DISPOSISI') {

                var count = parseInt($('#surat-masuk-count-el').text())
                if (isNaN(count)) {
                    count = 0
                }
                $('#surat-masuk-count-el').show().html(count + 1)

                playNotificationSound()

                window.toastr.clear()

                toastr['warning']('Surat masuk telah didisposisikan kepada Anda', 'NOTIFIKASI', {
                    closeButton: true,
                    tapToDismiss: false
                })
            }
        })

        socket.on('suratMasukVerifCompletedEmit', function(data) {
            console.log('suratMasukVerifCompletedEmit')
            if (typeof gridSuratMasuk !== "undefined") {
                gridSuratMasuk.ajax.reload()
            }

            var user_id = "<?php echo logged('user_id'); ?>"
            var jabatan_id = "<?php echo logged('jabatan_id'); ?>"

            if (data.user_id != user_id && data.data.status_verifikasi == 'TERVERIFIKASI' && data.data.tujuan.split(',').indexOf(jabatan_id) > -1) {

                var count = parseInt($('#surat-masuk-count-el').text())
                if (isNaN(count)) {
                    count = 0
                }
                $('#surat-masuk-count-el').show().html(count + 1)

                playNotificationSound()

                window.toastr.clear()

                toastr['warning']('Surat masuk sudah terverifikasi', 'NOTIFIKASI', {
                    closeButton: true,
                    tapToDismiss: false
                })
            }
        })

        socket.on('suratMasukVerifEmit', function(data) {
            console.log('suratMasukVerifEmit')
            if (typeof gridSuratMasuk !== "undefined") {
                gridSuratMasuk.ajax.reload()
            }

            var user_id = "<?php echo logged('user_id'); ?>"
            var verifikator_level = "<?php echo logged('verifikator_level'); ?>"

            if (data.user_id != user_id && data.data.status_verifikasi != 'TERVERIFIKASI' && (data.data.level_verifikasi == verifikator_level || (data.data.level_verifikasi == verifikator_level - 1 && data.data.status_verifikasi == 'SEDANG DIVERIFIKASI'))) {

                var count = parseInt($('#surat-masuk-count-el').text())
                if (isNaN(count)) {
                    count = 0
                }
                $('#surat-masuk-count-el').show().html(count + 1)

                playNotificationSound()

                window.toastr.clear()

                toastr['warning']('Surat masuk membutuhkan verifikasi Anda', 'NOTIFIKASI', {
                    closeButton: true,
                    tapToDismiss: false
                })
            }
        })

        socket.on('suratMasukViewEmit', function(data) {

            var user_id = "<?php echo logged('user_id'); ?>"

            if (data.user_id == user_id) {

                var count = parseInt($('#surat-masuk-count-el').text())
                if (count - 1 > 0) {
                    $('#surat-masuk-count-el').show().html(count - 1)
                } else {
                    $('#surat-masuk-count-el').hide().html(0)
                }
            }
        })

        socket.on('suratKeluarVerifEmit', function(data) {
            if (typeof gridSuratKeluar !== "undefined") {
                gridSuratKeluar.ajax.reload()
            }

            var user_id = "<?php echo logged('user_id'); ?>"
            var verifikator_level = "<?php echo logged('verifikator_level'); ?>"

            if (data.user_id != user_id && data.data.status_verifikasi != 'TERVERIFIKASI' && (data.data.level_verifikasi == verifikator_level || (data.data.level_verifikasi == verifikator_level - 1 && data.data.status_verifikasi == 'SEDANG DIVERIFIKASI'))) {

                var count = parseInt($('#surat-keluar-count-el').text())
                if (isNaN(count)) {
                    count = 0
                }
                $('#surat-keluar-count-el').show().html(count + 1)

                playNotificationSound()

                window.toastr.clear()

                toastr['warning']('Surat keluar membutuhkan verifikasi Anda', 'NOTIFIKASI', {
                    closeButton: true,
                    tapToDismiss: false
                })
            }
        })

        socket.on('suratKeluarViewEmit', function(data) {

            var user_id = "<?php echo logged('user_id'); ?>"

            if (data.user_id == user_id) {

                var count = parseInt($('#surat-keluar-count-el').text())
                if (count - 1 > 0) {
                    $('#surat-keluar-count-el').show().html(count - 1)
                } else {
                    $('#surat-keluar-count-el').hide().html(0)
                }
            }
        })

        // console.log('$suratMasukNotifCount', suratMasukNotifCount)
        // console.log('suratKeluarNotifCount', suratKeluarNotifCount)

        if (suratMasukNotifCount > 0) {
            $('#surat-masuk-count-el').show().html(suratMasukNotifCount)
        } else {
            $('#surat-masuk-count-el').hide()
        }

        if (suratKeluarNotifCount > 0) {
            $('#surat-keluar-count-el').show().html(suratKeluarNotifCount)
        } else {
            $('#surat-keluar-count-el').hide()
        }
    })
</script>
