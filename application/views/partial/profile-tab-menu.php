<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<ul class="nav nav-pills mb-2">
    <li class="nav-item">
        <a class="nav-link <?php echo ($page->tab == 'account') ? 'active' : '' ?>" href="<?php echo base_url() . 'profile' ?>">
            <i data-feather="user" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Account</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link <?php echo ($page->tab == 'security') ? 'active' : '' ?>" href="<?php echo base_url() . 'profile/security' ?>">
            <i data-feather="lock" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Security</span>
        </a>
    </li>
</ul>