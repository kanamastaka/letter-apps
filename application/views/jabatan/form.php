<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

<form id="formJabatan" class="row gy-1 gx-2 mt-75" onsubmit="return formJabatanSubmit()">
</form>

<template id="formJabatanTpl">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>"/>
    <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>"/>
    <input type="hidden" name="id" id="id"/>

    <div class="row gy-0 gx-0 mt-75">

        <div class="col-sm-12 col-lg-12">
            <div class="row gy-1 gx-2">
                <div class="col-12">
                    <label class="form-label" for="nama">Nama Jabatan</label>
                    <input type="text" id="nama" name="nama" class="form-control"/>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-50">
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        Simpan
                    </button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

</template>

<script>
    var formJabatan = $('#formJabatan'),
        formJabatanLoading = false,
        formJabatanMode = 'ADD';

    $(window).on('load', function() {

        $('#formJabatanModal').on('shown.bs.modal', function(e) {
            window.toastr.clear()

            $('button[type=submit], input[type=submit]', formJabatan).find('.spinner').hide()
        })

        $('#formJabatanModal').on('hidden.bs.modal', function(e) {
            e.preventDefault()
            e.stopPropagation()

            $('button[type=submit], input[type=submit]', formJabatan).find('.spinner').hide()

            formJabatan.validate().resetForm()
            formJabatan.my('remove')
        })

        formJabatan.submit(function() {
            if (formJabatan.valid()) {
                $('button[type=submit], input[type=submit]', formJabatan).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formJabatan).find('.spinner').show()
            }
        })
    })

    function formJabatanInit(idJabatan = null) {
        if (idJabatan) {
            $.post("<?php echo base_url() ?>jabatan/getById", {
                    id: idJabatan,
                    modeView: false
                },
                function(resp) {
                    if (resp.error) {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    }
                    formJabatanRender(resp.data.id, resp.data)
                }, 'json')
        } else {
            formJabatanRender()
        }
    }

    function formJabatanRender(idJabatan = null, formData = null) {

        formJabatanMode = idJabatan ? 'EDIT' : 'ADD'
        var title = idJabatan ? 'Edit Data' : 'Tambah Data'

        $('#formJabatanModalTitle').html(title + ' <?php echo $page->title ?>')

        // Manifest
        var manifest = {

            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id: <?php echo logged('user_id'); ?>,
                id: null,
                nama: null,
            },

            // Init function
            init: function($form, runtime) {
                $form.empty().html($('#formJabatanTpl').html())

                $form.then(function() { // Fade when start succeds
                    $('input:visible:enabled:first', formJabatan).focus()
                    formJabatan.validate({
                        rules: {
                            nama: {
                                required: true
                            },
                        },
                        messages: {
                            nama: 'nama harus diisi',
                        }
                    })
                })

                if (idJabatan != null) {
                    $('#formJabatanModalTitle').html('Edit <?php echo $page->title ?> Data')
                }

                $('#formJabatanModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show')
            },

            // Bindings
            ui: {
                '#id': {
                    bind: 'id'
                },
                '#nama': {
                    bind: 'nama'
                },
            }
        }

        // Init $.my over DOM node
        formJabatan.my(manifest, formData)
    }

    function formJabatanSubmit() {
        if (formJabatan.valid()) {

            var data = formJabatan.my('data')

            var formData = new FormData()

            // Append the text fields
            formData.append('id', data.id)
            formData.append('nama', data.nama)

            fetch("<?php echo base_url(); ?>jabatan/process_save", {
                method: 'POST',
                // headers: {
                //     'X-CSRF-TOKEN': formProfile.querySelector('[name="_token"]').value
                // },
                body: formData
            }).then((resp) => resp.json())
              .then(resp => {

                  if (resp.error) {
                      toastr['error'](resp.message, 'ERROR', {
                          closeButton: true,
                          tapToDismiss: false
                      })
                  } else {
                      // base64data = null
                      // $('#org-logo').attr('src', 'app-assets/images/jabatan/default.jpg')
                      gridJabatan.ajax.reload()

                      toastr['success'](' ', resp.message, {
                          closeButton: false,
                          tapToDismiss: true
                      })

                      if (formJabatanMode == 'ADD') {

                          formJabatan.trigger('reset')
                          formJabatan.validate().resetForm()

                          $('input:visible:enabled:first', formJabatan).focus()
                      } else {
                          $('#formJabatanModal').modal('hide')
                      }
                  }

                  $('button[type=submit], input[type=submit]', formJabatan).prop('disabled', false)
                  $('button[type=submit], input[type=submit]', formJabatan).find('.spinner').hide()

                  return false
              })

            return false
        }

        return false
    }
</script>
