<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="<?php echo $this->config->item("appdesc"); ?>">
    <meta name="keywords" content="surat, arsip">
    <meta name="author" content="<?php echo $this->config->item("appowner"); ?>">
    <title><?php echo $this->config->item("apptitle"); ?></title>

    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(); ?>app-assets/images/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(); ?>app-assets/images/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(); ?>app-assets/images/ico/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url(); ?>app-assets/images/ico/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url(); ?>app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/themes/bordered-layout.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/pages/authentication.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-toastr.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click"
      data-menu="vertical-menu-modern" data-col="blank-page">
<!-- BEGIN: Content-->
<div class="app-content content login-bg">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div id="auth-particles" class="auth-wrapper auth-basic px-2">
                <div class="auth-inner my-2" style="position:absolute;">
                    <div class="my-2 text-center">

                    </div>
                    <div class="my-2 text-center">
                        <a href="<?php echo base_url(); ?>" class="brand-logo m-0">
                            <img src="<?php echo base_url(); ?>/app-assets/images/logo/logo-light.png" alt=""
                                 height="150">
                        </a>
                        <h2 class="brand-text text-white text-uppercase mt-2 fs-4"><?php echo $this->config->item("apptitle"); ?></h2>
                    </div>

                    <!-- Login basic -->
                    <div class="card mb-0">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                               value="<?php echo $this->security->get_csrf_hash(); ?>"/>
                        <input type="hidden" id="otpId" name="otpId" value="<?php echo $otpId; ?>"/>

                        <div class="card-body">
                            <h2 class="text-center card-title fw-bolder mb-1 mt-1">Two Step Verification</h2>

                            <p class="text-center card-text mb-75 fs-5">
                                Kami telah mengirimkan kode OTP ke email <?php echo $email; ?>.
                            </p>

                            <p class="text-center card-text mb-75 fs-5">Masukkan kode OTP pada kolom di bawah ini.</p>

                            <div id="otp_target"></div>

                            <!--                            <button class="btn btn-primary btn-lg w-100 my-2" id="ok">OK</button>-->

                            <p class="text-center mt-2">
                                <span>Tidak mendapatkan kode OTP ?</span>
                                <a id="resendBtn" href="Javascript:void(0)"><span>&nbsp;Kirim Ulang OTP</span></a>
                            </p>

                            <p class="text-center mt-0 text-warning">
                                <span id="exp-info"></span>
                            </p>

                        </div>
                    </div>
                    <!-- /Login basic -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/polyfill.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/particles.js/particles.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/otpdesigner.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/moment/moment-with-locales.min.js"></script>

<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/core/app.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/particles.js/particles.app.js"></script>
<!-- END: Theme JS-->

<script>
    var countDownDate = <?php echo floatval($otp_timestamp_to) ?>

    var x = setInterval(function() {
        diff = moment.unix(countDownDate).diff(moment())

        if (diff <= 0) {
            clearInterval(x)
            // If the count down is finished, write some text
            $('#exp-info').text('')
        } else {
            $('#exp-info').text(moment.utc(diff).format('HH:mm:ss'))
        }

    }, 1000)

    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            })
        }
    })

    $(document).ready(function() {
        $('#otp_target').otpdesigner({
            typingDone: function(code) {
                checkOtp(code)
            },
            length: 6,
            onlyNumbers: true,
            inputsClasses: 'form-control auth-input height-50 text-center numeral-mask mx-25 mb-1'
        })

        $('#ok').on('click', function() {
            window.toastr.clear()
            let result = $('#otp_target').otpdesigner('code')
            if (result.done) {
                checkOtp(result.code)
            } else {
                $('#otp_target').otpdesigner('focus')
                toastr['error']('Kode OTP tidak sesuai', 'ERROR', {
                    closeButton: true,
                    tapToDismiss: false
                })
            }
        })

        $('#resendBtn').on('click', function() {
            window.toastr.clear()
            $('.pageloader').fadeIn('slow')

            $.post("<?php echo base_url(); ?>otp/resendOtp", {
                    otpId: $('#otpId').val()
                },
                function(r) {
                    $('.pageloader').fadeOut('slow')
                    if (!r.error) {
                        location = r.redirect
                    }
                }, 'json')

        })

        setTimeout(function() {
            $('#otp_target').otpdesigner('focus')
        }, 1000)
    })

    function checkOtp(code) {
        $('.pageloader').fadeIn('slow')

        $.post("<?php echo base_url(); ?>otp/checkOtp", {
                otpCode: code,
                otpId: $('#otpId').val()
            },
            function(r) {
                $('.pageloader').fadeOut('slow')

                if (r.error) {
                    Swal.fire({
                        title: 'Error',
                        text: r.message,
                        icon: 'error',
                        customClass: {
                            confirmButton: 'btn btn-primary'
                        },
                        buttonsStyling: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#otp_target').otpdesigner('clear')

                            setTimeout(function() {
                                $('#otp_target').otpdesigner('focus')
                            }, 1000)
                        }
                    })

                    return false
                } else {
                    Swal.fire({
                        title: r.message,
                        // text: r.message,
                        icon: 'success',
                        customClass: {
                            confirmButton: 'btn btn-primary'
                        },
                        buttonsStyling: false
                    }).then((result) => {
                        location = r.redirect
                    })
                }
            }, 'json')
        return false
    }
</script>
</body>
<!-- END: Body-->

</html>
