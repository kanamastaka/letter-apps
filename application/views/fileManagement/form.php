<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

<form id="formFileManagement" class="row gy-1 gx-2 mt-75" onsubmit="return formFileManagementSubmit()">
</form>

<template id="formFileManagementTpl">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>"/>
    <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>"/>
    <input type="hidden" name="id" id="id"/>

    <div class="row gy-0 gx-0 mt-75">

        <div class="col-12">
            <div class="row gy-1 gx-2">

                <div class="col-md-12">
                    <label class="form-label" for="deskripsi">Deskripsi</label>
                    <input type="text" id="deskripsi" name="deskripsi" class="form-control"/>
                </div>

                <div class="col-md-12">
                    <label class="form-label" for="category_id">Kategori</label>
                    <select class="select2 select2-data-ajax form-select" id="category_id" name="category_id"></select>
                </div>

                <div class="col-sm-6 col-12">
<!--                    <label class="form-label" for="is_public">Public</label>-->
                    <div class="demo-inline-spacing position-relative">
                        <div class="form-check form-check-inline mt-1" style="min-width: 65px;">
                            <input class="form-check-input" type="checkbox" name="is_public" id="is_public1" value="1">
                            <label class="form-check-label" for="is_public1">Public</label>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <label for="file" class="form-label">File</label>
                    <input class="form-control" type="file" id="file" name="file" accept="application/pdf,application/msword,
  application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/*"/>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-50">
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        Simpan
                    </button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="cropper-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title" id="modalLabel">&nbsp;</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-sm-5 mx-50 pb-5">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-8">
                                <!--  default image where we will set the src via jquery-->
                                <img id="image">
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
                </div>
            </div>
        </div>
    </div>


</template>

<script>
    var formFileManagement = $('#formFileManagement'),
        categoryIdEl = null,
        categoryIdEl = null,
        formFileManagementLoading = false,
        formFileManagementMode = 'ADD'

    $(window).on('load', function() {

        $('#formFileManagementModal').on('shown.bs.modal', function(e) {

            $('button[type=submit], input[type=submit]', formFileManagement).find('.spinner').hide()

            categoryIdEl = $('#category_id', formFileManagement)
            categoryIdEl.wrap('<div class="position-relative"></div>')
                        .prepend('<option selected=""></option>').select2({
                placeholder: 'Pilih Kategori',
                ajax: {
                    url: '<?php echo base_url(); ?>fileManagementCategory/get_select2_list',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                            page: params.page
                        }
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 10) < data.total
                                // more: data.total
                            }
                        }
                    },
                    cache: true,
                    dataType: 'json',
                    type: 'GET'
                },
                // minimumResultsForSearch: -1,
                allowClear: true,
                dropdownAutoWidth: true,
                dropdownParent: categoryIdEl.parent(),
                width: '100%'
            }).on('change.select2', function(e) {
                formFileManagement.validate().resetForm()
            })

            if (formFileManagement.my('data').category_id !== undefined) {
                if (formFileManagement.my('data').category !== undefined) {
                    var newOption = new Option(formFileManagement.my('data').category.text, formFileManagement.my('data').category.id, false, false)
                    categoryIdEl.append(newOption)
                }

                categoryIdEl.val(formFileManagement.my('data').category_id).trigger('change')
            }
        })

        $('#formFileManagementModal').on('hidden.bs.modal', function(e) {
            e.preventDefault()
            e.stopPropagation()

            // categoryIdEl.select2('destroy')
            $('button[type=submit], input[type=submit]', formFileManagement).find('.spinner').hide()
            formFileManagement.my('data').is_public = null
            formFileManagement.validate().resetForm()
            formFileManagement.my('remove')
        })

        formFileManagement.submit(function() {
            if (formFileManagement.valid()) {
                $('button[type=submit], input[type=submit]', formFileManagement).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formFileManagement).find('.spinner').show()
            }
        })
    })

    function formFileManagementInit(idFileManagement = null) {



        console.log('idFileManagement', idFileManagement)

        if (idFileManagement) {
            $.post("<?php echo base_url() ?>fileManagement/getById", {
                    id: idFileManagement
                },
                function(r) {
                    console.log('r', r)
                    if (r.error) {
                        $('#formFileManagementModal').modal('hide')

                        toastr['error'](r.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    } else {
                        $('#formFileManagementModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        }).modal('show')

                        formFileManagementRender(r.data.id, r.data)
                    }
                }, 'json')
        } else {
            $('#formFileManagementModal').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show')

            formFileManagementRender()
        }
    }

    function formFileManagementRender(idFileManagement = null, formData = null) {
        formFileManagementMode = idFileManagement ? 'EDIT' : 'ADD'
        var title = idFileManagement ? 'Edit' : 'Tambah'

        $('#formFileManagementModalTitle').html(title + ' Data')

        // Manifest
        var manifest = {

            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id: <?php echo logged('user_id'); ?>,
                id: null,
                category_id: null,
                deskripsi: null,
                is_public: null,
                file: null
            },

            // Init function
            init: function($form, runtime) {
                $form.empty().html($('#formFileManagementTpl').html())

                $form.then(function() { // Fade when start succeds
                    $('input:visible:enabled:first', formFileManagement).focus()

                    var formFileManagementValRules = {
                        category_id: {
                            required: true
                        },
                        deskripsi: {
                            required: true
                        },
                        file: {
                            required: true
                        }
                    }

                    formFileManagement.validate({
                        rules: formFileManagementValRules,
                        messages: {
                            category_id: 'Kategori harus diisi',
                            deskripsi: 'Deskripsi file harus diisi',
                            file: 'File harus diisi'
                        }
                    })
                })

                if (idFileManagement != null) {
                    $('#formFileManagementModalTitle').html('Edit Data')
                }
            },

            // Bindings
            ui: {
                '#id': {
                    bind: 'id'
                },
                '#category_id': {
                    bind: 'category_id'
                },
                '#deskripsi': {
                    bind: 'deskripsi'
                },
                'input[name=\'is_public\']': {
                    bind: 'is_public'
                },
                // '#is_public': {
                //     bind: 'is_public'
                // },
                '#file': {
                    bind: 'file'
                }
            }
        }

        // Init $.my over DOM node
        formFileManagement.my(manifest, formData)

        // if (formData.org_id) {
        //     categoryIdEl.select2().select2(formData.org_id)
        // }
    }

    function formFileManagementSubmit() {

        if (formFileManagement.valid()) {

            var data = formFileManagement.my('data')

            var formData = new FormData(),
                fileInput = document.querySelector('#file')

            formData.append('id', data.id)
            formData.append('category_id', data.category_id)
            formData.append('deskripsi', data.deskripsi)
            formData.append('is_public', data.is_public)
            formData.append('user_id', data.user_id)

            let docFile = fileInput.files
            if (docFile.length > 0) {
                formData.append('file', docFile[0])
            }

            fetch("<?php echo base_url(); ?>fileManagement/process_save", {
                method: 'POST',
                // headers: {
                //     'X-CSRF-TOKEN': formProfile.querySelector('[name="_token"]').value
                // },
                body: formData
            }).then((resp) => resp.json())
              .then(resp => {

                  if (resp.error) {
                      toastr['error'](resp.message, 'ERROR', {
                          closeButton: true,
                          tapToDismiss: false
                      })
                  } else {

                      gridFileManagement.ajax.reload()

                      toastr['success'](' ', resp.message, {
                          closeButton: false,
                          tapToDismiss: true
                      })

                      if (formFileManagementMode == 'ADD') {

                          formFileManagement.trigger('reset')
                          formFileManagement.validate().resetForm()

                          $('input:visible:enabled:first', formFileManagement).focus()
                          categoryIdEl.val(null).trigger('change')
                          formFileManagement.my('data').is_public = null
                      } else {
                          $('#formFileManagementModal').modal('hide')
                      }
                  }

                  $('button[type=submit], input[type=submit]', formFileManagement).prop('disabled', false)
                  $('button[type=submit], input[type=submit]', formFileManagement).find('.spinner').hide()

                  return false
              })

            return false
        }

        return false
    }
</script>
