<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

<form id="formShareFileManagement" class="row gy-1 gx-2 mt-75" onsubmit="return formShareFileManagementSubmit()">
</form>

<template id="formShareFileManagementTpl">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>"/>
    <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>"/>
    <input type="hidden" name="id" id="id"/>

    <div class="row gy-0 gx-0 mt-75">

        <div class="col-12">
            <div class="row gy-1 gx-2">

                <div class="col-md-12">
                    <label class="form-label" for="user_list_id">User</label>
                    <select class="select2 select2-data-ajax form-select" id="user_list_id"
                            name="user_list_id" multiple></select>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-50">
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        Simpan
                    </button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="cropper-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title" id="modalLabel">&nbsp;</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-sm-5 mx-50 pb-5">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-8">
                                <!--  default image where we will set the src via jquery-->
                                <img id="image">
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
                </div>
            </div>
        </div>
    </div>


</template>

<script>
    var formShareFileManagement = $('#formShareFileManagement'),
        userListIdEl = null,
        formShareFileManagementLoading = false,
        formShareFileManagementMode = 'ADD'

    $(window).on('load', function() {

        $('#formShareFileManagementModal').on('shown.bs.modal', function(e) {

            $('button[type=submit], input[type=submit]', formShareFileManagement).find('.spinner').hide()

            userListIdEl = $('#user_list_id', formShareFileManagement)
            userListIdEl.wrap('<div class="position-relative"></div>')
                // .prepend('<option selected=""></option>')
                        .select2({
                            placeholder: 'Pilih User',
                            ajax: {
                                url: '<?php echo base_url(); ?>user/get_select2_list',
                                delay: 250,
                                data: function(params) {
                                    return {
                                        q: params.term,
                                        page: params.page
                                    }
                                },
                                processResults: function(data, params) {
                                    params.page = params.page || 1
                                    return {
                                        results: data.items,
                                        pagination: {
                                            more: (params.page * 10) < data.total
                                            // more: data.total
                                        }
                                    }
                                },
                                cache: true,
                                dataType: 'json',
                                type: 'GET'
                            },
                            // minimumResultsForSearch: -1,
                            allowClear: true,
                            dropdownAutoWidth: true,
                            dropdownParent: userListIdEl.parent(),
                            width: '100%'
                        }).on('change.select2', function(e) {
                formShareFileManagement.validate().resetForm()
            })

            if (formShareFileManagement.my('data').user_list_option !== undefined && formShareFileManagement.my('data').user_list_option.length > 0) {
                var user_list_option = formShareFileManagement.my('data').user_list_option

                user_list_option.each(function(val, i){
                    var newOption = new Option(val.text, val.id, false, false)
                    userListIdEl.append(newOption)
                })

                userListIdEl.val(formShareFileManagement.my('data').user_list).trigger('change')

            }
        })

        $('#formShareFileManagementModal').on('hidden.bs.modal', function(e) {
            e.preventDefault()
            e.stopPropagation()

            // userListIdEl.select2('destroy')
            $('button[type=submit], input[type=submit]', formShareFileManagement).find('.spinner').hide()
            formShareFileManagement.my('data').is_public = null
            formShareFileManagement.validate().resetForm()
            formShareFileManagement.my('remove')
        })

        formShareFileManagement.submit(function() {
            if (formShareFileManagement.valid()) {
                $('button[type=submit], input[type=submit]', formShareFileManagement).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formShareFileManagement).find('.spinner').show()
            }
        })
    })

    function formShareFileManagementInit(idFileManagement = null) {

        $('#formShareFileManagementModal').modal({
            backdrop: 'static',
            keyboard: false
        }).modal('show')

        if (idFileManagement) {
            $.post("<?php echo base_url() ?>fileManagement/getByIdForShare", {
                    id: idFileManagement
                },
                function(r) {
                    if (r.error) {
                        return false
                    }

                    formShareFileManagementRender(r.data.id, r.data)
                }, 'json')
        } else {
            formShareFileManagementRender()
        }
    }

    function formShareFileManagementRender(idFileManagement = null, formData = null) {
        formShareFileManagementMode = idFileManagement ? 'EDIT' : 'ADD'
        // var title = idFileManagement ? 'Edit' : 'Add'
        var title = 'Share'

        $('#formShareFileManagementModalTitle').html(title + ' <?php echo $page->title ?>')

        // Manifest
        var manifest = {

            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id: <?php echo logged('user_id'); ?>,
                id: null,
                user_list_id: null
            },

            // Init function
            init: function($form, runtime) {
                $form.empty().html($('#formShareFileManagementTpl').html())

                $form.then(function() { // Fade when start succeds
                    $('input:visible:enabled:first', formShareFileManagement).focus()

                    var formShareFileManagementValRules = {
                        user_list_id: {
                            required: true
                        }
                    }

                    formShareFileManagement.validate({
                        rules: formShareFileManagementValRules,
                        messages: {
                            user_list_id: 'User harus dipilih'
                        }
                    })
                })

                if (idFileManagement != null) {
                    $('#formShareFileManagementModalTitle').html('Share File Data')
                }
            },

            // Bindings
            ui: {
                '#id': {
                    bind: 'id'
                },
                '#user_list_id': {
                    bind: 'user_list_id'
                }
                // '#deskripsi': {
                //     bind: 'deskripsi'
                // },
                // 'input[name=\'is_public\']': {
                //     bind: 'is_public'
                // },
                // '#is_public': {
                //     bind: 'is_public'
                // },
                // '#file': {
                //     bind: 'file'
                // }
            }
        }

        // Init $.my over DOM node
        formShareFileManagement.my(manifest, formData)

        // if (formData.org_id) {
        //     userListIdEl.select2().select2(formData.org_id)
        // }
    }

    function formShareFileManagementSubmit() {

        if (formShareFileManagement.valid()) {

            var data = formShareFileManagement.my('data')

            var formData = new FormData()

            formData.append('id', data.id)
            formData.append('user_list_id', data.user_list_id)
            formData.append('user_id', data.user_id)

            fetch("<?php echo base_url(); ?>fileManagement/share", {
                method: 'POST',
                // headers: {
                //     'X-CSRF-TOKEN': formProfile.querySelector('[name="_token"]').value
                // },
                body: formData
            }).then((resp) => resp.json())
              .then(resp => {

                  if (resp.error) {
                      toastr['error'](resp.message, 'ERROR', {
                          closeButton: true,
                          tapToDismiss: false
                      })
                  } else {

                      gridFileManagement.ajax.reload()

                      toastr['success'](' ', resp.message, {
                          closeButton: false,
                          tapToDismiss: true
                      })

                      if (formShareFileManagementMode == 'ADD') {

                          formShareFileManagement.trigger('reset')
                          formShareFileManagement.validate().resetForm()

                          $('input:visible:enabled:first', formShareFileManagement).focus()
                          userListIdEl.val(null).trigger('change')
                          formShareFileManagement.my('data').is_public = null
                      } else {
                          $('#formShareFileManagementModal').modal('hide')
                      }
                  }

                  $('button[type=submit], input[type=submit]', formShareFileManagement).prop('disabled', false)
                  $('button[type=submit], input[type=submit]', formShareFileManagement).find('.spinner').hide()

                  return false
              })

            return false
        }

        return false
    }
</script>
