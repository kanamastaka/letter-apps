<!DOCTYPE html>
<html class="loading light-layout" lang="en" data-layout="light-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="<?php echo $this->config->item("appdesc"); ?>">
    <meta name="keywords" content="surat, arsip">

    <meta name="author" content="<?php echo $this->config->item("appowner"); ?>">
    <title><?php echo $this->config->item("apptitle"); ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="app-assets/images/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="app-assets/images/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="app-assets/images/ico/favicon-16x16.png">
    <link rel="manifest" href="app-assets/images/ico/site.webmanifest">
    <link rel="mask-icon" href="app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<?php $this->load->view("partial/header"); ?>

<?php $this->load->view("partial/sidebar"); ?>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">

        <div class="content-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <!-- <h4 class="card-header">Change Password</h4> -->
                        <div class="card-body">
                            <form id="formChangePassword" onsubmit="return formChangePasswordSubmit()">
                                <div class="alert alert-warning mb-2 d-none" role="alert">
                                    <h6 class="alert-heading">Ensure that these requirements are met</h6>
                                    <div class="alert-body fw-normal">Minimum 8 characters long, uppercase &amp; symbol</div>
                                </div>

                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>" />

                                <div class="row">
                                    <div class="mb-2 col-12 form-password-toggle">
                                        <label class="form-label" for="passwordOld">Old Password</label>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input class="form-control validate-equalTo-blur" type="text" id="passwordOld" name="passwordOld" placeholder="············" aria-describedby="passwordOld-error" aria-invalid="false">
                                            <span class="input-group-text cursor-pointer">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off font-small-4">
                                                    <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                                    <line x1="1" y1="1" x2="23" y2="23"></line>
                                                </svg>
                                            </span>
                                        </div><span id="passwordOld-error" class="error" style="display: none;"></span>
                                    </div>

                                    <div class="mb-2 col-12 form-password-toggle">
                                        <label class="form-label" for="password">New Password</label>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input class="form-control validate-equalTo-blur" type="text" id="password" name="password" placeholder="············" aria-describedby="password-error" aria-invalid="false">
                                            <span class="input-group-text cursor-pointer">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off font-small-4">
                                                    <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                                    <line x1="1" y1="1" x2="23" y2="23"></line>
                                                </svg>
                                            </span>
                                        </div><span id="password-error" class="error" style="display: none;"></span>
                                    </div>

                                    <div class="mb-2 col-12 form-password-toggle">
                                        <label class="form-label" for="passwordConfirm">Confirm New Password</label>
                                        <div class="input-group input-group-merge">
                                            <input class="form-control" type="password" name="passwordConfirm" id="passwordConfirm" placeholder="············" aria-describedby="passwordConfirm-error" aria-invalid="false">
                                            <span class="input-group-text cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye">
                                                    <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                    <circle cx="12" cy="12" r="3"></circle>
                                                </svg></span>
                                        </div><span id="passwordConfirm-error" class="error" style="display: none;"></span>
                                    </div>
                                    <div>

                                        <button type="submit" class="btn btn-primary me-2 waves-effect waves-float waves-light">
                                            <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            <span class="visually-hidden">Loading...</span>
                                            Change Password
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/polyfill.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/core/app.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/init.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->

<script>
    var formChangePassword = $('#formChangePassword');

    // $(document).ready(function() {

    // })

    $(function() {
        'use strict';

        hideLoading()

        if (formChangePassword.length) {
            formChangePassword.validate({
                rules: {
                    passwordOld: {
                        required: true,
                        // minlength: 8
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                    passwordConfirm: {
                        required: true,
                        minlength: 8,
                        equalTo: "#password"
                    }
                },
                // messages: {
                //     username: "Enter your User ID !",
                //     password: "Enter your Password !",
                // }
            });
        }
    })

    function showLoading() {
        $('button[type=submit], input[type=submit]', formChangePassword).prop('disabled', true)
        $('button[type=submit], input[type=submit]', formChangePassword).find('.spinner').show()
    }

    function hideLoading() {
        $('button[type=submit], input[type=submit]', formChangePassword).prop('disabled', false)
        $('button[type=submit], input[type=submit]', formChangePassword).find('.spinner').hide()
    }

    function formChangePasswordSubmit() {
        if (formChangePassword.valid()) {
            $(".pageloader").fadeIn("slow");
            showLoading()

            $.post("<?php echo base_url(); ?>profile/changePasswordSave", formChangePassword.serialize(),
                function(r) {
                    $(".pageloader").fadeOut("slow");

                    Swal.fire({
                        text: r.message,
                        icon: r.error ? 'error' : 'success',
                        customClass: {
                            confirmButton: 'btn btn-primary'
                        },
                        buttonsStyling: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            hideLoading()

                            $("#passwordOld").val("");
                            $("#password").val("");
                            $("#passwordConfirm").val("");

                            hideLoading()

                            setTimeout(() => {
                                $("#passwordOld").focus();
                            }, 500);
                        }
                    })

                    return false

                    // location = r.redirect;
                }, "json");

            return false;
        }

        return false
    }
</script>

</body>
<!-- END: Body-->

</html>
