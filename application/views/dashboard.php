<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="<?php echo $this->config->item("appdesc"); ?>">
    <meta name="keywords" content="surat, arsip">

    <meta name="author" content="<?php echo $this->config->item("appowner"); ?>">
    <title><?php echo $this->config->item("apptitle"); ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="app-assets/images/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="app-assets/images/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="app-assets/images/ico/favicon-16x16.png">
    <link rel="manifest" href="app-assets/images/ico/site.webmanifest">
    <link rel="mask-icon" href="app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/bootstrap-daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/calendars/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/pages/app-calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-toastr.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/daterangepicker-custom.css">
    <!-- END: Custom CSS-->

    <style>
        #map {
            /* position: absolute; */
            top: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
        }
    </style>

</head>
<!-- END: Head-->

<?php $this->load->view("partial/header"); ?>

<?php $this->load->view("partial/sidebar"); ?>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">

        <div class="content-body">

            <div id="print-area">

                <div id="btn-toolbar" class="row mb-50">
                    <div class="col-md-6 mb-1">
                        <div class="d-md-flex">
                            <div class="btn-group me-md-1 me-0 mb-md-0 mb-1 flex-fill" role="group" aria-label="Basic radio toggle button group">
                                <input value="harian" type="radio" class="btn-check" name="period" id="radio_option1" autocomplete="off" />
                                <label class="btn btn-outline-primary" for="radio_option1">Harian</label>

                                <input value="bulanan" type="radio" class="btn-check" name="period" id="radio_option2" autocomplete="off" checked />
                                <label class="btn btn-outline-primary" for="radio_option2">Bulanan</label>

                                <input value="tahunan" type="radio" class="btn-check" name="period" id="radio_option3" autocomplete="off" />
                                <label class="btn btn-outline-primary" for="radio_option3">Tahunan</label>

                                <input value="manual" type="radio" class="btn-check" name="period" id="radio_option4" autocomplete="off" />
                                <label class="btn btn-outline-primary" for="radio_option4" style="width: 110px;">Pilih Tanggal</label>
                            </div>

                            <div class="position-relative me-1 mb-md-0 mb-1 flex-fill w-100">
                                <input type="text" id="date" class="form-control" placeholder="" />
                            </div>

                            <div class="position-relative me-1 mb-md-0 mb-1 flex-fill w-100">
                                <input id="month-picker" type="text" class="form-control shadow-none bg-transparent pe-0" />
                            </div>

                            <div class="position-relative mb-md-0 mb-1 flex-fill w-100">
                                <input id="year-picker" type="text" class="form-control shadow-none bg-transparent pe-0" />
                            </div>

                            <div class="position-relative mb-md-0 mb-1 flex-fill w-100">
                                <button id="advance-daterange" class="btn btn-outline-primary d-flex align-items-center text-start"><span class="text-truncate">&nbsp;</span><i class="fa fa-caret-down ms-1"></i></button>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0 fs-4 text-uppercase">Surat Masuk</h5>
                    </div>
                    <div class="card-body pb-0"">
                        <div class="row match-height">
                            <div class="col-12">
                                <div class="row match-height">

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-secondary text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Baru</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratMasukBaruCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-warning text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Sedang Diverifikasi</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratMasukSedangVerifikasiCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-success text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Terverifikasi</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratMasukTerverifikasiCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-danger text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Ditolak</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratMasukDitolakCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0 fs-4 text-uppercase">Surat Keluar</h5>
                    </div>
                    <div class="card-body pb-0"">
                        <div class="row match-height">
                            <div class="col-12">
                                <div class="row match-height">

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-secondary text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Baru</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratKeluarBaruCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-warning text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Sedang Diverifikasi</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratKeluarSedangVerifikasiCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-success text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Terverifikasi</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratKeluarTerverifikasiCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg col-md-6 col-6 counter-summary">
                                        <div class="card card-block bg-danger text-white">
                                            <div class="card-body pb-50" style="position: relative;">
                                                <h3 class="mb-0 text-white">Ditolak</h3>
                                                <h2 class="fw-bolder display-5 text-white" id="suratKeluarDitolakCount"></h2>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="app-calendar overflow-hidden border">
                    <div class="row g-0">
                        <!-- Sidebar -->
                        <div class="col app-calendar-sidebar flex-grow-0 overflow-hidden d-flex flex-column" id="app-calendar-sidebar">
                            <div class="sidebar-wrapper">
                                <div class="card-body d-flex justify-content-center">
                                    <button class="btn btn-primary btn-toggle-sidebar w-100" data-bs-toggle="modal" data-bs-target="#add-new-sidebar">
                                        <span class="align-middle">Add Event</span>
                                    </button>
                                </div>
                                <div class="card-body pb-0">
                                    <h5 class="section-label mb-1">
                                        <span class="align-middle">Filter</span>
                                    </h5>
                                    <div class="form-check mb-1">
                                        <input type="checkbox" class="form-check-input select-all" id="select-all" checked />
                                        <label class="form-check-label" for="select-all">View All</label>
                                    </div>
                                    <div class="calendar-events-filter">
                                        <div class="form-check form-check-success mb-1">
                                            <input type="checkbox" class="form-check-input input-filter" id="public" data-value="public" checked />
                                            <label class="form-check-label" for="public">Public</label>
                                        </div>
                                        <div class="form-check form-check-primary mb-1">
                                            <input type="checkbox" class="form-check-input input-filter" id="personal" data-value="personal" checked />
                                            <label class="form-check-label" for="personal">Personal</label>
                                        </div>
<!--                                        <div class="form-check form-check-warning mb-1">-->
<!--                                            <input type="checkbox" class="form-check-input input-filter" id="family" data-value="family" checked />-->
<!--                                            <label class="form-check-label" for="family">Family</label>-->
<!--                                        </div>-->
<!--                                        <div class="form-check form-check-success mb-1">-->
<!--                                            <input type="checkbox" class="form-check-input input-filter" id="holiday" data-value="holiday" checked />-->
<!--                                            <label class="form-check-label" for="holiday">Holiday</label>-->
<!--                                        </div>-->
<!--                                        <div class="form-check form-check-info">-->
<!--                                            <input type="checkbox" class="form-check-input input-filter" id="lainnya" data-value="lainnya" checked />-->
<!--                                            <label class="form-check-label" for="lainnya">Lainnya</label>-->
<!--                                        </div>-->
                                    </div>
                                </div>
                            </div>
<!--                            <div class="mt-auto">-->
<!--                                <img src="--><?php //echo base_url(); ?><!--app-assets/images/pages/calendar-illustration.png" alt="Calendar illustration" class="img-fluid" />-->
<!--                            </div>-->
                        </div>
                        <!-- /Sidebar -->

                        <!-- Calendar -->
                        <div class="col position-relative">
                            <div class="card shadow-none border-0 mb-0 rounded-0">
                                <div class="card-body pb-0">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /Calendar -->
                        <div class="body-content-overlay"></div>
                    </div>
                </div>

                <div class="modal modal-slide-in event-sidebar fade" id="add-new-sidebar">
                    <div class="modal-dialog sidebar-lg">
                        <div class="modal-content p-0">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                            <div class="modal-header mb-1">
                                <h5 class="modal-title">Add Event</h5>
                            </div>
                            <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                                <form class="event-form needs-validation" data-ajax="false" novalidate>
                                    <div class="mb-1">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Event Title" required />
                                    </div>
                                    <div class="mb-1">
                                        <label for="select-label" class="form-label">Label</label>
                                        <select class="select2 select-label form-select w-100" id="select-label" name="select-label">
                                            <option data-label="success" value="Public" selected>Public</option>
                                            <option data-label="primary" value="Personal">Personal</option>
<!--                                            <option data-label="warning" value="Family">Family</option>-->
<!--                                            <option data-label="success" value="Holiday">Holiday</option>-->
<!--                                            <option data-label="info" value="ETC">ETC</option>-->
                                        </select>
                                    </div>
                                    <div class="mb-1 position-relative">
                                        <label for="start-date" class="form-label">Start Date</label>
                                        <input type="text" class="form-control" id="start-date" name="start-date" placeholder="Start Date" />
                                    </div>
                                    <div class="mb-1 position-relative">
                                        <label for="end-date" class="form-label">End Date</label>
                                        <input type="text" class="form-control" id="end-date" name="end-date" placeholder="End Date" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="form-check form-switch">
                                            <input type="checkbox" class="form-check-input allDay-switch" id="customSwitch3" />
                                            <label class="form-check-label" for="customSwitch3">All Day</label>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-none">
                                        <label for="event-url" class="form-label">Event URL</label>
                                        <input type="url" class="form-control" id="event-url" placeholder="https://www.google.com" />
                                    </div>
                                    <div class="mb-1 select2-primary d-none">
                                        <label for="event-guests" class="form-label">Add Guests</label>
                                        <select class="select2 select-add-guests form-select w-100" id="event-guests" multiple>
                                            <option data-avatar="1-small.png" value="Jane Foster">Jane Foster</option>
                                            <option data-avatar="3-small.png" value="Donna Frank">Donna Frank</option>
                                            <option data-avatar="5-small.png" value="Gabrielle Robertson">Gabrielle Robertson</option>
                                            <option data-avatar="7-small.png" value="Lori Spears">Lori Spears</option>
                                            <option data-avatar="9-small.png" value="Sandy Vega">Sandy Vega</option>
                                            <option data-avatar="11-small.png" value="Cheryl May">Cheryl May</option>
                                        </select>
                                    </div>
                                    <div class="mb-1">
                                        <label for="event-location" class="form-label">Location</label>
                                        <input type="text" class="form-control" id="event-location" placeholder="Enter Location" />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label">Description</label>
                                        <textarea name="event-description-editor" id="event-description-editor" class="form-control"></textarea>
                                    </div>
                                    <div class="mb-1 d-flex">
                                        <button type="submit" class="btn btn-primary add-event-btn me-1">Add</button>
                                        <button type="button" class="btn btn-outline-secondary btn-cancel" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary update-event-btn d-none me-1">Update</button>
                                        <button class="btn btn-outline-danger btn-delete-event d-none">Delete</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-2">
                    &nbsp;
                </div>

            </div>

        </div>
    </div>
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/moment/moment-with-locales.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/polyfill.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/calendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/core/app.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/init.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.4.0/socket.io.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/modernizr-custom.js"></script>
<script src='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js'></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/charts/chart.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
 <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/flatpickr/id.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/scripts/extensions/ext-component-toastr.min.js"></script>
<!-- END: Page JS-->

<script>
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        currency: {
            symbol: 'Rp'
        }
    });
    numeral.locale('id')

    var periodEl = $('input[name="period"]'),
        dateEl = $('#date'),
        monthEl = $('#month-picker'),
        yearEl = $('#year-picker'),

        rangePickr = $('#advance-daterange'),
        yearEl2 = $('#year-picker-2'),
        period = 'bulanan',
        monthSelected = moment().format('MM'),
        yearSelected = moment().format('YYYY'),
        date = moment(),

        yearSelected2 = new Date().getFullYear(),
        sDate = moment(),
        eDate = moment(),
        cardSection = $('.card-block'),
        suratMasukBaruCount = 0,
        suratMasukSedangVerifikasiCount = 0,
        suratMasukTerverifikasiCount = 0,
        suratMasukDitolakCount = 0,
        suratKeluarBaruCount = 0,
        suratKeluarSedangVerifikasiCount = 0,
        suratKeluarTerverifikasiCount = 0,
        suratKeluarDitolakCount = 0,
        calendarsColorList = <?php echo json_encode($calendarsColorList) ?>

    $(document).ready(function() {

        $(periodEl).click(function() {
            loadDashboardData()
        })

        setValue()

        dateEl.flatpickr({
            altInput: true,
            altFormat: 'j F Y',
            dateFormat: 'Y-m-d',
            locale: 'id',
            defaultDate: date.format('YYYY-MM-DD'),
            onChange(selectedDates, dateStr) {
                sDate = moment(selectedDates[0])
                loadDashboardData()
                // loadDataChart()
            }
        })

        monthEl.val(monthSelected)
               .select2({
                   data: generateArrayOfMonths(),
                   allowClear: false,
                   dropdownAutoWidth: true,
                   dropdownParent: monthEl.parent(),
                   minimumResultsForSearch: -1,
                   width: '100%',
               }).on("change.select2", function(e) {
            loadDashboardData()
            // loadDataChart()
        });

        yearEl.val(yearSelected)
              .select2({
                  data: generateArrayOfYears(),
                  allowClear: false,
                  dropdownAutoWidth: true,
                  dropdownParent: yearEl.parent(),
                  minimumResultsForSearch: -1,
                  width: '100%',
              }).on("change.select2", function(e) {
            loadDashboardData()
            // loadDataChart()
        });

        yearEl2.val(yearSelected2)
               .wrap('<div class="position-relative" style="width:80px"></div>')
               .select2({
                   data: generateArrayOfYears(),
                   allowClear: false,
                   dropdownAutoWidth: true,
                   dropdownParent: yearEl2.parent(),
                   minimumResultsForSearch: -1,
                   width: '100%',
               }).on("change.select2", function(e) {
            loadDataChart()
        });

        $('#advance-daterange span').html(sDate.format('D MMM YYYY') + ' - ' + eDate.format('D MMM YYYY'));

        rangePickr.daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: sDate,
            endDate: eDate,
            // minDate: '01/01/2012',
            // maxDate: '12/31/2015',
            // dateLimit: {
            //     days: 60
            // },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            // ranges: {
            //     'Hari ini': [moment(), moment()],
            //     'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            //     // 'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            //     // 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            //     'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
            //     'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            // },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            // locale: {
            //     applyLabel: 'Pilih',
            //     cancelLabel: 'Batal',
            //     fromLabel: 'Dari',
            //     toLabel: 'Sampai',
            //     customRangeLabel: 'Pilih tanggal',
            //     daysOfWeek: ['Ming', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
            //     monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            //     firstDay: 1
            // }
        }, function(start, end, label) {
            $('#advance-daterange span').html(start.format('D MMM YYYY') + ' - ' + end.format('D MMM YYYY'));
        }).on('apply.daterangepicker', function(ev, picker) {
            sDate = picker.startDate
            eDate = picker.endDate
            loadDashboardData()
        });

        setTimeout(function() {
            loadDashboardData()
            renderCalendar()
        }, 1000)
    })

    $(".pageloader").fadeOut("slow")

    function showLoading() {
        cardSection.block({
            message: '<div class="spinner-border text-white" role="status"></div>',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            },
            overlayCSS: {
                opacity: 0.5
            }
        })
    }

    function hideLoading() {
        cardSection.unblock()
    }

    function setValue() {
        $('#suratMasukDitolakCount').html(numeral(suratMasukDitolakCount).format('0,0'))
        $('#suratMasukBaruCount').html(numeral(suratMasukBaruCount).format('0,0'))
        $('#suratMasukSedangVerifikasiCount').html(numeral(suratMasukSedangVerifikasiCount).format('0,0'))
        $('#suratMasukTerverifikasiCount').html(numeral(suratMasukTerverifikasiCount).format('0,0'))

        $('#suratKeluarDitolakCount').html(numeral(suratKeluarDitolakCount).format('0,0'))
        $('#suratKeluarBaruCount').html(numeral(suratKeluarBaruCount).format('0,0'))
        $('#suratKeluarSedangVerifikasiCount').html(numeral(suratKeluarSedangVerifikasiCount).format('0,0'))
        $('#suratKeluarTerverifikasiCount').html(numeral(suratKeluarTerverifikasiCount).format('0,0'))
    }

    function loadDashboardData() {
        showLoading()

        period = periodEl.filter(":checked").val()

        if (period == 'harian') {
            dateEl.parent().show()
            monthEl.parent().hide()
            yearEl.parent().hide()
            rangePickr.parent().hide()
        } else if (period == 'bulanan') {
            dateEl.parent().hide()
            monthEl.parent().show()
            yearEl.parent().show()
            rangePickr.parent().hide()
        } else if (period == 'tahunan') {
            dateEl.parent().hide()
            monthEl.parent().hide()
            yearEl.parent().show()
            rangePickr.parent().hide()
        } else if (period == 'manual') {
            dateEl.parent().hide()
            monthEl.parent().hide()
            yearEl.parent().hide()
            rangePickr.parent().show()
        }

        monthSelected = monthEl.select2('val')
        yearSelected = yearEl.select2('val')

        $.post("<?php echo base_url() ?>dash/get_dashboard_data", {
                period: period,
                month: monthSelected,
                year: yearSelected,
                sDate: moment(sDate).format('YYYY-MM-DD'),
                eDate: moment(eDate).format('YYYY-MM-DD'),
            },
            function(r) {
                if (!r.error) {
                    suratMasukBaruCount = parseInt(r.data.suratMasukBaruCount)
                    suratMasukSedangVerifikasiCount = parseInt(r.data.suratMasukSedangVerifikasiCount)
                    suratMasukTerverifikasiCount = parseInt(r.data.suratMasukTerverifikasiCount)
                    suratMasukDitolakCount = parseInt(r.data.suratMasukDitolakCount)

                    suratKeluarBaruCount = parseInt(r.data.suratKeluarBaruCount)
                    suratKeluarSedangVerifikasiCount = parseInt(r.data.suratKeluarSedangVerifikasiCount)
                    suratKeluarTerverifikasiCount = parseInt(r.data.suratKeluarTerverifikasiCount)
                    suratKeluarDitolakCount = parseInt(r.data.suratKeluarDitolakCount)
                }

                setValue()
                hideLoading()
            }, "json");
    }

    function btnRefreshClicked() {}

    function generateArrayOfYears() {
        var max = new Date().getFullYear()
        var min = max - 4
        var years = []

        for (var i = min; i <= max; i++) {
            years.push({
                id: i,
                text: i,
            })
        }
        return years
    }

    function generateArrayOfMonths() {
        var min = moment().startOf('year').format('MM')
        var max = moment().endOf('year').format('MM')
        var months = []

        for (var i = parseInt(min); i <= parseInt(max); i++) {
            var month = moment(`1900-${i}-1`, 'YYYY-M-D')
            months.push({
                id: month.format('MM'),
                text: month.format('MMMM'),
            })
        }

        return months
    }


     //blonge
     var idleTime = 0;
    var idleInterval = setInterval(timerIncrement, 60000);
    jQuery(this).mousemove(function (e) { idleTime = 0; });
    jQuery(this).keypress(function (e) { idleTime = 0; });
    function timerIncrement()
    {
		idleTime = idleTime + 1;
		if (idleTime > 5) {  //5mnt
            location = '<?php echo base_url(); ?>logout';
		}
    }


</script>
<script src="<?php echo base_url(); ?>app-assets/js/dashboardCalendarInit.js"></script>

</body>
<!-- END: Body-->

</html>
