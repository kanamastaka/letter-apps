<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title> <?php echo $this->config->item("apptitle"); ?> </title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link href="
			<?php echo base_url(); ?>assets/css/vendor.min.css" rel="stylesheet" />
  <link href="
				<?php echo base_url(); ?>assets/css/app.min.css" rel="stylesheet" />
  <link href="
					<?php echo base_url(); ?>assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />


</head>

<body>
  <?php $this->load->view("partial/app"); ?>

  <?php $this->load->view("partial/sidebar"); ?>

  <div id="content" class="app-content">
    <div class="row">
      <div class="col-xl-3 col-lg-6">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">DATA COLLECTIONS</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div class="row align-items-center mb-2">
              <div class="col-7">
                <h3 class="mb-0">4.2m</h3>
              </div>
              <div class="col-5">
                <div class="mt-n2" data-render="apexchart" data-type="bar" data-title="Visitors" data-height="30"></div>
              </div>
            </div>
            <div class="small text-white text-opacity-50 text-truncate">
              <i class="fa fa-chevron-up fa-fw me-1"></i> 33.3% more than last week <br />
              <!--<i class="far fa-user fa-fw me-1"></i> 45.5% new visitors <br />-->
              <i class="far fa-times-circle fa-fw me-1"></i> 3.25% bounce rate <br />
              &nbsp;
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">SPIDER</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div class="row align-items-center mb-2">
              <div class="col-7">
                <h3 class="mb-0">35.2K</h3>
              </div>
              <div class="col-5">
                <div class="mt-n2" data-render="apexchart" data-type="line" data-title="Visitors" data-height="30"></div>
              </div>
            </div>
            <div class="small text-white text-opacity-50 text-truncate">
              <i class="fa fa-chevron-up fa-fw me-1"></i> 20.4% more than last week <br />
              <i class="fa fa-shopping-bag fa-fw me-1"></i> 33.5% new data <br />
              <i class="fa fa-chevron-up fa-fw me-1"></i> 2.21% errors
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">VULNERABILITY</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div class="row align-items-center mb-2">
              <div class="col-7">
                <h3 class="mb-0">4,490</h3>
              </div>
              <div class="col-5">
                <div class="mt-n3 mb-n2" data-render="apexchart" data-type="pie" data-title="Visitors" data-height="45"></div>
              </div>
            </div>
            <div class="small text-white text-opacity-50 text-truncate">
              <i class="fa fa-chevron-up fa-fw me-1"></i> 59.5% more than last week <br />
              <i class="bi bi-bug"></i> 45.5% from public <br />
              <i class="bi bi-bug"></i> 15.25% from tor
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">BANDWIDTH</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div class="row align-items-center mb-2">
              <div class="col-7">
                <h3 class="mb-0">4.5TB</h3>
              </div>
              <div class="col-5">
                <div class="mt-n3 mb-n2" data-render="apexchart" data-type="donut" data-title="Visitors" data-height="45"></div>
              </div>
            </div>
            <div class="small text-white text-opacity-50 text-truncate">
              <i class="fa fa-chevron-up fa-fw me-1"></i> 5.3% more than last week <br />
              <i class="far fa-hdd fa-fw me-1"></i> 10.5% from total usage <br />
              <i class="far fa-hand-point-up fa-fw me-1"></i> 2MB per visit
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>



      <div class="col-xl-12">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">CYBERTHREATH</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div style="height: 635px; display:block;overflow: hidden;">
              <iframe id="cybermap" src="https://cybermap.kaspersky.com/" width="100%" height="700" title="W3Schools Free Online Web Tutorials"></iframe>
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>




      <div class="col-xl-12">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">SERVER STATS</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div class="ratio ratio-21x9 mb-3">
              <div id="chart-server"></div>
            </div>
            <div class="row">
              <div class="col-lg-6 mb-3 mb-lg-0">
                <div class="d-flex align-items-center">
                  <div class="w-50px h-50px">
                    <div data-render="apexchart" data-type="donut" data-title="Visitors" data-height="50"></div>
                  </div>
                  <div class="ps-3 flex-1">
                    <div class="fs-10px fw-bold text-white text-opacity-50 mb-1">DISK USAGE</div>
                    <div class="mb-2 fs-5 text-truncate">20.04 / 256 GB</div>
                    <div class="progress h-3px bg-white-transparent-2 mb-1">
                      <div class="progress-bar bg-theme" style="width: 20%"></div>
                    </div>
                    <div class="fs-11px text-white text-opacity-50 mb-2 text-truncate"> Last updated 1 min ago </div>
                    <div class="d-flex align-items-center small">
                      <i class="bi bi-circle-fill fs-6px me-2 text-theme"></i>
                      <div class="flex-1">DISK C</div>
                      <div>19.56GB</div>
                    </div>
                    <div class="d-flex align-items-center small">
                      <i class="bi bi-circle-fill fs-6px me-2 text-theme text-opacity-50"></i>
                      <div class="flex-1">DISK D</div>
                      <div>0.50GB</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="d-flex">
                  <div class="w-50px pt-3">
                    <div data-render="apexchart" data-type="donut" data-title="Visitors" data-height="50"></div>
                  </div>
                  <div class="ps-3 flex-1">
                    <div class="fs-10px fw-bold text-white text-opacity-50 mb-1">BANDWIDTH</div>
                    <div class="mb-2 fs-5 text-truncate">83.76GB / 10TB</div>
                    <div class="progress h-3px bg-white-transparent-2 mb-1">
                      <div class="progress-bar bg-theme" style="width: 10%"></div>
                    </div>
                    <div class="fs-11px text-white text-opacity-50 mb-2 text-truncate"> Last updated 1 min ago </div>
                    <div class="d-flex align-items-center small">
                      <i class="bi bi-circle-fill fs-6px me-2 text-theme"></i>
                      <div class="flex-1">HTTP</div>
                      <div>35.47GB</div>
                    </div>
                    <div class="d-flex align-items-center small">
                      <i class="bi bi-circle-fill fs-6px me-2 text-theme text-opacity-50"></i>
                      <div class="flex-1">FTP</div>
                      <div>1.25GB</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>










      <!--
        <div class="col-xl-6">
          <div class="card mb-3">
            <div class="card-body">
              <div class="d-flex fw-bold small mb-3">
                <span class="flex-grow-1">TOP PRODUCTS</span>
                <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                  <i class="bi bi-fullscreen"></i>
                </a>
              </div>
              <div class="table-responsive">
                <table class="w-100 mb-0 small align-middle text-nowrap">
                  <tbody>
                    <tr>
                      <td>
                        <div class="d-flex">
                          <div class="position-relative mb-2">
                            <div class="bg-center bg-cover bg-no-repeat w-80px h-60px" style="background-image: url(
																		<?php echo base_url(); ?>assets/img/dashboard/product-1.jpg);">
                            </div>
                            <div class="position-absolute top-0 start-0">
                              <span class="badge bg-theme text-theme-900 rounded-0 d-flex align-items-center justify-content-center w-20px h-20px">1</span>
                            </div>
                          </div>
                          <div class="flex-1 ps-3">
                            <div class="mb-1">
                              <small class="fs-9px fw-500 lh-1 d-inline-block rounded-0 badge bg-white bg-opacity-25 text-white text-opacity-75 pt-5px">SKU90400</small>
                            </div>
                            <div class="fw-500 text-white">Huawei Smart Watch</div> $399.00
                          </div>
                        </div>
                      </td>
                      <td>
                        <table class="mb-2">
                          <tr>
                            <td class="pe-3">QTY:</td>
                            <td class="text-white text-opacity-75 fw-500">129</td>
                          </tr>
                          <tr>
                            <td class="pe-3">REVENUE:</td>
                            <td class="text-white text-opacity-75 fw-500">$51,471</td>
                          </tr>
                          <tr>
                            <td class="pe-3 text-nowrap">PROFIT:</td>
                            <td class="text-white text-opacity-75 fw-500">$15,441</td>
                          </tr>
                        </table>
                      </td>
                      <td>
                        <a href="#" class="text-decoration-none text-white">
                          <i class="bi bi-search"></i>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="d-flex mb-2 align-items-center">
                          <div class="position-relative">
                            <div class="bg-center bg-cover bg-no-repeat w-80px h-60px" style="background-image: url(
																		<?php echo base_url(); ?>assets/img/dashboard/product-2.jpg);">
                            </div>
                            <div class="position-absolute top-0 start-0">
                              <span class="badge bg-theme text-theme-900 rounded-0 d-flex align-items-center justify-content-center w-20px h-20px">2</span>
                            </div>
                          </div>
                          <div class="flex-1 ps-3">
                            <div class="mb-1">
                              <small class="fs-9px fw-500 lh-1 d-inline-block rounded-0 badge bg-white bg-opacity-25 text-white text-opacity-75 pt-5px">SKU85999</small>
                            </div>
                            <div class="text-white fw-500">Nike Shoes Black Version</div> $99.00
                          </div>
                        </div>
                      </td>
                      <td>
                        <table class="mb-2">
                          <tr>
                            <td class="pe-3">QTY:</td>
                            <td class="text-white text-opacity-75 fw-500">108</td>
                          </tr>
                          <tr>
                            <td class="pe-3">REVENUE:</td>
                            <td class="text-white text-opacity-75 fw-500">$10,692</td>
                          </tr>
                          <tr>
                            <td class="pe-3 text-nowrap">PROFIT:</td>
                            <td class="text-white text-opacity-75 fw-500">$5,346</td>
                          </tr>
                        </table>
                      </td>
                      <td>
                        <a href="#" class="text-decoration-none text-white">
                          <i class="bi bi-search"></i>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="d-flex mb-2 align-items-center">
                          <div class="position-relative">
                            <div class="bg-center bg-cover bg-no-repeat w-80px h-60px" style="background-image: url(
																		<?php echo base_url(); ?>assets/img/dashboard/product-3.jpg);">
                            </div>
                            <div class="position-absolute top-0 start-0">
                              <span class="badge bg-theme text-theme-900 rounded-0 d-flex align-items-center justify-content-center w-20px h-20px">3</span>
                            </div>
                          </div>
                          <div class="flex-1 ps-3">
                            <div class="mb-1">
                              <small class="fs-9px fw-500 lh-1 d-inline-block rounded-0 badge bg-white bg-opacity-25 text-white text-opacity-75 pt-5px">SKU20400</small>
                            </div>
                            <div class="text-white fw-500">White Sony PS4</div> $599
                          </div>
                        </div>
                      </td>
                      <td>
                        <table class="mb-2">
                          <tr>
                            <td class="pe-3">QTY:</td>
                            <td class="text-white text-opacity-75 fw-500">72</td>
                          </tr>
                          <tr>
                            <td class="pe-3">REVENUE:</td>
                            <td class="text-white text-opacity-75 fw-500">$43,128</td>
                          </tr>
                          <tr>
                            <td class="pe-3 text-nowrap">PROFIT:</td>
                            <td class="text-white text-opacity-75 fw-500">$4,312</td>
                          </tr>
                        </table>
                      </td>
                      <td>
                        <a href="#" class="text-decoration-none text-white">
                          <i class="bi bi-search"></i>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="d-flex mb-2 align-items-center">
                          <div class="position-relative">
                            <div class="bg-center bg-cover bg-no-repeat w-80px h-60px" style="background-image: url(
																		<?php echo base_url(); ?>assets/img/dashboard/product-4.jpg);">
                            </div>
                            <div class="position-absolute top-0 start-0">
                              <span class="badge bg-black bg-opacity-50 rounded-0 d-flex align-items-center justify-content-center w-20px h-20px">4</span>
                            </div>
                          </div>
                          <div class="flex-1 ps-3">
                            <div class="mb-1">
                              <small class="fs-9px fw-500 lh-1 d-inline-block rounded-0 badge bg-white bg-opacity-25 text-white text-opacity-75 pt-5px">SKU19299</small>
                            </div>
                            <div class="text-white fw-500">Apple Watch Series 5</div> $1,099
                          </div>
                        </div>
                      </td>
                      <td>
                        <table class="mb-2">
                          <tr>
                            <td class="pe-3">QTY:</td>
                            <td class="text-white text-opacity-75 fw-500">53</td>
                          </tr>
                          <tr>
                            <td class="pe-3">REVENUE:</td>
                            <td class="text-white text-opacity-75 fw-500">$58,247</td>
                          </tr>
                          <tr>
                            <td class="pe-3 text-nowrap">PROFIT:</td>
                            <td class="text-white text-opacity-75 fw-500">$2,912</td>
                          </tr>
                        </table>
                      </td>
                      <td>
                        <a href="#" class="text-decoration-none text-white">
                          <i class="bi bi-search"></i>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="d-flex align-items-center">
                          <div class="position-relative">
                            <div class="bg-center bg-cover bg-no-repeat w-80px h-60px" style="background-image: url(
																		<?php echo base_url(); ?>assets/img/dashboard/product-5.jpg);">
                            </div>
                            <div class="position-absolute top-0 start-0">
                              <span class="badge bg-black bg-opacity-50 rounded-0 d-flex align-items-center justify-content-center w-20px h-20px">5</span>
                            </div>
                          </div>
                          <div class="flex-1 ps-3">
                            <div class="mb-1">
                              <small class="fs-9px fw-500 lh-1 d-inline-block rounded-0 badge bg-white bg-opacity-25 text-white text-opacity-75 pt-5px">SKU19299</small>
                            </div>
                            <div class="text-white fw-500">Black Nikon DSLR</div> 1,899
                          </div>
                        </div>
                      </td>
                      <td>
                        <table>
                          <tr>
                            <td class="pe-3">QTY:</td>
                            <td class="text-white text-opacity-75 fw-500">50</td>
                          </tr>
                          <tr>
                            <td class="pe-3">REVENUE:</td>
                            <td class="text-white text-opacity-75 fw-500">$90,950</td>
                          </tr>
                          <tr>
                            <td class="pe-3 text-nowrap">PROFIT:</td>
                            <td class="text-white text-opacity-75 fw-500">$2,848</td>
                          </tr>
                        </table>
                      </td>
                      <td>
                        <a href="#" class="text-decoration-none text-white">
                          <i class="bi bi-search"></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-arrow">
              <div class="card-arrow-top-left"></div>
              <div class="card-arrow-top-right"></div>
              <div class="card-arrow-bottom-left"></div>
              <div class="card-arrow-bottom-right"></div>
            </div>
          </div>
        </div>
                            -->

      <div class="col-xl-12">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex fw-bold small mb-3">
              <span class="flex-grow-1">ACTIVITY LOG</span>
              <a href="#" data-toggle="card-expand" class="text-white text-opacity-50 text-decoration-none">
                <i class="bi bi-fullscreen"></i>
              </a>
            </div>
            <div class="table-responsive">
              <table class="table table-striped table-borderless mb-2px small text-nowrap">
                <tbody>

                  <tr>
                    <td>
                      <span class="d-flex align-items-center">
                        <i class="bi bi-circle-fill fs-6px text-white-transparent-3 me-2"></i> Firewall upgrade </span>
                    </td>
                    <td>
                      <small>1 min ago</small>
                    </td>
                    <td>
                      <span class="badge d-block bg-white bg-opacity-25 rounded-0 pt-5px w-70px" style="min-height: 18px">SERVER</span>
                    </td>
                    <td>
                      <a href="#" class="text-decoration-none text-white">
                        <i class="bi bi-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span class="d-flex align-items-center">
                        <i class="bi bi-circle-fill fs-6px text-white-transparent-3 me-2"></i> Push notification v2.0 installation </span>
                    </td>
                    <td>
                      <small>1 mins ago</small>
                    </td>
                    <td>
                      <span class="badge d-block bg-white bg-opacity-25 rounded-0 pt-5px w-70px" style="min-height: 18px">ANDROID</span>
                    </td>
                    <td>
                      <a href="#" class="text-decoration-none text-white">
                        <i class="bi bi-search"></i>
                      </a>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <span class="d-flex align-items-center">
                        <i class="bi bi-circle-fill fs-6px text-white text-opacity-25 me-2"></i> 2 Unread enquiry </span>
                    </td>
                    <td>
                      <small>2 mins ago</small>
                    </td>
                    <td>
                      <span class="badge d-block bg-white bg-opacity-25 rounded-0 pt-5px w-70px" style="min-height: 18px">ENQUIRY</span>
                    </td>
                    <td>
                      <a href="#" class="text-decoration-none text-white">
                        <i class="bi bi-search"></i>
                      </a>
                    </td>
                  </tr>


                  <tr>
                    <td>
                      <span class="d-flex align-items-center">
                        <i class="bi bi-circle-fill fs-6px text-white text-opacity-25 me-2"></i> 1 pull request from github </span>
                    </td>
                    <td>
                      <small>5 mins ago</small>
                    </td>
                    <td>
                      <span class="badge d-block bg-white bg-opacity-25 rounded-0 pt-5px w-70px" style="min-height: 18px">GITHUB</span>
                    </td>
                    <td>
                      <a href="#" class="text-decoration-none text-white">
                        <i class="bi bi-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span class="d-flex align-items-center">
                        <i class="bi bi-circle-fill fs-6px text-white-transparent-3 me-2"></i> 3 pending module to generate </span>
                    </td>
                    <td>
                      <small>5 mins ago</small>
                    </td>
                    <td>
                      <span class="badge d-block bg-white bg-opacity-25 rounded-0 pt-5px w-70px" style="min-height: 18px">MODULE</span>
                    </td>
                    <td>
                      <a href="#" class="text-decoration-none text-white">
                        <i class="bi bi-search"></i>
                      </a>
                    </td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
          <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a href="#" data-toggle="scroll-to-top" class="btn-scroll-top fade">
    <i class="fa fa-arrow-up"></i>
  </a>
  <div class="app-theme-panel">
    <div class="app-theme-panel-container">
      <a href="javascript:;" data-toggle="theme-panel-expand" class="app-theme-toggle-btn">
        <i class="bi bi-sliders"></i>
      </a>
      <div class="app-theme-panel-content">
        <div class="app-theme-list">
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-pink" data-theme-class="theme-pink" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Pink">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-red" data-theme-class="theme-red" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Red">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-warning" data-theme-class="theme-warning" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Orange">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-yellow" data-theme-class="theme-yellow" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Yellow">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-lime" data-theme-class="theme-lime" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Lime">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-green" data-theme-class="theme-green" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Green">&nbsp;</a>
          </div>
          <div class="app-theme-list-item active">
            <a href="javascript:;" class="app-theme-list-link bg-teal" data-theme-class="" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Default">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-info" data-theme-class="theme-info" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Cyan">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-primary" data-theme-class="theme-primary" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Blue">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-purple" data-theme-class="theme-purple" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Purple">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-indigo" data-theme-class="theme-indigo" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Indigo">&nbsp;</a>
          </div>
          <div class="app-theme-list-item">
            <a href="javascript:;" class="app-theme-list-link bg-gray-100" data-theme-class="theme-gray-200" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Gray">&nbsp;</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <script data-cfasync="false" src="https://seantheme.com/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
  <script src="
					<?php echo base_url(); ?>assets/plugins/jvectormap-next/jquery-jvectormap.min.js" type="9211c9b28fccf100c5a12ab4-text/javascript">
  </script>
  <script src="
					<?php echo base_url(); ?>assets/plugins/jvectormap-next/jquery-jvectormap-world-mill.js" type="9211c9b28fccf100c5a12ab4-text/javascript">
  </script>
  <script src="
					<?php echo base_url(); ?>assets/plugins/apexcharts/dist/apexcharts.min.js" type="9211c9b28fccf100c5a12ab4-text/javascript">
  </script>
  <script src="
					<?php echo base_url(); ?>assets/js/demo/dashboard.demo.js" type="9211c9b28fccf100c5a12ab4-text/javascript">
  </script>

  <script src="https://seantheme.com/cdn-cgi/scripts/7d0fa10a/cloudflare-static/rocket-loader.min.js" data-cf-settings="9211c9b28fccf100c5a12ab4-|49" defer=""></script>
  <script defer src="https://static.cloudflareinsights.com/beacon.min.js" data-cf-beacon='{"rayId":"6a8252e8a9026bcf","version":"2021.10.0","r":1,"token":"4db8c6ef997743fda032d4f73cfeff63","si":100}'></script>

</body>

</html>