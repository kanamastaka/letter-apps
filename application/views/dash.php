<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> <?php echo $this->config->item("apptitle"); ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<?php echo base_url(); ?>assets/css/vendor.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/app.min.css" rel="stylesheet" />
</head>

<body>
    <?php $this->load->view("app"); ?>

    <?php $this->load->view("sidebar"); ?>

    <div id="content" class="app-content p-0">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
    </div>

    <a href="#" data-toggle="scroll-to-top" class="btn-scroll-top fade">
        <i class="fa fa-arrow-up"></i>
    </a>

    <script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

    <script>
    </script>

</body>

</html>
