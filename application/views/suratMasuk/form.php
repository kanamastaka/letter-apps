<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

<form id="formSuratMasuk" class="row gy-1 gx-2 mt-75" onsubmit="return formSuratMasukSubmit()">
</form>

<template id="formSuratMasukTpl">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>"/>
    <input type="hidden" name="user_id" value="<?php echo logged('user_id'); ?>"/>
    <input type="hidden" name="id" id="id"/>

    <div class="row gy-0 gx-0 mt-75">

        <div class="col-sm-12 col-lg-12">
            <div class="row gy-1 gx-2">
                <div class="col-sm-6 col-12">
                    <label class="form-label" for="tgl_surat">Tanggal Surat</label>
                    <input type="text" id="tgl_surat" name="tgl_surat" class="form-control"/>
                </div>

                <div class="col-sm-6 col-12">
                    <label class="form-label" for="tgl_terima">Tanggal Terima</label>
                    <input type="text" id="tgl_terima" name="tgl_terima" class="form-control"/>
                </div>

                <div class="col-sm-6 col-12">
                    <label class="form-label" for="no_surat">Nomor Surat</label>
                    <input type="text" id="no_surat" name="no_surat" class="form-control"/>
                </div>

                <div class="col-sm-6 col-12">
                    <label class="form-label" for="pengirim">Pengirim</label>
                    <input type="text" id="pengirim" name="pengirim" class="form-control"/>
                </div>

                <div class="col-sm-6 col-12">
                    <label class="form-label" for="perihal">Perihal</label>
                    <input type="text" id="perihal" name="perihal" class="form-control"/>
                </div>

                <div class="col-sm-6 col-12">
                    <label class="form-label" for="kerahasiaan">Kerahasiaan Surat</label>
                    <select class="select2 select2-data-ajax form-select" id="kerahasiaan" name="kerahasiaan"></select>
                </div>

                <div class="col-sm-6 col-12">
                    <label class="form-label" for="tujuan">Tujuan</label>
                    <div class="demo-inline-spacing mb-2 position-relative">
                        <div class="form-check form-check-inline mt-1" style="min-width: 65px;">
                            <input class="form-check-input" type="checkbox" name="tujuan" id="tujuan1" value="1">
                            <label class="form-check-label" for="tujuan1">KASAL</label>
                        </div>
                        <div class="form-check form-check-inline mt-1" style="min-width: 65px;">
                            <input class="form-check-input" type="checkbox" name="tujuan" id="tujuan2" value="2">
                            <label class="form-check-label" for="tujuan2">WAKASAL</label>
                        </div>
                    </div>
                </div>

                <!--                <div class="col-12">-->
                <!--                    <label class="form-label" for="tujuan">Tujuan</label>-->
                <!--                    <select class="form-select" id="tujuan" name="tujuan" multiple="multiple">-->
                <!--                        <option>KASAL</option>-->
                <!--                        <option>WAKASAL</option>-->
                <!--                    </select>-->
                <!--                </div>-->

                <!--                <div class="col-12">-->
                <!--                    <label class="form-label" for="perihal">Tujuan Surat</label>-->
                <!--                    <div class="demo-inline-spacing">-->
                <!--                    <div class="form-check form-check-inline mt-0">-->
                <!--                        <input class="form-check-input" type="checkbox" name="tujuan" value="KASAL" />KASAL-->
                <!--                    </div>-->
                <!--                    <div class="form-check form-check-inline mt-0">-->
                <!--                        <input class="form-check-input" type="checkbox" name="tujuan" value="WAKASAL"/>WAKASAL-->
                <!--                    </div>-->
                <!--                    </div>-->
                <!--                </div>-->

                <div class="col-12">
                    <label for="file" class="form-label">Upload Lampiran</label>
                    <input class="form-control" type="file" id="file" name="file" accept="application/pdf,application/msword,
  application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                </div>

                <div class="col-12">
                    <label class="form-label" for="catatan">Catatan</label>
                    <textarea id="catatan" name="catatan" rows="3" class="form-control"></textarea>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-50">
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        Simpan
                    </button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

</template>

<script>
    var formSuratMasuk = $('#formSuratMasuk'),
        tglSuratEl = null,
        tglTerimaEl = null,
        kerahasiaanEl = null,
        formSuratMasukLoading = false,
        formSuratMasukMode = 'ADD',
        tgl_surat = moment(),
        tgl_terima = moment(),
        // base64data = null,
        acceptFiles = $('#dpz-accept-files');

    jQuery(document).ready(function() {

    })

    $(window).on('load', function() {

        $('#formSuratMasukModal').on('shown.bs.modal', function(e) {
            window.toastr.clear()

            tglSuratEl = $('#tgl_surat', formSuratMasuk).flatpickr({
                altInput: true,
                altFormat: 'j F Y',
                dateFormat: 'Y-m-d',
                // locale: 'id',
                // defaultDate: tgl_surat.format('YYYY-MM-DD'),
                onChange(selectedDates, dateStr) {
                    // sDate = moment(selectedDates[0])
                    // loadData()
                    // loadDataChart()
                }
            })

            tglTerimaEl = $('#tgl_terima', formSuratMasuk).flatpickr({
                altInput: true,
                altFormat: 'j F Y',
                dateFormat: 'Y-m-d',
                // locale: 'id',
                // defaultDate: tgl_terima.format('YYYY-MM-DD'),
                onChange(selectedDates, dateStr) {
                    // sDate = moment(selectedDates[0])
                    // loadData()
                    // loadDataChart()
                }
            })

            $('button[type=submit], input[type=submit]', formSuratMasuk).find('.spinner').hide()

            kerahasiaanEl = $('#kerahasiaan', formSuratMasuk)
            kerahasiaanEl
                .wrap('<div class="position-relative"></div>')
                .prepend('<option selected=""></option>')
                .select2({
                    placeholder: 'Pilih Kerahasiaan',
                    data: [
                        { id: 'BIASA', text: 'BIASA' },
                        { id: 'RAHASIA', text: 'RAHASIA' }
                    ],
                    // minimumResultsForSearch: -1,
                    allowClear: true,
                    dropdownAutoWidth: true,
                    dropdownParent: kerahasiaanEl.parent(),
                    width: '100%'
                }).on('change.select2', function(e) {
                var val = kerahasiaanEl.select2('val')
                if (val) {
                    formSuratMasuk.validate().resetForm()
                }
            })

            // kerahasiaanEl.select2('focus')

            if (formSuratMasuk.my('data').kerahasiaan) {
                kerahasiaanEl.val(formSuratMasuk.my('data').kerahasiaan).trigger('change')
            }

            // if (formSuratMasuk.my('data').file) {
            //     var fileInput = document.querySelector('#file')
            //
            //     // Create a new File object
            //     var myFile = new File([formSuratMasuk.my('data').file], formSuratMasuk.my('data').file, {
            //         type: formSuratMasuk.my('data').file_mime,
            //         lastModified: new Date()
            //     })
            //
            //     // Now let's create a DataTransfer to get a FileList
            //     const dataTransfer = new DataTransfer();
            //     dataTransfer.items.add(myFile);
            //     fileInput.files = dataTransfer.files;
            // }
        })

        $('#formSuratMasukModal').on('hidden.bs.modal', function(e) {
            e.preventDefault()
            e.stopPropagation()

            // base64data = null
            // $('#org-logo').attr('src', 'app-assets/images/suratMasuk/default.jpg')
            $('button[type=submit], input[type=submit]', formSuratMasuk).find('.spinner').hide()

            formSuratMasuk.my('data').tujuan = null
            formSuratMasuk.validate().resetForm()
            formSuratMasuk.my('remove')
        })

        formSuratMasuk.submit(function() {
            if (formSuratMasuk.valid()) {
                $('button[type=submit], input[type=submit]', formSuratMasuk).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formSuratMasuk).find('.spinner').show()
            }
        })
    })

    function formSuratMasukInit(idSuratMasuk = null) {
        if (idSuratMasuk) {
            $.post("<?php echo base_url() ?>suratMasuk/getById", {
                    id: idSuratMasuk,
                    modeView: false
                },
                function(resp) {
                    if (resp.error) {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    }
                    formSuratMasukRender(resp.data.id, resp.data)
                }, 'json')
        } else {
            formSuratMasukRender()
        }
    }

    function formSuratMasukRender(idSuratMasuk = null, formData = null) {

        formSuratMasukMode = idSuratMasuk ? 'EDIT' : 'ADD'
        var title = idSuratMasuk ? 'Edit Data' : 'Tambah Data'

        $('#formSuratMasukModalTitle').html(title + ' <?php echo $page->title ?>')

        // Manifest
        var manifest = {

            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id: <?php echo logged('user_id'); ?>,
                id: null,
                tgl_surat: null,
                tgl_terima: null,
                no_surat: null,
                pengirim: null,
                perihal: null,
                kerahasiaan: null,
                tujuan: null,
                file: null,
                catatan: null
            },

            // Init function
            init: function($form, runtime) {
                $form.empty().html($('#formSuratMasukTpl').html())

                $form.then(function() { // Fade when start succeds
                    $('input:visible:enabled:first', formSuratMasuk).focus()
                    formSuratMasuk.validate({
                        rules: {
                            tgl_surat: {
                                required: true
                            },
                            tgl_terima: {
                                required: true
                            },
                            no_surat: {
                                required: true
                            },
                            pengirim: {
                                required: true
                            },
                            perihal: {
                                required: true
                            },
                            kerahasiaan: {
                                required: true
                            },
                            tujuan: {
                                required: true
                            },
                            file: {
                                required: true
                            }
                        },
                        messages: {
                            tgl_surat: 'Tanggal surat harus diisi',
                            tgl_terima: 'Tanggal terima surat harus diisi',
                            no_surat: 'Nomor surat harus diisi',
                            pengirim: 'Pengirim harus diisi',
                            perihal: 'Perihal harus diisi',
                            kerahasiaan: 'Kerahasiaan surat harus diisi',
                            tujuan: 'Tujuan surat harus diisi',
                            file: 'File surat harus diisi'
                        }
                    })
                })

                if (idSuratMasuk != null) {
                    $('#formSuratMasukModalTitle').html('Edit <?php echo $page->title ?> Data')
                }

                $('#formSuratMasukModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show')
            },

            // Bindings
            ui: {
                '#id': {
                    bind: 'id'
                },
                '#tgl_surat': {
                    bind: 'tgl_surat'
                },
                '#tgl_terima': {
                    bind: 'tgl_terima'
                },
                '#no_surat': {
                    bind: 'no_surat'
                },
                '#pengirim': {
                    bind: 'pengirim'
                },
                '#perihal': {
                    bind: 'perihal'
                },
                '#kerahasiaan': {
                    bind: 'kerahasiaan'
                },
                'input[name=\'tujuan\']': {
                    bind: 'tujuan'
                },
                '#file': {
                    bind: 'file'
                },
                '#catatan': {
                    bind: 'catatan'
                }
            }
        }

        // Init $.my over DOM node
        formSuratMasuk.my(manifest, formData)
    }

    function formSuratMasukSubmit() {
        if (formSuratMasuk.valid()) {

            var data = formSuratMasuk.my('data')

            var formData = new FormData(),
                fileInput = document.querySelector('#file')

            // Append the text fields
            formData.append('id', data.id)
            formData.append('tgl_surat', data.tgl_surat)
            formData.append('tgl_terima', data.tgl_terima)
            formData.append('no_surat', data.no_surat)
            formData.append('pengirim', data.pengirim)
            formData.append('perihal', data.perihal)
            formData.append('tujuan', data.tujuan)
            formData.append('kerahasiaan', data.kerahasiaan)
            formData.append('catatan', data.catatan)

            let docFile = fileInput.files
            if (docFile.length > 0) {
                console.log('docFile', docFile[0])
                formData.append('file', docFile[0])
            }

            fetch("<?php echo base_url(); ?>suratMasuk/process_save", {
                method: 'POST',
                // headers: {
                //     'X-CSRF-TOKEN': formProfile.querySelector('[name="_token"]').value
                // },
                body: formData
            }).then((resp) => resp.json())
              .then(resp => {

                  if (resp.error) {
                      toastr['error'](resp.message, 'ERROR', {
                          closeButton: true,
                          tapToDismiss: false
                      })
                  } else {
                      // base64data = null
                      // $('#org-logo').attr('src', 'app-assets/images/suratMasuk/default.jpg')
                      // gridSuratMasuk.ajax.reload()
                      loadDashboardData()

                      toastr['success'](' ', resp.message, {
                          closeButton: false,
                          tapToDismiss: true
                      })

                      if (formSuratMasukMode == 'ADD') {

                          formSuratMasuk.trigger('reset')
                          formSuratMasuk.validate().resetForm()

                          $('input:visible:enabled:first', formSuratMasuk).focus()
                          kerahasiaanEl.val(null).trigger('change')
                          tglSuratEl.setDate(tgl_surat.format('YYYY-MM-DD'))
                          tglTerimaEl.setDate(tgl_terima.format('YYYY-MM-DD'))
                          formSuratMasuk.my('data').tujuan = null
                      } else {
                          $('#formSuratMasukModal').modal('hide')
                      }
                  }

                  $('button[type=submit], input[type=submit]', formSuratMasuk).prop('disabled', false)
                  $('button[type=submit], input[type=submit]', formSuratMasuk).find('.spinner').hide()

                  return false
              })

            return false
        }

        return false
    }
</script>
