<!DOCTYPE html>
<html class="loading light-layout" lang="en" data-layout="light-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="<?php echo $this->config->item("appdesc"); ?>">
    <meta name="keywords" content="surat, arsip">

    <meta name="author" content="<?php echo $this->config->item("appowner"); ?>">
    <title><?php echo $this->config->item("apptitle"); ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="app-assets/images/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="app-assets/images/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="app-assets/images/ico/favicon-16x16.png">
    <link rel="manifest" href="app-assets/images/ico/site.webmanifest">
    <link rel="mask-icon" href="app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/js/bootstrap-daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/charts/apexcharts.css">
    <!--    <link href="https://unpkg.com/filepond@^4/dist/filepond.css" rel="stylesheet" />-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/extensions/ext-component-toastr.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>app-assets/css/plugins/forms/form-file-uploader.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/datatable-custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/daterangepicker-custom.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<?php $this->load->view("partial/header"); ?>

<?php $this->load->view("partial/sidebar"); ?>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">

        <div class="content-body">

            <div id="btn-toolbar" class="row mb-50">
                <div class="col-md-6 mb-1">
                    <div class="d-md-flex">
                        <div class="btn-group me-md-1 me-0 mb-md-0 mb-1 flex-fill" role="group"
                             aria-label="Basic radio toggle button group">
                            <input value="harian" type="radio" class="btn-check" name="period" id="radio_option1"
                                   autocomplete="off"/>
                            <label class="btn btn-outline-primary" for="radio_option1">Harian</label>

                            <input value="bulanan" type="radio" class="btn-check" name="period" id="radio_option2"
                                   autocomplete="off" checked/>
                            <label class="btn btn-outline-primary" for="radio_option2">Bulanan</label>

                            <input value="tahunan" type="radio" class="btn-check" name="period" id="radio_option3"
                                   autocomplete="off"/>
                            <label class="btn btn-outline-primary" for="radio_option3">Tahunan</label>

                            <input value="manual" type="radio" class="btn-check" name="period" id="radio_option4"
                                   autocomplete="off"/>
                            <label class="btn btn-outline-primary" for="radio_option4" style="width: 110px;">Pilih
                                Tanggal</label>
                        </div>

                        <div class="position-relative me-1 mb-md-0 mb-1 flex-fill w-100">
                            <input type="text" id="date" class="form-control" placeholder=""/>
                        </div>

                        <div class="position-relative me-1 mb-md-0 mb-1 flex-fill w-100">
                            <input id="month-picker" type="text" class="form-control shadow-none bg-transparent pe-0"/>
                        </div>

                        <div class="position-relative mb-md-0 mb-1 flex-fill w-100">
                            <input id="year-picker" type="text" class="form-control shadow-none bg-transparent pe-0"/>
                        </div>

                        <div class="position-relative mb-md-0 mb-1 flex-fill w-100">
                            <button id="advance-daterange"
                                    class="btn btn-outline-primary d-flex align-items-center text-start"><span
                                    class="text-truncate">&nbsp;</span><i class="fa fa-caret-down ms-1"></i></button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row match-height">
                <div class="col-12">
                    <div class="row match-height">

                        <div class="col-lg col-md-4 col-6 counter-summary">
                            <div class="card card-block bg-secondary text-white">
                                <div class="card-body pb-50" style="position: relative;">
                                    <h3 class="mb-0 text-white">Baru</h3>
                                    <h2 class="fw-bolder display-5 text-white" id="suratMasukBaruCount"></h2>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg col-md-4 col-6 counter-summary">
                            <div class="card card-block bg-warning text-white">
                                <div class="card-body pb-50" style="position: relative;">
                                    <h3 class="mb-0 text-white">Sedang Diverifikasi</h3>
                                    <h2 class="fw-bolder display-5 text-white"
                                        id="suratMasukSedangVerifikasiCount"></h2>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg col-md-4 col-6 counter-summary">
                            <div class="card card-block bg-success text-white">
                                <div class="card-body pb-50" style="position: relative;">
                                    <h3 class="mb-0 text-white">Terverifikasi</h3>
                                    <h2 class="fw-bolder display-5 text-white" id="suratMasukTerverifikasiCount"></h2>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg col-md-6 col-6 counter-summary">
                            <div class="card card-block bg-danger text-white">
                                <div class="card-body pb-50" style="position: relative;">
                                    <h3 class="mb-0 text-white">Ditolak</h3>
                                    <h2 class="fw-bolder display-5 text-white" id="suratMasukDitolakCount"></h2>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg col-md-6 col-6 counter-summary">
                            <div class="card card-block bg-primary text-white">
                                <div class="card-body pb-50" style="position: relative;">
                                    <h3 class="mb-0 text-white">Disposisi</h3>
                                    <h2 class="fw-bolder display-5 text-white" id="suratMasukDisposisiCount"></h2>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <!-- <div class="card-header">
                    <h4 class="card-title">card-title</h4>
                </div> -->
                <div class="card-body p-0">
                    <table id="gridSuratMasuk" class="table text-nowrap w-100">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>No. Agenda</th>
                            <th>No. Surat</th>
                            <th>Tgl. Surat</th>
                            <th>Tgl. Terima</th>
                            <th>Pengirim</th>
                            <th>Perihal</th>
                            <th>Tujuan</th>
                            <th>Kerahasiaan</th>
                            <th>Status</th>
                            <th>Level Verifikasi</th>
                            <!--                            <th>Catatan</th>-->
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Vendor JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sugar/1.4.1/sugar.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/jQuery.my/jquerymy.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/polyfill.min.js"></script>

<!-- <script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script> -->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>

<script
    src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons/js/buttons.print.min.js"></script>
<script
    src="<?php echo base_url(); ?>app-assets/vendors/js/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js"></script>

<script src="<?php echo base_url(); ?>app-assets/vendors/js/moment/moment-with-locales.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/jq-signature.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<!--<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>-->
<!--<script src="https://unpkg.com/filepond@^4/dist/filepond.js"></script>-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/core/app.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/js/init.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?php echo base_url(); ?>app-assets/js/scripts/extensions/ext-component-toastr.min.js"></script>
<!-- END: Page JS-->

<div class="modal fade" id="formSuratMasukModal" data-bs-focus="false" aria-labelledby="formSuratMasukModalTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- <div class="modal-header bg-transparent">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div> -->
            <div class="modal-body px-sm-5 mx-50 pb-5">
                <h1 class="text-center mb-1" id="formSuratMasukModalTitle"></h1>

                <?php $this->load->view("suratMasuk/form"); ?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewSuratMasukModal" data-bs-focus="false" aria-labelledby="formSuratMasukModalTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
            <!-- <div class="modal-header bg-transparent">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div> -->
            <div class="modal-body px-sm-5 mx-50 pb-5">
                <h1 class="text-center mb-1" id="viewSuratMasukModalTitle"></h1>

                <?php $this->load->view("suratMasuk/view"); ?>

            </div>
        </div>
    </div>
</div>

<script>
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        currency: {
            symbol: 'Rp'
        }
    })
    numeral.locale('id')
    moment.locale('id')

    var formDataView = null
    var interval = null
    var gridSuratMasuk = null

    var periodEl = $('input[name="period"]'),
        dateEl = $('#date'),
        monthEl = $('#month-picker'),
        yearEl = $('#year-picker'),

        rangePickr = $('#advance-daterange'),
        yearEl2 = $('#year-picker-2'),
        period = 'bulanan',
        monthSelected = moment().format('MM'),
        yearSelected = moment().format('YYYY'),
        date = moment(),

        yearSelected2 = new Date().getFullYear(),
        sDate = moment(),
        eDate = moment(),
        cardSection = $('.card-block'),
        suratMasukBaruCount = 0,
        suratMasukSedangVerifikasiCount = 0,
        suratMasukTerverifikasiCount = 0,
        suratMasukDitolakCount = 0,
        suratMasukDisposisiCount = 0

    jQuery(document).ready(function() {

        $(periodEl).click(function() {
            loadDashboardData()
        })

        setValue()

        dateEl.flatpickr({
            altInput: true,
            altFormat: 'j F Y',
            dateFormat: 'Y-m-d',
            // locale: 'id',
            defaultDate: date.format('YYYY-MM-DD'),
            onChange(selectedDates, dateStr) {
                sDate = moment(selectedDates[0])
                loadDashboardData()
                // loadDataChart()
            }
        })

        monthEl.val(monthSelected)
               .select2({
                   data: generateArrayOfMonths(),
                   allowClear: false,
                   dropdownAutoWidth: true,
                   dropdownParent: monthEl.parent(),
                   minimumResultsForSearch: -1,
                   width: '100%'
               }).on('change.select2', function(e) {
            loadDashboardData()
            // loadDataChart()
        })

        yearEl.val(yearSelected)
              .select2({
                  data: generateArrayOfYears(),
                  allowClear: false,
                  dropdownAutoWidth: true,
                  dropdownParent: yearEl.parent(),
                  minimumResultsForSearch: -1,
                  width: '100%'
              }).on('change.select2', function(e) {
            loadDashboardData()
            // loadDataChart()
        })

        yearEl2.val(yearSelected2)
               .wrap('<div class="position-relative" style="width:80px"></div>')
               .select2({
                   data: generateArrayOfYears(),
                   allowClear: false,
                   dropdownAutoWidth: true,
                   dropdownParent: yearEl2.parent(),
                   minimumResultsForSearch: -1,
                   width: '100%'
               }).on('change.select2', function(e) {
            loadDataChart()
        })

        $('#advance-daterange span').html(sDate.format('D MMM YYYY') + ' - ' + eDate.format('D MMM YYYY'))

        rangePickr.daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: sDate,
            endDate: eDate,
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to '
        }, function(start, end, label) {
            $('#advance-daterange span').html(start.format('D MMM YYYY') + ' - ' + end.format('D MMM YYYY'))
        }).on('apply.daterangepicker', function(ev, picker) {
            sDate = picker.startDate
            eDate = picker.endDate
            loadDashboardData()
        })

        gridSuratMasuk = $('#gridSuratMasuk').DataTable({
            dom: '<"d-flex flex-row align-items-center mx-0 px-1"<"mt-50"B>f>rt<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"<"d-inline-flex flex-row align-items-baseline"li>><"col-sm-12 col-md-6"p>>',
            lengthMenu: [10, 20, 30, 40, 50],
            processing: true,
            serverSide: true,
            responsive: false,
            scrollX: true,
            language: {
                search: '',
                searchPlaceholder: 'pencarian',
                processing: '<span>Loading...</span>',
                lengthMenu: '_MENU_',
                paginate: {
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },
            order: [
                [1, 'desc']
            ],
            ajax: {
                'url': "<?php echo base_url(); ?>" + 'suratMasuk/get_list',
                'data': function(data) {
                    data.period = period,
                        data.month = monthSelected,
                        data.year = yearSelected,
                        data.sDate = moment(sDate).format('YYYY-MM-DD'),
                        data.eDate = moment(eDate).format('YYYY-MM-DD')
                },
                'type': 'POST',
                'dataType': 'json'
            },
            buttons: {
                dom: {
                    button: {
                        tag: 'button',
                        className: ''
                    }
                },
                buttons: [{
                    titleAttr: 'add new',
                    text: '<i class="fas fa-sm fa-fw fa-plus"></i>',
                    className: 'btn btn-icon btn-outline-secondary waves-effect',
                    action: function(e, dt, node, config) {
                        formSuratMasukInit()
                        // $('#formSuratMasukModalTitle').html('Add Data <?php echo $page->title ?>');
                        // $('#formSuratMasukModal').modal('show');
                    }
                },
                    {
                        titleAttr: 'refresh',
                        text: '<i class="fas fa-sm fa-fw fa-sync"></i>',
                        className: 'btn btn-icon btn-outline-secondary waves-effect',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload()
                        }
                    }
                    // {
                    //     extend: 'print',
                    //     className: 'btn btn-outline-default btn-sm ms-2'
                    // },
                    // {
                    //     extend: 'csv',
                    //     className: 'btn btn-outline-default btn-sm'
                    // }
                ]
            },
            columns: [
                {
                    data: 'id'
                }, {
                    data: 'id'
                },
                {
                    data: 'no_agenda'
                },
                {
                    data: 'no_surat'
                },
                {
                    data: 'tgl_surat'
                },
                {
                    data: 'tgl_terima'
                },
                {
                    data: 'pengirim'
                },
                {
                    data: 'perihal'
                },
                {
                    data: 'tujuan'
                },
                {
                    data: 'kerahasiaan'
                },
                {
                    data: 'status_verifikasi'
                },
                {
                    data: 'level_verifikasi'
                }
                // {
                //     data: 'catatan'
                // }
            ],
            columnDefs: [{
                targets: 0,
                width: '10px',
                searchable: false,
                orderable: false,
                className: 'pt-0 pb-0 px-1',
                render: function(data, type, row, meta) {
                    var el = `<button title="Edit" onclick="btnEditSuratMasukClicked('${row.id}')" class="btn btn-icon btn-outline-primary btn-sm waves-effect me-50" id=n-"' + meta.row + '"><i class="fas fa-sm fa-fw fa-edit"></i></button><button title="Lihat Detail" onclick="btnViewSuratMasukClicked('${row.id}')" class="btn btn-icon btn-outline-primary btn-sm waves-effect me-50" id=n-"' + meta.row + '"><i class="fas fa-sm fa-fw fa-eye"></i></button>`

                    if (row.deleted_at !== null) {
                        el += `</button><button title="Hapus" onclick="btnDeleteSuratMasukClicked('${row.id}')" class="btn btn-icon btn-outline-primary btn-sm waves-effect me-50" id=n-"' + meta.row + '"><i class="fas fa-sm fa-fw fa-trash"></i></button>`
                    }

                    return el
                }
            },
                {
                    targets: 1,
                    visible: false,
                    width: '30px'
                },
                {
                    targets: 2,
                    visible: false,
                    width: '30px'
                },
                {
                    targets: 3,
                },
                {
                    targets: 9,
                    width: '80px',
                    render: function(data, type, row, meta) {
                        if (row.kerahasiaan == 'BIASA') {
                            return `<span class="badge badge-light-success">${row.kerahasiaan}</span>`
                        } else {
                            return `<span class="badge badge-light-warning">${row.kerahasiaan}</span>`
                        }
                    }
                },
                {
                    targets: 10,
                    width: '80px',
                    render: function(data, type, row, meta) {
                        if (row.status_verifikasi == 'BARU') {
                            return `<span class="badge badge-light-secondary">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'SEDANG DIVERIFIKASI') {
                            return `<span class="badge badge-light-warning">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'DITOLAK') {
                            return `<span class="badge badge-light-danger">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'TERVERIFIKASI') {
                            return `<span class="badge badge-light-success">${row.status_verifikasi}</span>`
                        } else if (row.status_verifikasi == 'DISPOSISI') {
                            return `<span class="badge badge-light-primary">${row.status_verifikasi}</span>`
                        } else {
                            return `<span class="badge">${row.status_verifikasi}</span>`
                        }
                    }
                },
                {
                    targets: 11,
                    width: '80px',
                    className: 'text-center',
                    render: function(data, type, row, meta) {

                        var level_verifikasi = row.level_verifikasi,
                            level_verifikasi_max = <?php echo $this->config->item("level_verifikasi_max"); ?>

                        if (row.status_verifikasi == 'SEDANG DIVERIFIKASI' || (row.status_verifikasi == 'DITOLAK' && level_verifikasi > 0)) {
                            level_verifikasi = row.level_verifikasi - 1
                        } else if (row.status_verifikasi == 'TERVERIFIKASI') {
                            level_verifikasi = level_verifikasi_max
                        }

                        return `${level_verifikasi} / ${level_verifikasi_max}`
                    }
                }
                // {
                //     targets: 11,
                //     className: 'none',
                //     render: function(data, type, row, meta) {
                //         return `<pre>${row.catatan}</pre>`
                //     }
                // }
            ]
            // createdRow: function(row, data, dataIndex) {
            //     $(row).addClass('row-clicked');
            // }
        })

        $('#gridSuratMasuk tbody').on('click', '.dtr-control button', function(e) {
            e.preventDefault()
            e.stopPropagation()
        })

        setTimeout(function() {
            loadDashboardData()
        }, 1000)
    })

    $('.pageloader').fadeOut('slow')

    function showLoading() {
        cardSection.block({
            message: '<div class="spinner-border text-white" role="status"></div>',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            },
            overlayCSS: {
                opacity: 0.5
            }
        })
    }

    function hideLoading() {
        cardSection.unblock()
    }

    function setValue() {
        $('#suratMasukDitolakCount').html(numeral(suratMasukDitolakCount).format('0,0'))
        $('#suratMasukBaruCount').html(numeral(suratMasukBaruCount).format('0,0'))
        $('#suratMasukSedangVerifikasiCount').html(numeral(suratMasukSedangVerifikasiCount).format('0,0'))
        $('#suratMasukTerverifikasiCount').html(numeral(suratMasukTerverifikasiCount).format('0,0'))
        $('#suratMasukDisposisiCount').html(numeral(suratMasukDisposisiCount).format('0,0'))
    }

    function loadDashboardData() {
        showLoading()

        period = periodEl.filter(':checked').val()

        if (period == 'harian') {
            dateEl.parent().show()
            monthEl.parent().hide()
            yearEl.parent().hide()
            rangePickr.parent().hide()
        } else if (period == 'bulanan') {
            dateEl.parent().hide()
            monthEl.parent().show()
            yearEl.parent().show()
            rangePickr.parent().hide()
        } else if (period == 'tahunan') {
            dateEl.parent().hide()
            monthEl.parent().hide()
            yearEl.parent().show()
            rangePickr.parent().hide()
        } else if (period == 'manual') {
            dateEl.parent().hide()
            monthEl.parent().hide()
            yearEl.parent().hide()
            rangePickr.parent().show()
        }

        monthSelected = monthEl.select2('val')
        yearSelected = yearEl.select2('val')

        gridSuratMasuk.ajax.reload()

        $.post("<?php echo base_url() ?>dash/get_dashboard_data", {
                period: period,
                month: monthSelected,
                year: yearSelected,
                sDate: moment(sDate).format('YYYY-MM-DD'),
                eDate: moment(eDate).format('YYYY-MM-DD')
            },
            function(r) {
                if (!r.error) {
                    suratMasukBaruCount = parseInt(r.data.suratMasukBaruCount)
                    suratMasukSedangVerifikasiCount = parseInt(r.data.suratMasukSedangVerifikasiCount)
                    suratMasukTerverifikasiCount = parseInt(r.data.suratMasukTerverifikasiCount)
                    suratMasukDitolakCount = parseInt(r.data.suratMasukDitolakCount)
                    suratMasukDisposisiCount = parseInt(r.data.suratMasukDisposisiCount)

                    suratKeluarBaruCount = parseInt(r.data.suratKeluarBaruCount)
                    suratKeluarSedangVerifikasiCount = parseInt(r.data.suratKeluarSedangVerifikasiCount)
                    suratKeluarTerverifikasiCount = parseInt(r.data.suratKeluarTerverifikasiCount)
                    suratKeluarDitolakCount = parseInt(r.data.suratKeluarDitolakCount)
                }

                setValue()
                hideLoading()
            }, 'json')
    }

    function generateArrayOfYears() {
        var max = new Date().getFullYear()
        var min = max - 4
        var years = []

        for (var i = min; i <= max; i++) {
            years.push({
                id: i,
                text: i
            })
        }
        return years
    }

    function generateArrayOfMonths() {
        var min = moment().startOf('year').format('MM')
        var max = moment().endOf('year').format('MM')
        var months = []

        for (var i = parseInt(min); i <= parseInt(max); i++) {
            var month = moment(`1900-${i}-1`, 'YYYY-M-D')
            months.push({
                id: month.format('MM'),
                text: month.format('MMMM')
            })
        }

        return months
    }

    function btnEditSuratMasukClicked(data) {
        formSuratMasukInit(data)
    }

    function btnViewSuratMasukClicked(data) {
        viewSuratMasukInit(data)
    }

    function btnDeleteSuratMasukClicked(row) {
        // var data = gridSuratMasuk.rows(row).data()[0]

        Swal.fire({
            text: `Hapus Data Surat Masuk`,
            icon: 'warning',
            showCancelButton: true,
            customClass: {
                confirmButton: 'btn btn-lg btn-outline-success me-1',
                cancelButton: 'btn btn-lg btn-outline-secondary'
            },
            confirmButtonText: 'YA',
            cancelButtonText: 'TIDAK',
            buttonsStyling: false
        }).then((result) => {
            if (result.isConfirmed) {
                // clearInterval(interval)
                $('#resrow').hide()

                $.post("<?php echo base_url() ?>suratMasuk/delete", 'id=' + row,
                    function(resp) {
                        if (!resp.error) {
                            gridSuratMasuk.ajax.reload()

                            toastr['success'](resp.message, 'SUKSES', {
                                closeButton: true,
                                tapToDismiss: false
                            })

                        } else {
                            toastr['error'](resp.message, 'ERROR', {
                                closeButton: true,
                                tapToDismiss: false
                            })
                        }
                    }, 'json')
                return false
            }
        })
    }
</script>

<?php $this->load->view("partial/socket-js"); ?>

</body>
<!-- END: Body-->

</html>
