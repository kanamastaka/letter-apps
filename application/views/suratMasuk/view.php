<style>
    .form-check {
        min-width: 165px;
        margin-bottom: 5px;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-50 text-primary">Detail Surat Masuk</h4>
                <div>
                    <button type="button"
                            class="btn btn-primary waves-effect waves-float waves-light redisposisi-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        <span id="redisposisi-btn-txt">REDISPOSISI</span>
                    </button>
                    <button type="button" class="btn btn-primary waves-effect waves-float waves-light verif-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        <span id="verif-btn-txt">VERIFIKASI</span>
                    </button>
                    <button type="button" class="btn btn-danger waves-effect waves-float waves-light reject-btn">
                            <span class="spinner spinner-border spinner-border-sm" role="status"
                                  aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        <span id="reject-btn-txt">TOLAK</span>
                    </button>
                    <button type="button"
                            class="btn btn-icon btn-secondary waves-effect waves-float waves-light close-btn">
                        <i data-feather="x-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-2">
                        <dl class="row mb-0">
                            <dt class="col-sm-2 fw-bolder mb-1">Tanggal Surat</dt>
                            <dd class="col-sm-4 mb-1" id="tglSuratEl"></dd>

                            <dt class="col-sm-2 fw-bolder mb-1">Tanggal Terima</dt>
                            <dd class="col-sm-4 mb-1" id="tglTerimaEl"></dd>

                            <dt class="col-sm-2 fw-bolder mb-1">Pengirim</dt>
                            <dd class="col-sm-4 mb-1" id="pengirimEl"></dd>

                            <dt class="col-sm-2 fw-bolder mb-1">Perihal</dt>
                            <dd class="col-sm-4 mb-1" id="perihalEl"></dd>

                            <dt class="col-sm-2 fw-bolder mb-1">Tujuan</dt>
                            <dd class="col-sm-4 mb-1" id="tujuanEl"></dd>

                            <dt class="col-sm-2 fw-bolder mb-1">Kerahasiaan</dt>
                            <dd class="col-sm-4 mb-1" id="kerahasiaanEl"></dd>

                            <dd id="catatan-container" class="col-12 mb-1">
                                <label class="form-label" for="catatan">Catatan</label>
                                <textarea id="catatan" name="catatan" rows="3" class="form-control"></textarea>
                                <!--                                <pre id="catatanEl" class="list-view">-->
                                <!--        -->
                                <!--                                </pre>-->
                            </dd>

                            <dd id="verifParafContainer" class="col-12 mb-1">
                                <label class="form-label" for="catatan">Paraf</label>
                                <div class="d-flex">
                                    <div class='js-signature-blank d-none'></div>
                                    <div class='js-signature position-relative'>
                                        <label for="img-upload"
                                               class="btn btn-sm btn-secondary mb-50 position-absolute rounded-0 js-signature-clear-btn">CLEAR</label>
                                    </div>
                                </div>
                            </dd>

                        </dl>
                    </div>

                    <div id="disposisi-info-container" class="col-12 mb-2">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <h4 class="fw-bolder text-primary">Disposisi</h4>
                                <dl class="row mb-0">
                                    <dt class="col-sm-2 fw-bolder mb-1">Tanggal</dt>
                                    <dd class="col-sm-10 mb-1" id="tglDisposisiEl"></dd>

                                    <dt class="col-sm-2 fw-bolder mb-1">Alamat Aksi</dt>
                                    <dd class="col-sm-10 mb-1" id="disposisiUserEl"></dd>

                                    <dt class="col-sm-2 fw-bolder mb-1">Aksi</dt>
                                    <dd class="col-sm-10 mb-1" id="tindakanSegeraInfoEl"></dd>

                                    <dt class="col-sm-2 fw-bolder mb-1">Catatan</dt>
                                    <dd class="col-sm-10 mb-1" id="catatanDisposisiInfoEl"></dd>

                                    <!--                                    <dt class="col-sm-2 fw-bolder mb-1">Tindakan Biasa</dt>-->
                                    <!--                                    <dd class="col-sm-4 mb-1" id="tindakanBiasaInfoEl"></dd>-->
                                </dl>
                            </div>
                        </div>
                    </div>

                    <div id="fileInfoElContainer" class="col-12">
                        <dl class="row mb-0">
                            <dt class="col-sm-2 fw-bolder mb-1">File</dt>
                            <dd class="col-sm-10 mb-1" id="filename">
                                <a id="fileDownloadEl" class="btn btn-primary waves-effect waves-float waves-light">
                                    Preview File
                                </a>
                            </dd>
                        </dl>
                    </div>

                    <div id="disposisi-form-container" class="p-0">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title text-primary">Disposisi</h4>
                            </div>
                            <div class="card-body">
                                <form id="formDisposisiSuratMasuk" class="row gy-1 gx-2"
                                      onsubmit="return formDisposisiSuratMasukSubmit()">

                                    <!--                                    <div class="col-12">-->
                                    <!--                                        <label class="form-label" for="disposisi_user_id">Disposisi Kepada</label>-->
                                    <!--                                        <select class="select2 select2-data-ajax form-select"-->
                                    <!--                                                id="disposisi_user_id" name="disposisi_user_id"></select>-->
                                    <!--                                    </div>-->

                                    <div class="col-12 mt-0">
                                        <label class="form-label text-uppercase fw-bolder" for="">Alamat Aksi</label>
                                        <div class="demo-inline-spacing">
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_2"
                                                       name="alamat_aksi" value="2">
                                                <label class="form-check-label" for="alamat_aksi_2">WAKASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_17"
                                                       name="alamat_aksi" value="17">
                                                <label class="form-check-label" for="alamat_aksi_17">KADISKUAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_28"
                                                       name="alamat_aksi" value="28">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_28">KADISMINPERSAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_41"
                                                       name="alamat_aksi" value="41">
                                                <label class="form-check-label" for="alamat_aksi_41">PANGKOARMADA
                                                    RI</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_3"
                                                       name="alamat_aksi" value="3">
                                                <label class="form-check-label" for="alamat_aksi_3">IRJENAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_18"
                                                       name="alamat_aksi" value="18">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_18">KADISINFOLAHTAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_29"
                                                       name="alamat_aksi" value="29">
                                                <label class="form-check-label" for="alamat_aksi_29">KADISDIKAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_42"
                                                       name="alamat_aksi" value="42">
                                                <label class="form-check-label" for="alamat_aksi_42">PANGKOARMADA
                                                    I</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_4"
                                                       name="alamat_aksi" value="4">
                                                <label class="form-check-label" for="alamat_aksi_4">KOORSAHLI
                                                    KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_19"
                                                       name="alamat_aksi" value="19">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_19">KADISLITBANGAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_30"
                                                       name="alamat_aksi" value="30">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_30">KADISWATPERSAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_43"
                                                       name="alamat_aksi" value="43">
                                                <label class="form-check-label" for="alamat_aksi_43">PANGKOARMADA
                                                    II</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_5"
                                                       name="alamat_aksi" value="5">
                                                <label class="form-check-label" for="alamat_aksi_5">ASRENA KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_31"
                                                       name="alamat_aksi" value="31">
                                                <label class="form-check-label" for="alamat_aksi_31">KADISKESAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_44"
                                                       name="alamat_aksi" value="44">
                                                <label class="form-check-label" for="alamat_aksi_44">PANGKOARMADA
                                                    III</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_6"
                                                       name="alamat_aksi" value="6">
                                                <label class="form-check-label" for="alamat_aksi_6">ASINTEL
                                                    KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_20"
                                                       name="alamat_aksi" value="20">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_20">KADISPAMSANAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_32"
                                                       name="alamat_aksi" value="32">
                                                <label class="form-check-label" for="alamat_aksi_32">KADISPSIAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_45"
                                                       name="alamat_aksi" value="45">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_45">PANGKOLINLAMIL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_7"
                                                       name="alamat_aksi" value="7">
                                                <label class="form-check-label" for="alamat_aksi_7">ASOPS KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_21"
                                                       name="alamat_aksi" value="21">
                                                <label class="form-check-label" for="alamat_aksi_21">KADISPENAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_33"
                                                       name="alamat_aksi" value="33">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_33">KADISBINTALAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_46"
                                                       name="alamat_aksi" value="46">
                                                <label class="form-check-label" for="alamat_aksi_46">DANKORMAR</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_8"
                                                       name="alamat_aksi" value="8">
                                                <label class="form-check-label" for="alamat_aksi_8">ASPERS KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_22"
                                                       name="alamat_aksi" value="22">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_22">KADISJARAHAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_47"
                                                       name="alamat_aksi" value="47">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_47">DANKODIKLATAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_9"
                                                       name="alamat_aksi" value="9">
                                                <label class="form-check-label" for="alamat_aksi_9">ASLOG KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_34"
                                                       name="alamat_aksi" value="34">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_34">KADISFASLANAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_48"
                                                       name="alamat_aksi" value="48">
                                                <label class="form-check-label" for="alamat_aksi_48">GUBERNUR
                                                    AAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_10"
                                                       name="alamat_aksi" value="10">
                                                <label class="form-check-label" for="alamat_aksi_10">ASPOTMAR
                                                    KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_23"
                                                       name="alamat_aksi" value="23">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_23">KADISOPSLATAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_35"
                                                       name="alamat_aksi" value="35">
                                                <label class="form-check-label" for="alamat_aksi_35">KADISMATAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_49"
                                                       name="alamat_aksi" value="49">
                                                <label class="form-check-label" for="alamat_aksi_49">DANSESKOAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_11"
                                                       name="alamat_aksi" value="11">
                                                <label class="form-check-label" for="alamat_aksi_11">ASKOMLEK
                                                    KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_24"
                                                       name="alamat_aksi" value="24">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_24">KADISKOMLEKAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_36"
                                                       name="alamat_aksi" value="36">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_36">KADISENLEKAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_50"
                                                       name="alamat_aksi" value="50">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_50">DANPUSHIDROSAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_25"
                                                       name="alamat_aksi" value="25">
                                                <label class="form-check-label" for="alamat_aksi_25">KADISKUMAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_37"
                                                       name="alamat_aksi" value="37">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_37">KADISLAIKMATAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_12"
                                                       name="alamat_aksi" value="12">
                                                <label class="form-check-label" for="alamat_aksi_12">KAPUSKODAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_26"
                                                       name="alamat_aksi" value="26">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_26">KADISPOTMARAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_38"
                                                       name="alamat_aksi" value="38">
                                                <label class="form-check-label" for="alamat_aksi_38">KADISBEKAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_51"
                                                       name="alamat_aksi" value="51">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_51">DANPUSPENERBAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_13"
                                                       name="alamat_aksi" value="13">
                                                <label class="form-check-label" for="alamat_aksi_13">KASETUMAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_39"
                                                       name="alamat_aksi" value="39">
                                                <label class="form-check-label" for="alamat_aksi_39">KADISADAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_52"
                                                       name="alamat_aksi" value="52">
                                                <label class="form-check-label" for="alamat_aksi_52">DANPUSPOMAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_14"
                                                       name="alamat_aksi" value="14">
                                                <label class="form-check-label" for="alamat_aksi_14">DANDENMA
                                                    MABESAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_53"
                                                       name="alamat_aksi" value="53">
                                                <label class="form-check-label" for="alamat_aksi_53">DAN STTAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_15"
                                                       name="alamat_aksi" value="15">
                                                <label class="form-check-label" for="alamat_aksi_15">KOORSMIN
                                                    KASAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                &nbsp;
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_54"
                                                       name="alamat_aksi" value="54">
                                                <label class="form-check-label"
                                                       for="alamat_aksi_54">DANPUSKOPASKA</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_16"
                                                       name="alamat_aksi" value="16">
                                                <label class="form-check-label" for="alamat_aksi_16">KA PP
                                                    JALASENASTRI</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_27"
                                                       name="alamat_aksi" value="27">
                                                <label class="form-check-label" for="alamat_aksi_27">KEINKOPAL</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_40"
                                                       name="alamat_aksi" value="40">
                                                <label class="form-check-label" for="alamat_aksi_40">KA YASBHUM</label>
                                            </div>
                                            <div class="form-check form-check-inline mt-0">
                                                <input class="form-check-input" type="checkbox" id="alamat_aksi_55"
                                                       name="alamat_aksi" value="55">
                                                <label class="form-check-label" for="alamat_aksi_55">KA YASNALA</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <label class="form-label text-uppercase fw-bolder" for="">Aksi</label>
                                        <div id="aksiListEl" class="demo-inline-spacing"></div>
                                    </div>

                                    <div class="col-12">
                                        <label class="form-label" for="catatan_disposisi">Catatan</label>
                                        <textarea id="catatan_disposisi" name="catatan_disposisi" rows="3"
                                                  class="form-control"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label" for="catatan_disposisi">Paraf</label>
                                        <div class="d-flex">
                                            <div class='js-signature-blank d-none'></div>
                                            <div class='js-signature position-relative'>
                                                <label for="img-upload"
                                                       class="btn btn-sm btn-secondary mb-50 position-absolute rounded-0 js-signature-clear-btn">CLEAR</label>
                                            </div>
                                            <img src="<?php echo base_url(); ?>app-assets/images/frame.png" alt=""
                                                 width="100px">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary me-50">
                                                <span class="spinner spinner-border spinner-border-sm" role="status"
                                                      aria-hidden="true"></span>
                                            <span class="visually-hidden">Loading...</span>
                                            Proses Disposisi
                                        </button>
                                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                                aria-label="Close">
                                            Batal
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="fileElContainer" class="col-12">
                        <iframe id="fileEl" width="100%" height="800px" frameborder="0" src=""></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="mb-2">
            <div id="verif-progress-chart"></div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Riwayat</h4>
            </div>
            <div id="riwayat-container" class="card-body" style="position: relative;height: 400px;overflow: hidden;">

                <ul id="riwayatEl" class="timeline">
                    <!--                    <li class="timeline-item">-->
                    <!--                        <span class="timeline-point timeline-point-indicator"></span>-->
                    <!--                        <div class="timeline-event">-->
                    <!--                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">-->
                    <!--                                <h6>12 Invoices have been paid</h6>-->
                    <!--                                <span class="timeline-event-time">12 min ago</span>-->
                    <!--                            </div>-->
                    <!--                            <p>Invoices have been paid to the company.</p>-->
                    <!--                            <div class="d-flex flex-row align-items-center">-->
                    <!--                                <img class="me-1" src="../../../app-assets/images/icons/file-icons/pdf.png" alt="invoice" height="23">-->
                    <!--                                <span>invoice.pdf</span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    var viewSuratMasukModalEl = $('#viewSuratMasukModal'),
        fileInfoElContainer = $('#fileInfoElContainer'),
        fileElContainer = $('#fileElContainer'),
        catatanContainer = $('#catatan-container'),
        riwayatContainer = $('#riwayat-container'),
        disposisiFormContainer = $('#disposisi-form-container'),
        disposisiInfoContainer = $('#disposisi-info-container'),
        formDisposisiSuratMasuk = $('#formDisposisiSuratMasuk'),
        verifParafContainer = $('#verifParafContainer'),
        aksiListEl = $('#aksiListEl'),
        verifBtn = $('.verif-btn', viewSuratMasukModalEl),
        rejectBtn = $('.reject-btn', viewSuratMasukModalEl),
        redisposisiBtn = $('.redisposisi-btn', viewSuratMasukModalEl),
        viewData = null,
        verifProgressChart = null,
        disposisiUserIdEl = null,
        tindakanSegeraEl = null,
        tindakanBiasaEl = null,
        aksiList = <?php echo json_encode($aksiList) ?>

    var $textHeadingColor = '#5e5873'
    var $white = '#fff'

    $(window).on('load', function() {
        fileInfoElContainer.hide()
        fileElContainer.hide()

        $.each(aksiList, function(i, val) {
            aksiListEl.append(`
            <div class="form-check form-check-inline mt-0">
                <input class="form-check-input" type="checkbox" name="tindakan_segera" id="aksi_${val.id}" value="${val.id}">
                <label class="form-check-label" for="aksi_${val.id}">${val.nama}</label>
            </div>`)
        })

        $('.js-signature', formDisposisiSuratMasuk).jqSignature({
            lineColor: '#000000',
            width: 200,
            height: 100
        })

        $('.js-signature-blank', formDisposisiSuratMasuk).jqSignature({
            lineColor: '#000000',
            width: 200,
            height: 100
        })

        $('.js-signature-clear-btn', formDisposisiSuratMasuk).on('click', function(e) {
            $('.js-signature', formDisposisiSuratMasuk).jqSignature('clearCanvas')
        })

        $('.js-signature', verifParafContainer).jqSignature({
            lineColor: '#000000',
            width: 200,
            height: 100
        })

        $('.js-signature-blank', verifParafContainer).jqSignature({
            lineColor: '#000000',
            width: 200,
            height: 100
        })

        $('.js-signature-clear-btn', verifParafContainer).on('click', function(e) {
            $('.js-signature', verifParafContainer).jqSignature('clearCanvas')
        })

        viewSuratMasukModalEl.on('shown.bs.modal', function(e) {
            $('.js-signature', formDisposisiSuratMasuk).jqSignature('clearCanvas')
            $('.js-signature', verifParafContainer).jqSignature('clearCanvas')

            $('#catatan', viewSuratMasukModalEl).val('')

            window.toastr.clear()

            verifBtn.prop('disabled', false)
            verifBtn.find('.spinner').hide()

            rejectBtn.prop('disabled', false)
            rejectBtn.find('.spinner').hide()
            rejectBtn.hide()

            redisposisiBtn.prop('disabled', false)
            redisposisiBtn.find('.spinner').hide()
            redisposisiBtn.hide()

            disposisiFormContainer.hide()
            disposisiInfoContainer.hide()
            verifParafContainer.hide()

            $('#tglSuratEl', viewSuratMasukModalEl).html(moment(viewData.tgl_surat).format('DD MMM YYYY'))
            $('#tglTerimaEl', viewSuratMasukModalEl).html(moment(viewData.tgl_terima).format('DD MMM YYYY'))
            $('#pengirimEl', viewSuratMasukModalEl).html(viewData.pengirim)
            $('#perihalEl', viewSuratMasukModalEl).html(viewData.perihal)
            $('#tujuanEl', viewSuratMasukModalEl).html(viewData.tujuan)
            $('#kerahasiaanEl', viewSuratMasukModalEl).html(viewData.kerahasiaan)
            // $('#catatanEl', viewSuratMasukModalEl).html(viewData.catatan)

            $('#disposisiUserEl', viewSuratMasukModalEl).html(viewData.alamat_aksi)
            $('#tglDisposisiEl', viewSuratMasukModalEl).html(moment(viewData.tgl_disposisi).format('DD MMM YYYY'))
            $('#tindakanSegeraInfoEl', viewSuratMasukModalEl).html(viewData.aksi)
            $('#catatanDisposisiInfoEl', viewSuratMasukModalEl).html(viewData.catatan_disposisi)
            // $('#tindakanBiasaInfoEl', viewSuratMasukModalEl).html(viewData.tindakan_biasa)

            if (viewData.status_verifikasi == 'TERVERIFIKASI') {
                verifBtn.hide()
                catatanContainer.hide()

                disposisiFormContainer.show()
                $('.card-title', disposisiFormContainer).html('Disposisi')
                renderFormDisposisi()

                setTimeout(function() {
                    formDisposisiSuratMasuk.trigger('reset')
                }, 1000)
            } else if (viewData.status_verifikasi == 'DISPOSISI') {
                redisposisiBtn.show()
                verifBtn.hide()
                catatanContainer.hide()

                disposisiFormContainer.hide()
                disposisiInfoContainer.show()
            } else {
                verifBtn.show()
                catatanContainer.show()

                if (viewData.level_verifikasi == 0 && viewData.status_verifikasi == 'BARU') {
                    $('#verif-btn-txt', viewSuratMasukModalEl).html('AJUKAN VERIFIKASI')
                } else {
                    rejectBtn.show()
                    verifParafContainer.show()
                    $('#verif-btn-txt', viewSuratMasukModalEl).html('VERIFIKASI')
                }

                if (viewData.status_verifikasi == 'DITOLAK') {
                    if (viewData.level_verifikasi == 0) {
                        verifParafContainer.hide()
                    }

                    rejectBtn.hide()
                    $('#verif-btn-txt', viewSuratMasukModalEl).html('AJUKAN VERIFIKASI')
                }
            }

            if (viewData.file_extension == 'pdf') {
                fileElContainer.show()
                $('#fileEl', viewSuratMasukModalEl).attr('src', `<?php echo base_url(); ?>app-assets/upload/suratMasuk/${viewData.filename}`)
            } else {
                fileInfoElContainer.show()
                $('#fileDownloadEl', viewSuratMasukModalEl).attr('href', `<?php echo base_url(); ?>app-assets/upload/suratMasuk/${viewData.filename}`)
            }

            $.each(viewData.log, function(i, val) {
                var catatan = ''
                if (val.log_data !== null) {
                    var logData = JSON.parse(val.log_data)
                    if (logData.status_verifikasi != 'Disposisi' && logData.catatan !== undefined && logData.catatan !== null && logData.catatan != '') {
                        catatan = `<div>Catatan<pre class="p-1 mt-50 fs-6">${logData.catatan}</pre></div>`
                    } else if (logData.status_verifikasi = 'Disposisi' && logData.catatan_disposisi !== undefined && logData.catatan_disposisi !== null && logData.catatan_disposisi != '') {
                        catatan = `<div>Catatan<pre class="p-1 mt-50 fs-6">${logData.catatan_disposisi}</pre></div>`
                    }
                }

                var pointClass = val.log_action == 'Penolakan Verifikasi Surat Masuk' ? 'timeline-point-danger' : ''
                $('#riwayatEl', viewSuratMasukModalEl).append(`
                    <li class="timeline-item">
                        <span class="timeline-point timeline-point-indicator ${pointClass}"></span>
                        <div class="timeline-event">
                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                <h6>${val.log_action}</h6>
                                <span class="timeline-event-time">${moment(val.log_created).format('DD MMM YYYY, HH:mm:ss')}</span>
                            </div>
                            <div class="mb-50">User : ${val.log_user}</div>
                            ${catatan}
                        </div>
                    </li>
                `)
            })

            renderVerifProgressChart()
            new PerfectScrollbar(riwayatContainer[0])
        })

        viewSuratMasukModalEl.on('hidden.bs.modal', function(e) {
            $('#catatan', viewSuratMasukModalEl).val('')
            $('#riwayatEl', viewSuratMasukModalEl).html('')
            $('#fileEl', viewSuratMasukModalEl).attr('src', '')
            fileInfoElContainer.hide()
            fileElContainer.hide()
            $('#riwayatEl', viewSuratMasukModalEl).empty()

            if (verifProgressChart !== null) {
                verifProgressChart.destroy()
            }

            formDisposisiSuratMasuk.trigger('reset')
            formDisposisiSuratMasuk.validate().resetForm()

            if (tindakanSegeraEl !== null) {
                tindakanSegeraEl.val(null).trigger('change')
            }

            if (tindakanBiasaEl !== null) {
                tindakanBiasaEl.val(null).trigger('change')
            }

            $('#catatan_disposisi', formDisposisiSuratMasuk).val('')
            $('.js-signature', formDisposisiSuratMasuk).jqSignature('clearCanvas')

            if (disposisiUserIdEl !== null) {
                disposisiUserIdEl.val(null).trigger('change')
            }
        })

        $('.close-btn', viewSuratMasukModalEl).on('click', function() {
            viewSuratMasukModalEl.modal('hide')
        })

        $('.verif-btn', viewSuratMasukModalEl).on('click', function(e) {

            var paraf = $('.js-signature', verifParafContainer).jqSignature('getDataURL')
            var blank = $('.js-signature-blank', verifParafContainer).jqSignature('getDataURL')

            if (paraf == blank && viewData.level_verifikasi != 0) {
                toastr['error']('Harap paraf terlebih dahulu', 'ERROR', {
                    closeButton: true,
                    tapToDismiss: false
                })
                return false
            } else {

                var title = viewData.level_verifikasi == 0 ? 'Proses Pengajuan Verifikasi' : 'Verifikasi'

                Swal.fire({
                    title: `${title} Data Surat Masuk`,
                    icon: 'question',
                    showCancelButton: true,
                    customClass: {
                        confirmButton: 'btn btn-lg btn-outline-success me-1',
                        cancelButton: 'btn btn-lg btn-outline-secondary'
                    },
                    confirmButtonText: 'PROSES',
                    cancelButtonText: 'BATAL',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {

                        verifBtn.prop('disabled', true)
                        verifBtn.find('.spinner').show()

                        $.post(`<?php echo base_url(); ?>suratMasuk/verifikasi`, {
                                id: viewData.id,
                                catatan: $('#catatan', viewSuratMasukModalEl).val(),
                                paraf: paraf
                            },
                            function(resp) {
                                if (resp.error) {
                                    toastr['error'](resp.message, 'ERROR', {
                                        closeButton: true,
                                        tapToDismiss: false
                                    })
                                } else {
                                    gridSuratMasuk.ajax.reload()

                                    viewSuratMasukModalEl.modal('hide')

                                    toastr['success'](resp.message, 'SUKSES', {
                                        closeButton: true,
                                        tapToDismiss: false
                                    })
                                }

                                verifBtn.prop('disabled', false)
                                verifBtn.find('.spinner').hide()
                                $('.js-signature', verifParafContainer).jqSignature('clearCanvas')

                            }, 'json')

                        return false
                    }
                })
            }
        })

        $('.reject-btn', viewSuratMasukModalEl).on('click', function(e) {
            Swal.fire({
                title: 'Alasan Penolakan',
                input: 'textarea',
                customClass: {
                    confirmButton: 'btn btn-lg btn-outline-success me-1',
                    cancelButton: 'btn btn-lg btn-outline-secondary'
                },
                confirmButtonText: 'PROSES',
                cancelButtonText: 'BATAL',
                buttonsStyling: false,
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var id = document.getElementsByClassName('swal2-textarea')
                    var value = id[0].value
                    if (value.length != 0) {
                    } else {
                        Swal.showValidationMessage('Alasan penolakan belum diisi!')
                    }
                }
            }).then(result => {
                if (result.isConfirmed) {
                    $.post(`<?php echo base_url(); ?>suratMasuk/reject`, {
                            id: viewData.id,
                            catatan: result.value
                        },
                        function(resp) {
                            if (resp.error) {
                                toastr['error'](resp.message, 'ERROR', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            } else {
                                viewSuratMasukModalEl.modal('hide')

                                gridSuratMasuk.ajax.reload()

                                toastr['success'](resp.message, 'SUKSES', {
                                    closeButton: true,
                                    tapToDismiss: false
                                })
                            }
                        }, 'json')
                }
            })
        })

        $('.disposisi-btn', viewSuratMasukModalEl).on('click', function(e) {
            viewSuratMasukModalEl.modal('hide')
        })

        $('.redisposisi-btn', viewSuratMasukModalEl).on('click', function(e) {
            disposisiFormContainer.show()
            $('.card-title', disposisiFormContainer).html('Redisposisi')
            renderFormDisposisi()

            setTimeout(function() {
                formDisposisiSuratMasuk.trigger('reset')
            }, 1000)
        })
    })

    function viewSuratMasukInit(idSuratMasuk = null) {
        if (idSuratMasuk) {
            $.post("<?php echo base_url() ?>suratMasuk/getById", {
                    id: idSuratMasuk,
                    modeView: true
                },
                function(resp) {
                    if (resp.error) {
                        toastr['error'](resp.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })

                        return false
                    }
                    viewSuratMasukRender(resp.data.id, resp.data)
                }, 'json')
        } else {
            viewSuratMasukRender()
        }
    }

    function viewSuratMasukRender(idSuratMasuk = null, formData = null) {

        viewData = formData

        $('#viewSuratMasukModal').modal().modal('show')
    }

    function renderVerifProgressChart() {
        verifProgressChart = new ApexCharts(document.querySelector('#verif-progress-chart'), {
            chart: {
                height: 300,
                type: 'radialBar'
            },
            plotOptions: {
                radialBar: {
                    size: 150,
                    offsetY: -30,
                    startAngle: -150,
                    endAngle: 150,
                    hollow: {
                        size: '65%'
                    },
                    track: {
                        background: $white,
                        strokeWidth: '100%'
                    },
                    dataLabels: {
                        name: {
                            offsetY: -5,
                            color: $textHeadingColor,
                            fontSize: '1rem'
                        },
                        value: {
                            offsetY: 15,
                            color: $textHeadingColor,
                            fontSize: '1.714rem'
                        }
                    }
                }
            },
            colors: [window.colors.solid.danger],
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: [window.colors.solid.primary],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            stroke: {
                dashArray: 8
            },
            series: [viewData.verif_progress],
            labels: ['Status Verifikasi']
        })
        verifProgressChart.render()
    }

    function renderFormDisposisi() {

        $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).prop('disabled', false)
        $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).find('.spinner').hide()

        //disposisiUserIdEl = $('#disposisi_user_id', formDisposisiSuratMasuk)
        //disposisiUserIdEl.wrap('<div class="position-relative"></div>')
        //                 .prepend('<option selected=""></option>').select2({
        //    placeholder: 'Pilih Disposisi Kepada',
        //    ajax: {
        //        url: '<?php //echo base_url(); ?>//user/get_select2_list',
        //        delay: 250,
        //        data: function(params) {
        //            return {
        //                q: params.term,
        //                page: params.page
        //            }
        //        },
        //        processResults: function(data, params) {
        //            params.page = params.page || 1
        //            return {
        //                results: data.items,
        //                pagination: {
        //                    more: (params.page * 10) < data.total
        //                    // more: data.total
        //                }
        //            }
        //        },
        //        cache: true,
        //        dataType: 'json',
        //        type: 'GET'
        //    },
        //    // minimumResultsForSearch: -1,
        //    allowClear: true,
        //    dropdownAutoWidth: true,
        //    dropdownParent: disposisiUserIdEl.parent(),
        //    width: '100%'
        //}).on('change.select2', function(e) {
        //    formDisposisiSuratMasuk.validate().resetForm()
        //})

        // tindakanSegeraEl = $('#tindakan_segera', formDisposisiSuratMasuk)
        // tindakanSegeraEl
        //     .wrap('<div class="position-relative"></div>')
        //     .prepend('<option selected=""></option>')
        //     .select2({
        //         placeholder: 'Pilih Tindakan Segera',
        //         data: [
        //             { id: 'Edarkan', text: 'Edarkan' },
        //             { id: 'Selesaikan', text: 'Selesaikan' },
        //             { id: 'Sesuai Catatan', text: 'Sesuai Catatan' },
        //             { id: 'Setuju', text: 'Setuju' },
        //             { id: 'Teliti & Pendapat', text: 'Teliti & Pendapat' },
        //             { id: 'Tindak Lanjuti', text: 'Tindak Lanjuti' },
        //             { id: 'Tolak', text: 'Tolak' },
        //             { id: 'Untuk Diketahui', text: 'Untuk Diketahui' },
        //             { id: 'Untuk Diperhatikan', text: 'Untuk Diperhatikan' }
        //         ],
        //         // minimumResultsForSearch: -1,
        //         allowClear: true,
        //         dropdownAutoWidth: true,
        //         dropdownParent: tindakanSegeraEl.parent(),
        //         width: '100%'
        //     }).on('change.select2', function(e) {
        //     formDisposisiSuratMasuk.validate().resetForm()
        // })

        // tindakanBiasaEl = $('#tindakan_biasa', formDisposisiSuratMasuk)
        // tindakanBiasaEl
        //     .wrap('<div class="position-relative"></div>')
        //     .prepend('<option selected=""></option>')
        //     .select2({
        //         placeholder: 'Pilih Tindakan Biasa',
        //         data: [
        //             { id: 'Jawab', text: 'Jawab' },
        //             { id: 'Perbaiki', text: 'Perbaiki' },
        //             { id: 'Bicarakan dengan saya', text: 'Bicarakan dengan saya' },
        //             { id: 'Bicarakan bersama', text: 'Bicarakan bersama' },
        //             { id: 'Harap dihadiri/diwakilkan', text: 'Harap dihadiri/diwakilkan' }
        //         ],
        //         // minimumResultsForSearch: -1,
        //         allowClear: true,
        //         dropdownAutoWidth: true,
        //         dropdownParent: tindakanBiasaEl.parent(),
        //         width: '100%'
        //     }).on('change.select2', function(e) {
        //     formDisposisiSuratMasuk.validate().resetForm()
        // })

        var manifest = {
            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id: <?php echo logged('user_id'); ?>,
                id: viewData.id,
                // disposisi_user_id: null,
                alamat_aksi: null,
                tindakan_segera: null,
                // tindakan_biasa: null,
                catatan_disposisi: null
            },

            // Init function
            init: function($form, runtime) {
                $form.then(function() { // Fade when start succeds
                    $('input:visible:enabled:first', formDisposisiSuratMasuk).focus()
                    formDisposisiSuratMasuk.validate({
                        rules: {
                            alamat_aksi: {
                                required: true
                            }
                            // disposisi_user_id: {
                            //     required: true
                            // }
                            // tindakan_segera: {
                            //     required: true
                            // },
                            // tindakan_biasa: {
                            //     required: true
                            // }
                        },
                        messages: {
                            alamat_aksi: 'Alamat aksi belum dipilih'
                            // disposisi_user_id: 'Disposisi kepada harus diisi'
                            // tindakan_segera: 'Tindakan segera harus diisi',
                            // tindakan_biasa: 'Tindakan biasa harus diisi'
                        }
                    })
                })
            },

            // Bindings
            ui: {
                '#id': {
                    bind: 'id'
                },
                // '#disposisi_user_id': {
                //     bind: 'disposisi_user_id'
                // },
                'input[name=\'alamat_aksi\']': {
                    bind: 'alamat_aksi'
                },
                'input[name=\'tindakan_segera\']': {
                    bind: 'tindakan_segera'
                },
                // '#tindakan_segera': {
                //     bind: 'tindakan_segera'
                // },
                // '#tindakan_biasa': {
                //     bind: 'tindakan_biasa'
                // },
                '#catatan_disposisi': {
                    bind: 'catatan_disposisi'
                }
            }
        }

        // Init $.my over DOM node
        formDisposisiSuratMasuk.my(manifest)

        formDisposisiSuratMasuk.submit(function() {
            if (formDisposisiSuratMasuk.valid()) {
                $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).find('.spinner').show()
            }
        })
    }

    function formDisposisiSuratMasukSubmit() {
        if (formDisposisiSuratMasuk.valid()) {
            var data = formDisposisiSuratMasuk.my('data')
            console.log('data', data)

            data.paraf = $('.js-signature', formDisposisiSuratMasuk).jqSignature('getDataURL')
            var blank = $('.js-signature-blank', formDisposisiSuratMasuk).jqSignature('getDataURL')

            if (data.paraf == blank) {
                toastr['error']('Harap paraf terlebih dahulu', 'ERROR', {
                    closeButton: true,
                    tapToDismiss: false
                })

                setTimeout(function() {
                    $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).prop('disabled', false)
                    $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).find('.spinner').hide()
                }, 1000)

                return false
            } else {
                data.redisposisi = redisposisiBtn.is(":visible")

                $.post("<?php echo base_url(); ?>suratMasuk/disposisi", data,
                    function(resp) {
                        if (resp.error) {
                            toastr['error'](resp.message, 'ERROR', {
                                closeButton: true,
                                tapToDismiss: false
                            })
                        } else {
                            gridSuratMasuk.ajax.reload()

                            toastr['success'](' ', resp.message, {
                                closeButton: false,
                                tapToDismiss: true
                            })

                            formDisposisiSuratMasuk.trigger('reset')
                            formDisposisiSuratMasuk.validate().resetForm()
                            $('input:visible:enabled:first', formDisposisiSuratMasuk).focus()

                            // disposisiUserIdEl.val(null).trigger('change')
                            // tindakanSegeraEl.val(null).trigger('change')
                            // tindakanBiasaEl.val(null).trigger('change')
                            $('#catatan_disposisi', formDisposisiSuratMasuk).val('')
                            $('.js-signature', formDisposisiSuratMasuk).jqSignature('clearCanvas')

                            viewSuratMasukModalEl.modal('hide')
                        }

                        $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).prop('disabled', false)
                        $('button[type=submit], input[type=submit]', formDisposisiSuratMasuk).find('.spinner').hide()

                        return false

                    }, 'json')
            }

            return false
        }

        return false
    }
</script>
