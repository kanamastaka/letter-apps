<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

<form id="formUser" class="row gy-1 gx-2 mt-75" onsubmit="return formUserSubmit()">
</form>

<template id="formUserTpl">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>"/>
    <input type="hidden" name="user_id_active" value="<?php echo logged('user_id'); ?>"/>
    <input type="hidden" name="user_id" id="user_id"/>

    <div class="row gy-0 gx-0 mt-75">

        <div class="col-12">
            <div class="row gy-1 gx-2">

                <!-- <div class="col-12">
                    <label class="form-label" for="org_id">Organization</label>
                    <select class="select2 select2-data-ajax form-select" id="org_id" name="org_id"></select>
                </div> -->

                <div class="col-md-12">
                    <label class="form-label" for="user_login">Username</label>
                    <input type="text" id="user_login" name="user_login" class="form-control text-lowercase"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="user_password">Password</label>
                    <input type="password" id="user_password" name="user_password" class="form-control"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="user_password_confirm">Repeat Password</label>
                    <input type="password" id="user_password_confirm" name="user_password_confirm"
                           class="form-control"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="user_name">Full Name</label>
                    <input type="text" id="user_name" name="user_name" class="form-control text-uppercase"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="user_login">Email</label>
                    <input type="text" id="email" name="email" class="form-control text-lowercase"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="user_login">NRP</label>
                    <input type="text" id="user_nrp" name="user_nrp" class="form-control text-uppercase"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="user_login">Mobile</label>
                    <input type="text" id="user_mobile" name="user_mobile" class="form-control text-lowercase"/>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="verifikator_level">Surat Masuk Verifikator Level</label>
                    <select class="select2 select2-data-ajax form-select" id="verifikator_level"
                            name="verifikator_level"></select>
                </div>

                <div class="col-md-6">
                    <label class="form-label" for="jabatan_id">Jabatan</label>
                    <select class="select2 select2-data-ajax form-select" id="jabatan_id" name="jabatan_id"></select>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-50">
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                        Save
                    </button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="cropper-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title" id="modalLabel">&nbsp;</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-sm-5 mx-50 pb-5">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-8">
                                <!--  default image where we will set the src via jquery-->
                                <img id="image">
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
                </div>
            </div>
        </div>
    </div>


</template>

<script>
    var formUser = $('#formUser'),
        verifikatorLevelEl = null,
        jabatanEl = null,
        formUserLoading = false,
        formUserMode = 'ADD'

    $(window).on('load', function() {

        $('#formUserModal').on('shown.bs.modal', function(e) {

            $('button[type=submit], input[type=submit]', formUser).find('.spinner').hide()

            verifikatorLevelEl = $('#verifikator_level')
            verifikatorLevelEl.wrap('<div class="position-relative"></div>')
                              .prepend('<option selected=""></option>')
                              .select2({
                                  placeholder: 'PILIH LEVEL VERIFIKASI',
                                  data: <?php echo json_encode($dataLevelVerifikasi) ?>,
                                  // minimumResultsForSearch: -1,
                                  allowClear: true,
                                  dropdownAutoWidth: true,
                                  dropdownParent: verifikatorLevelEl.parent(),
                                  width: '100%'
                              }).on('change.select2', function(e) {
                var val = verifikatorLevelEl.select2('val')
                if (val) {
                    formUser.validate().resetForm()
                }
                //
            })

            jabatanEl = $('#jabatan_id', formUser)
            jabatanEl.wrap('<div class="position-relative"></div>')
                     .prepend('<option selected=""></option>').select2({
                placeholder: 'Pilih Jabatan',
                ajax: {
                    url: '<?php echo base_url(); ?>jabatan/get_select2_list',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                            page: params.page
                        }
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 10) < data.total
                                // more: data.total
                            }
                        }
                    },
                    cache: true,
                    dataType: 'json',
                    type: 'GET'
                },
                // minimumResultsForSearch: -1,
                allowClear: true,
                dropdownAutoWidth: true,
                dropdownParent: jabatanEl.parent(),
                width: '100%'
            }).on('change.select2', function(e) {
                formUser.validate().resetForm()
            })

            if (formUser.my('data').verifikator_level) {
                verifikatorLevelEl.val(formUser.my('data').verifikator_level).trigger('change')
            }

            if (formUser.my('data').jabatan_id) {
                var newOption = new Option(formUser.my('data').jabatan.text, formUser.my('data').jabatan.id, false, false)
                jabatanEl.append(newOption).val(formUser.my('data').jabatan_id).trigger('change')
            }
        })

        $('#formUserModal').on('hidden.bs.modal', function(e) {
            e.preventDefault()
            e.stopPropagation()

            // verifikatorLevelEl.select2('destroy')
            $('button[type=submit], input[type=submit]', formUser).find('.spinner').hide()
            formUser.validate().resetForm()
            formUser.my('remove')
        })

        formUser.submit(function() {
            if (formUser.valid()) {
                $('button[type=submit], input[type=submit]', formUser).prop('disabled', true)
                $('button[type=submit], input[type=submit]', formUser).find('.spinner').show()
            }
        })
    })

    function formUserInit(idUser = null) {

        $('#formUserModal').modal({
            backdrop: 'static',
            keyboard: false
        }).modal('show')

        if (idUser) {
            $.post("<?php echo base_url() ?>user/getById", {
                    id: idUser
                },
                function(r) {
                    if (r.error) {
                        return false
                    }
                    formUserRender(r.data.user_id, r.data)
                }, 'json')
        } else {
            formUserRender()
        }
    }

    function formUserRender(idUser = null, formData = null) {
        formUserMode = idUser ? 'EDIT' : 'ADD'
        var title = idUser ? 'Edit' : 'Add'

        $('#formUserModalTitle').html(title + ' <?php echo $page->title ?>')

        // Manifest
        var manifest = {

            //Default data
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                user_id_active: <?php echo logged('user_id'); ?>,
                user_id: null,
                user_login: null,
                user_name: null,
                user_password: null,
                user_password_confirm: null,
                verifikator_level: null,
                jabatan_id: null,
                user_nrp: null,
                email: null,
                user_mobile: null
            },

            // Init function
            init: function($form, runtime) {
                $form.empty().html($('#formUserTpl').html())

                $form.then(function() { // Fade when start succeds
                    $('input:visible:enabled:first', formUser).focus()

                    var formUserValRules = {
                        user_name: {
                            required: true
                        },
                        user_login: {
                            required: true
                        },
                        email: {
                            required: true
                        }
                    }

                    formUser.validate({
                        rules: formUserValRules,
                        messages: {
                            user_name: 'Full name harus diisi',
                            user_login: 'Username harus diisi',
                            email: 'Email harus diisi',
                            user_password: 'Password harus diisi',
                            user_password_confirm: 'password tidak cocok!'
                        }
                    })

                    if (idUser) {
                        $('#user_password').rules('remove', 'required')
                        $('#user_password_confirm').rules('remove', 'required')
                        // $("#user_password_confirm").rules("remove", 'equalTo');
                    } else {
                        $('#user_password').rules('add', {
                            required: true,
                            minlength: 8
                        })
                        $('#user_password_confirm').rules('add', {
                            required: true,
                            minlength: 8,
                            equalTo: '#user_password'
                        })
                    }
                })

                if (idUser != null) {
                    $('#formUserModalTitle').html('Edit <?php echo $page->title ?> Data')
                }
            },

            // Bindings
            ui: {
                '#user_id': {
                    bind: 'user_id'
                },
                '#user_login': {
                    bind: 'user_login'
                },
                '#user_name': {
                    bind: 'user_name'
                },
                '#user_password': {
                    bind: 'user_password'
                },
                '#user_password_confirm': {
                    bind: 'user_password_confirm'
                },
                '#verifikator_level': {
                    bind: 'verifikator_level'
                },
                '#jabatan_id': {
                    bind: 'jabatan_id'
                },
                '#user_nrp': {
                    bind: 'user_nrp'
                },
                '#email': {
                    bind: 'email'
                },
                '#user_mobile': {
                    bind: 'user_mobile'
                }
            }
        }

        // Init $.my over DOM node
        formUser.my(manifest, formData)

        // if (formData.org_id) {
        //     verifikatorLevelEl.select2().select2(formData.org_id)
        // }
    }

    function formUserSubmit() {

        if (formUser.valid()) {

            var data = formUser.my('data')

            $.post("<?php echo base_url(); ?>user/process_save", data,
                function(r) {
                    if (r.error) {
                        toastr['error'](r.message, 'ERROR', {
                            closeButton: true,
                            tapToDismiss: false
                        })
                    } else {
                        gridUser.ajax.reload()
                        toastr['success'](' ', `${formUserMode} DATA SUCCESS`, {
                            closeButton: false,
                            tapToDismiss: true
                        })

                        if (formUserMode == 'ADD') {

                            formUser.trigger('reset')
                            formUser.validate().resetForm()
                            $('input:visible:enabled:first', formUser).focus()
                            verifikatorLevelEl.val(null).trigger('change')
                            jabatanEl.val(null).trigger('change')
                            // verifikatorLevelEl.select2('focus')
                        } else {
                            $('#formUserModal').modal('hide')
                        }
                    }

                    $('button[type=submit], input[type=submit]', formUser).prop('disabled', false)
                    $('button[type=submit], input[type=submit]', formUser).find('.spinner').hide()

                    return false

                }, 'json')

            return false
        }

        return false
    }
</script>
