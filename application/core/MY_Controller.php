<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\CodeigniterAdapter;

class MY_Controller extends CI_Controller {

    public $page_data;

    /**
     * Extends by most of controllers not all controllers
     */

    public function __construct()
    {

        parent::__construct();

        Carbon::setLocale('id');

        if (!empty($this->db->username) && !empty($this->db->hostname) && !empty($this->db->database)) {
        }
        else {
            $this->users_model->logout();
            die('Database is not configured');
        }

        date_default_timezone_set(setting('timezone'));

        $this->config->set_item('language', getUserlang());

        $this->lang->load([
            'basic',

        ], getUserlang());

        if (!is_logged()) {
            redirect('osintauth', 'refresh');
        }

        $this->page_data['url'] = (object) [
            'assets' => assets_url() . '/'
        ];

        $this->page_data['app'] = (object) [
            'site_title' => setting('company_name')
        ];

        $this->page_data['page'] = (object) [
            'title'   => 'Dashboard',
            'menu'    => 'dashboard',
            'submenu' => '',
        ];

        $this->page_data['suratMasukNotifCount'] = $this->getSuratMasukNotifCount();
        $this->page_data['suratKeluarNotifCount'] = $this->getSuratKeluarNotifCount();
    }

    public function getSuratMasukNotifCount()
    {
        $condition[] = "notification.user_id = " . logged('user_id');
        $condition[] = "notification.modul_type = 'Surat Masuk'";
        $condition[] = "notification.read_at IS NULL";
        $w = implode(' AND ', $condition);

        $sql = " SELECT COUNT(DISTINCT user_id, modul_type, modul_id) AS counter FROM notification WHERE $w";

        $query = $this->db->query($sql);
        return $query->result() ? intval($query->result()[0]->counter) : 0;
    }

    public function getSuratKeluarNotifCount()
    {
        $condition[] = "notification.user_id = " . logged('user_id');
        $condition[] = "notification.modul_type = 'Surat Keluar'";
        $condition[] = "notification.read_at IS NULL";
        $w = implode(' AND ', $condition);

        $sql = " SELECT COUNT(DISTINCT user_id, modul_type, modul_id) AS counter FROM notification WHERE $w";

        $query = $this->db->query($sql);
        return $query->result() ? intval($query->result()[0]->counter) : 0;
    }

    public function change_language()
    {
        // die(var_dump('test_func'));
    }

    public function getDatatable($select, $request = null)
    {
        $dt = new Datatables(new CodeigniterAdapter, $request);

        return $dt->query($select);
    }

}

/* End of file My_Controller.php */
/* Location: ./application/core/My_Controller.php */
