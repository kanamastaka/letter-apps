<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\models\MyEloquent_model;

class Aksi_model extends MyEloquent_model {

    // use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'aksi';

    protected $fillable = [
        'nama',
    ];
}
