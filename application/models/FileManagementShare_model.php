<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class FileManagementShare_model extends Eloquent {

    // use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'file_management_share';
    public $timestamps = false;

    protected $fillable = [
        'file_management_id',
        'user_id',
    ];
}
