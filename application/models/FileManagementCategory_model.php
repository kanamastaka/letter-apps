<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\models\MyEloquent_model;

class FileManagementCategory_model extends MyEloquent_model {

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'file_management_category';
    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'nama',
        'deleted_by',
    ];

    protected $casts = [
        // 'user_modified' => 'date:Y-m-d H:i:s',
    ];
}
