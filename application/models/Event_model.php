<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\models\MyEloquent_model;

class Event_model extends MyEloquent_model {

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'event';
    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'allday',
        'category',
        'location',
        'description',
        'url',
    ];

    protected $casts = [
    ];
}
