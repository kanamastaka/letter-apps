<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// use Illuminate\Database\Eloquent\Model as Eloquent;
use App\models\MyEloquent_model;

class SuratMasuk_model extends MyEloquent_model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'surat_masuk';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    // public $timestamps = false;
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;

    protected $fillable = [
        'no_agenda',
        'tgl_surat',
        'tgl_terima',
        'no_surat',
        'pengirim',
        'perihal',
        'tujuan',
        'kerahasiaan',
        'level_verifikasi',
        'status_verifikasi',
        'filename',
        'catatan',
        'catatan_disposisi',
        'tgl_disposisi',
        'disposisi_user_id',
        'tindakan_segera',
        'tindakan_biasa',
        'paraf1',
        'paraf2',
        'paraf3',
        'paraf4',
        'paraf5',
    ];

    protected $casts = [
        // 'user_modified' => 'date:Y-m-d H:i:s',
    ];

    public function aksi()
    {
        return $this->hasMany(SuratMasukAksi_model::class, 'surat_masuk_id', 'id');
    }

    public function alamatAksi()
    {
        return $this->hasMany(SuratMasukAlamatAksi_model::class, 'surat_masuk_id', 'id');
    }
}
