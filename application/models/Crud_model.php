<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Crud_model extends CI_Model {
    function __construct () {  parent::__construct(); }
    function create_guid(){if (function_exists('com_create_guid') === true){return trim(com_create_guid(), '{}');} return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));}

    function checklogin($u,$p)
    {
        $this->db->where("user_login",$u);
        $this->db->where("user_password",SHA1($p));
        $q = $this->db->get("User");
		$row = $q->row();
        return $row;
    }

    function checkloginByEmail($email)
    {
        $this->db->where("email",$email);
        $q = $this->db->get("User");
        $row = $q->row();
        return $row;
    }

	function getuploadimage()
	{
		$this->db->where("upload_status",1);
		$this->db->where("upload_approval_status",1);
		$this->db->where("upload_user_id",$this->sess->user_id);
		$q = $this->db->get("upload");
		$data = $q->result();
		return $data;
	}

    function selectdata($table,$fieldstatus)
    {
        $this->db->where($fieldstatus,$this->config->item("active_status"));
        if($table == $this->config->item("tbl_log"))
        {
			$this->db->order_by("log_id","desc");
			$this->db->limit(10);
			if($this->sess->user_type != 1)
			{
				$this->db->where("log_user",$this->sess->user_name);
			}
		}
		if($table == $this->config->item("tbl_user"))
        {
			if($this->sess->user_type == 2)
			{
				$this->db->where("user_id",$this->sess->user_id);
			}
        }
        if($table == "assets_room")
        {
			$this->db->order_by("room_tower_id","asc");
            $this->db->order_by("room_name","asc");
        }
        if($table == "assets_internet_provider")
        {
			$this->db->order_by("internet_provider_name","asc");
        }

        $q = $this->db->get($table);
        $rows = $q->result();
        return $rows;
    }
    function selectalldata($table)
    {
        if($table == $this->config->item("tbl_log"))
        {
			$this->db->order_by("log_id","desc");
			$this->db->limit(5);
			if($this->sess->user_type != 1)
			{
				$this->db->where("log_user",$this->sess->user_name);
			}
		}
		if($table == $this->config->item("tbl_user"))
        {
			if($this->sess->user_type == 2)
			{
				$this->db->where("user_id",$this->sess->user_id);
			}
		}
        $q = $this->db->get($table);
        $rows = $q->result();
        return $rows;
    }
    function selectdata_byid($table,$fid,$fieldkey)
    {
        $this->db->where($fieldkey,$fid);
        $q = $this->db->get($table);
        $row = $q->row();
        return $row;
    }
    function updatedata($table,$data,$id,$fieldid)
    {
		$status = false;
		$this->db->where($fieldid,$id);
		if($this->db->update($table,$data)){$status = true;}
		return $status;
	}
    function check_duplicate($table,$fieldvalue,$value)
    {
        $status = false;
        $this->db->where($fieldvalue,$value);
        $q = $this->db->get($table);
        $row = $q->row();
        $total = $q->num_rows();
        if($total > 0){$status = true;}
        return $status;
    }
    function deletedata($table,$fid,$id,$data)
	{
		$status = false;
		$this->db->where($fid,$id);
		if($this->db->update($table,$data)){$status = true;}
		return $status;
	}
    function insertdata($table,$data)
    {
        $status = false;
        if($this->db->insert($table,$data))
        {
            if($table == "UserAccount")
            {
                //$userid = $this->db->insert_id();
                $userid = $data["RecordID"];
				$sql = "UPDATE UserAccount SET UserPassword = SHA1('".$data['UserPassword']."') WHERE RecordID = '".$userid."'";
				$this->db->query($sql);
				$this->db->cache_delete_all();
				$status = true;
            }
            else
            {
				$status = true;
			}
        }
        return $status;
    }
    function check_ip_backlist($ip)
    {
        $this->db->where("ip_blacklist_status",1);
        $this->db->where("ip_blacklist_ip",$ip);
        $q = $this->db->get("c_ip_blacklist");
        if($q->num_rows()>0)
        {
            $status = true;
        }
        else
        {
            $status = false;
        }
        return $status;
    }


    function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
        }

        if(isset($this->sess->RecordID))
        {
            return date("His").$token.date("ydm").$this->sess->RecordID;
        }
        else
        {
            return date("His").$token.date("ydm");
        }

    }

    function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do
        {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        }
        while ($rnd > $range);
        return $min + $rnd;
    }

    function smtpmailer($to, $from, $from_name, $subject, $body)
	{
		require_once (APPPATH.'controllers/class/phpmailer/class.phpmailer.php');
		require_once (APPPATH.'controllers/class/phpmailer/class.smtp.php');
		global $error;
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPDebug = $this->config->item("smtp_debug");
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		$mail->Host = $this->config->item('smtp_host');
		$mail->Port = $this->config->item('smtp_port');
		$mail->IsHTML(true);
		$mail->Username = $this->config->item('smtp_acc');
		$mail->Password = $this->config->item('smtp_password');
		$mail->SetFrom($from, $from_name);
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->AddAddress($to);
		$isusecc = $this->config->item("is_usecc");

		if(!$mail->Send())
		{
			$error = 'Mail error: '.$mail->ErrorInfo;
			return $error;
		}
		else
		{
			$error = 'Message sent!';
			return true;
		}
	}
}

