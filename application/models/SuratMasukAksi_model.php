<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
class SuratMasukAksi_model extends Eloquent {

    // use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'surat_masuk_aksi';
    public $timestamps = false;

    protected $fillable = [
        'surat_masuk_id',
        'aksi_id',
    ];
}
