<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Project_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function insert($data)
    {
        $res = $this->db->insert("Project", $data);
        return $res;
    }
}
