<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Log_model extends CI_Model {

    function __construct ()
    {
		parent::__construct();
	}

    function insertlog($user, $type, $action, $logData=null)
    {
		date_default_timezone_set('Asia/Jakarta');
        unset($insertlog);

        $insertlog["log_name"] = $type;
        $insertlog["log_action"] = $action;
        $insertlog["log_user"] = $user;
        $insertlog["log_ip"] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";
        $insertlog["log_target"] = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "";
        $insertlog["log_created"] = date("Y-m-d H:i:s");
        $insertlog["log_modified"] = date("Y-m-d H:i:s");
        $insertlog["log_data"] = json_encode($logData);

        $this->db->insert("Log",$insertlog);
        return true;
    }

    function logInsert($data)
    {
		date_default_timezone_set('Asia/Jakarta');
        unset($insertlog);

        $user = logged();

        $insertlog["log_name"] = $data['type'];
        $insertlog["log_action"] = $data['action'];
        $insertlog["log_user"] = $user->user_name;
        $insertlog["log_ip"] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";
        $insertlog["log_target"] = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "";
        $insertlog["log_created"] = date("Y-m-d H:i:s");
        $insertlog["log_modified"] = date("Y-m-d H:i:s");

        if (isset($data['id'])) {
            $insertlog["subject_id"] = $data['id'];
        }

        if (isset($data['logData'])) {
            $insertlog["log_data"] = json_encode($data['logData']);
        }

        $this->db->insert("Log",$insertlog);

        return true;
    }
}

