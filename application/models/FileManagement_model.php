<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\models\MyEloquent_model;

class FileManagement_model extends MyEloquent_model {

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'file_management';
    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'category_id',
        'deskripsi',
        'client_filename',
        'filename',
        'filesize',
        'filetype',
        'fileext',
        'is_public',
    ];

    protected $casts = [
    ];

    public function fileManagementShare()
    {
        return $this->hasMany(FileManagementShare_model::class, 'file_management_id', 'id');
    }
}
