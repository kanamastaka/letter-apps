<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\models\MyEloquent_model;

class Jabatan_model extends MyEloquent_model {

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'jabatan';
    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'nama',
    ];

    protected $casts = [
        // 'user_modified' => 'date:Y-m-d H:i:s',
    ];
}
