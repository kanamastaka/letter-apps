<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\models\MyEloquent_model;

class Notification_model extends MyEloquent_model
{
    protected $table = 'notification';
    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'user_id',
        'modul_type',
        'modul_id',
        'message',
        'read_at',
        'created_at',
    ];

    protected $casts = [
        // 'user_modified' => 'date:Y-m-d H:i:s',
    ];
}
