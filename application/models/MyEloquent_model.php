<?php namespace App\models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class MyEloquent_model extends Eloquent
{
    public function __construct()
	{
		parent::__construct();

    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = logged('user_id');
            $model->updated_by = logged('user_id');
        });

        self::updating(function ($model) {
            if ($model->getDirty()) {
                $model->updated_by = logged('user_id');
            }
        });

        self::deleting(function ($model) {
            $model->deleted_by = logged('user_id');
        });
    }
}
