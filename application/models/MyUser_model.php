<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class MyUser_model extends Eloquent
{
    protected $table = 'User';
    public $timestamps = false;
    protected $primaryKey = 'user_id';
    // public $incrementing = false;

    protected $fillable = [
        'user_login',
        'user_name',
        'user_password',
        'user_type',
        'user_type_name',
        'user_status',
        'email',
        'user_nrp',
        'user_modified',
        'verifikator_level',
        'jabatan_id',
        'user_mobile',
    ];

    protected $casts = [
        'user_modified' => 'date:Y-m-d H:i:s',
    ];

    // public static function boot()
    // {
    //     parent::boot();

    //     self::creating(function ($model) {
    //         $model->created_by = logged('user_id');
    //         $model->updated_by = logged('user_id');
    //     });

    //     self::updating(function ($model) {
    //         if ($model->getDirty()) {
    //             $model->updated_by = logged('user_id');
    //         }
    //     });
    // }
}
