<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\models\MyEloquent_model;

class SuratKeluar_model extends MyEloquent_model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'surat_keluar';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    // public $timestamps = false;
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;

    protected $fillable = [
        'tgl_surat',
        'nama',
        'tujuan',
        'kerahasiaan',
        'level_verifikasi',
        'status_verifikasi',
        'filename',
        'catatan',
        'isi',
    ];

    protected $casts = [
        // 'user_modified' => 'date:Y-m-d H:i:s',
    ];

    // public static function boot()
    // {
    //     parent::boot();

    //     self::creating(function ($model) {
    //         $model->created_by = logged('user_id');
    //         $model->updated_by = logged('user_id');
    //     });

    //     self::updating(function ($model) {
    //         if ($model->getDirty()) {
    //             $model->updated_by = logged('user_id');
    //         }
    //     });
    // }
}
