<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Otp_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function makeOtp($email_phone_no, $OTPCategoryCode = 'E', $mailTitle = 'ASSGARD Login OTP')
    {
        try {
            $otpCode = substr(number_format(time() * rand(), 0, '', ''), 0, 6);

            // $duration = $OTPCategoryCode == 'P' ? 300 : 1800;
            $duration = 300; //5 menit

            $now = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s.u', \Carbon\Carbon::now()->format('Y-m-d H:i:s.u'));
            $OTPTimeStampFrom = $now->timestamp . '.' . $now->micro;
            $OTPTimeStampTo = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s.u', $now->format('Y-m-d H:i:s.u'))->addSeconds(intval($duration));
            $OTPTimeStampTo = $OTPTimeStampTo->timestamp . '.' . $OTPTimeStampTo->micro;

            $this->db->trans_begin();

            $id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $otpData = [
                'id'                 => $id,
                'email_phone_no'     => $email_phone_no,
                'otp_category_code'  => $OTPCategoryCode,
                'otp'                => $otpCode,
                'otp_timestamp_from' => $OTPTimeStampFrom,
                'otp_timestamp_to'   => $OTPTimeStampTo,
                'is_verified'        => false,
            ];

            $this->db->insert("otp", $otpData);

            if ($OTPCategoryCode == 'E') {
                $otpData['subjectTitle'] = $mailTitle;
                $this->sendEmailOtp($otpData);
            }

            $this->db->trans_commit();

            return $id;
        } catch (\Exception $e) {
            $this->db->trans_rollback();

            echo json_encode([
                "error"   => true,
                'message' => $e->getMessage()
            ]);
            exit;
        }
    }

    public function checkOtp($otpId, $otpCode)
    {
        $this->db->where("id", $otpId);
        $this->db->where("otp", $otpCode);
        $this->db->where("is_verified", 0);
        $this->db->order_by('otp_timestamp_from', 'desc');
        $q = $this->db->get("otp");
        $row = $q->row();

        if (isset($row->id) && $row->id != '') {
            $now = floatval(\Carbon\Carbon::now()->format('U.u'));
            if ($now > $row->otp_timestamp_to) {
                throw new Exception('Kode OTP sudah kedaluwarsa');
            }
        }
        else {
            throw new Exception('Kode OTP tidak ditemukan');
        }

        return $row;
    }

    public function sendEmailOtp($otpData)
    {
        $config = [
            'protocol'     => $_ENV['MAIL_MAILER'],
            'smtp_host'    => $_ENV['MAIL_HOST'],
            'smtp_port'    => $_ENV['MAIL_PORT'],
            'smtp_user'    => $_ENV['MAIL_USERNAME'],
            'smtp_pass'    => $_ENV['MAIL_PASSWORD'],
            'smtp_crypto'  => $_ENV['MAIL_ENCRYPTION'],
            'smtp_timeout' => '4',
            'mailtype'     => 'html',
            // 'charset'      => 'iso-8859-1'
            'charset'      => 'UTF-8'
        ];

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");

        $this->email->from($_ENV['MAIL_FROM_ADDRESS'], $_ENV['MAIL_FROM_NAME']);

        $data = [
            'otp'  => $otpData['otp'],
            'link' => base_url() . 'otp/' . $otpData['id'],
        ];

        $this->email->to($otpData['email_phone_no']);
        $this->email->subject($otpData['subjectTitle']);

        $body = $this->load->view('email/otpEmail', $data, true);
        $this->email->message($body);
        $this->email->send();
    }
}

