<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Org_model extends Eloquent
{
    protected $table = 'org';
    // public $incrementing = false;
    
    protected $fillable = [
        'id',
        'name',
        'lat',
        'lng',
        'website',
        'filename',
    ];

    protected $casts = [
        'lat' => 'double',
        'lng' => 'double',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = logged('user_id');
            $model->updated_by = logged('user_id');
        });

        self::updating(function ($model) {
            if ($model->getDirty()) {
                $model->updated_by = logged('user_id');
            }
        });
    }
}
