<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class SuratMasukAlamatAksi_model extends Eloquent {

    protected $table = 'surat_masuk_alamat_aksi';
    public $timestamps = false;

    protected $fillable = [
        'surat_masuk_id',
        'jabatan_id',
    ];
}
