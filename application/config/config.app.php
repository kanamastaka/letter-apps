<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['apptitle'] = $_ENV['APP_TITLE'];
$config['appdesc'] = $_ENV['APP_DESC'];
$config['appowner'] = $_ENV['APP_OWNER'];
$config['session_name'] = $_ENV['SESSION_NAME'];
$config['api_bigdata_cloud'] = "1c31e61efbfa4ba7a5d21f724444a015";
$config['results_txt_path'] = "/home/uosint/public_html/results-txt/";
$config['upload_path'] = "/Users/sonny/htdocs/surat/app-assets/upload";
$config['level_verifikasi_max'] = $_ENV['LEVEL_VERIFIKASI_MAX'];
$config['socket_url']	= $_ENV['SOCKET_URL'];
$config['socket_url_local']	= $_ENV['SOCKET_URL_LOCAL'];
$config['verificator_name'] = array('1'=>'Spri Kasetumal','2'=>'Kasetumal','3'=>'Spri Kasal/Wakasal','4'=>'Koorsmin Kasal/Wakasal');
