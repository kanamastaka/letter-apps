<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'osintauth';
$route['col/(:any)'] = 'col/index/';
$route['orderdetail/(:any)'] = 'orderdetail/index/';
$route['ac7iv4ti0n/(:any)'] = 'ac7iv4ti0n/index/';
$route['newsdetail/(:any)'] = 'newsdetail/index/';
$route['newscat/(:any)'] = 'newscat/index/';
$route['servicesdetail/(:any)'] = 'servicesdetail/index/';
$route['datalist/(:any)'] = 'datalist/index/';
$route['editing/m_user/(:num)'] = 'editing/index/';
$route['deleted/m_user/(:num)'] = 'deleted/index/';
$route['adding/(:any)'] = 'adding/index/';
$route['detail/m_user/(:num)'] = 'detail/index/';
$route['approve/(:any)'] = 'approve/index/';
$route['istoken/(:any)'] = 'istoken/index/';
$route['privacy'] = 'pages/privacy/';
$route['terms'] = 'pages/terms/';
$route['faq'] = 'pages/faq/';
$route['exportpdf/(:any)'] = 'exportpdf/index/';
$route['404_override'] = 'ctrl404';
$route['translate_uri_dashes'] = FALSE;

$route['otp/checkOtp'] = 'Otp/checkOtp';
$route['otp/resendOtp'] = 'Otp/resendOtp';
$route['otp/(:any)'] = 'Otp/index/$1';
