<?php
defined('BASEPATH') or exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['database'] = array("default");
$autoload['libraries'] = array('session', 'pagination', 'database');
$autoload['drivers'] = array();
$autoload['helper'] = array('basic', 'url', 'file', 'form', 'cookie', 'security', 'directory', 'language');
$autoload['config'] = array('config.app');
$autoload['language'] = array('basic');

$autoload['model'] = array(
	'users_model',
	'crud_model',
	'log_model',
	'settings_model',
	'otp_model',
);
